/*
// TI File $Revision: /main/3 $
// Checkin $Date: February 23, 2009   13:44:16 $
//###########################################################################
//
// FILE:	F2808.cmd
//
// TITLE:	Linker Command File For F2808 Device
//
//###########################################################################
// $TI Release: DSP2803x C/C++ Header Files V1.10 $
// $Release Date: July 27, 2009 $
//###########################################################################
*/

/* ======================================================
// For Code Composer Studio V2.2 and later
// ---------------------------------------
// In addition to this memory linker command file,
// add the header linker command file directly to the project.
// The header linker command file is required to link the
// peripheral structures to the proper locations within
// the memory map.
//
// The header linker files are found in <base>\DSP2803x_Headers\cmd
//
// For BIOS applications add:      DSP2803x_Headers_BIOS.cmd
// For nonBIOS applications add:   DSP2803x_Headers_nonBIOS.cmd
========================================================= */

/* ======================================================
// For Code Composer Studio prior to V2.2
// --------------------------------------
// 1) Use one of the following -l statements to include the
// header linker command file in the project. The header linker
// file is required to link the peripheral structures to the proper
// locations within the memory map                                    */

/* Uncomment this line to include file only for non-BIOS applications */
/* -l DSP2803x_Headers_nonBIOS.cmd */

/* Uncomment this line to include file only for BIOS applications */
/* -l DSP2803x_Headers_BIOS.cmd */

/* 2) In your project add the path to <base>\DSP2803x_headers\cmd to the
   library search path under project->build options, linker tab,
   library search path (-i).
/*========================================================= */

/* Define the memory block start/length for the F28035
   PAGE 0 will be used to organize program sections
   PAGE 1 will be used to organize data sections

   Notes:
         Memory blocks on F2803x are uniform (ie same
         physical memory) in both PAGE 0 and PAGE 1.
         That is the same memory region should not be
         defined for both PAGE 0 and PAGE 1.
         Doing so will result in corruption of program
         and/or data.

         L0 memory block is mirrored - that is
         it can be accessed in high memory or low memory.
         For simplicity only one instance is used in this
         linker file.

         Contiguous SARAM memory blocks or flash sectors can be
         be combined if required to create a larger memory block.
*/

MEMORY
{
PAGE 0:    /* Program Memory */
           /* Memory (RAM/FLASH/OTP) blocks can be moved to PAGE1 for data allocation */
   /*RAML0       : origin = 0x008000, length = 0x000800      on-chip RAM block L0 */

    RAML0BOOT_PRG   : origin = 0x008920, length = 0x000020 

    /*RAML0_PRG   : origin = 0x008000, length = 0x000920*/

    OTP         : origin = 0x3D7800, length = 0x000400     /* on-chip OTP */

	APPVEC 	   : origin = 0x3F0000, length = 0x000010

	FLASH       : origin = 0x3F0010, length = 0x006FEF

 	APPCRC	   : origin = 0x3F6FFF, length = 0x000001
	BOOTVEC    : origin = 0x3F7000, length = 0x000040
   	RAMFLASH   : origin = 0x3F7040, length = 0x000020

   	LIB_FLASH   : origin = 0x3F7060, length = 0x0001D8

   	FLASH_BOOT  : origin = 0x3F7238, length = 0x000D46     /* on-chip FLASH */ 
    REPAIR	    : origin = 0x3F7F7E, length = 0x000002

   	CSM_RSVD    : origin = 0x3F7F80, length = 0x000076     /* Part of FLASHA.  Program with all 0x0000 when CSM is in use. */
   	BEGIN       : origin = 0x3F7FF6, length = 0x000002     /* Part of FLASHA.  Used for "boot to Flash" bootloader mode. */
   	CSM_PWL     : origin = 0x3F7FF8, length = 0x000008     /* Part of FLASHA.  CSM password locations in FLASHA */

   	IQTABLES    : origin = 0x3FE000, length = 0x000B50     /* IQ Math Tables in Boot ROM */
   	IQTABLES2   : origin = 0x3FEB50, length = 0x00008C     /* IQ Math Tables in Boot ROM */
   	IQTABLES3   : origin = 0x3FEBDC, length = 0x0000AA	  /* IQ Math Tables in Boot ROM */

   	ROM         : origin = 0x3FF27C, length = 0x000D44     /* Boot ROM */
   	RESET       : origin = 0x3FFFC0, length = 0x000002     /* part of boot ROM  */
   	VECTORS     : origin = 0x3FFFC2, length = 0x00003E     /* part of boot ROM  */

PAGE 1 :   /* Data Memory */
           /* Memory (RAM/FLASH/OTP) blocks can be moved to PAGE0 for program allocation */
           /* Registers remain on PAGE1                                                  */
   	RAMM0       : origin = 0x000000, length = 0x000001     /* on-chip RAM block M0 */
   	RAMM1       : origin = 0x000001, length = 0x0007FF     /* on-chip RAM block M1 */
   	RAML0_DATA  : origin = 0x008940, length = 0x000900   	//wzc 20160914
	RAML0_PRG   : origin = 0x008000, length = 0x000920
}

/* Allocate sections to memory blocks.
   Note:
         codestart user defined section in DSP28_CodeStartBranch.asm used to redirect code
                   execution when booting to flash
         ramfuncs  user defined section to store functions that will be copied from Flash into RAM
*/

SECTIONS
{
	
  .cinit              : > FLASH      PAGE = 0 

  .text               : > FLASH      PAGE = 0 
    
   .init			  : LOAD = FLASH_BOOT      PAGE = 0 

    .cal  : > LIB_FLASH      PAGE = 0      
	{
		-lrts2800_ml.lib <fd_mpy.obj u_div.obj
		l_div.obj fd_tol.obj fs_tofd.obj fs_tou.obj
		boot.obj exit.obj _lock.obj args_main.obj> (.text)

		-lrts2800_ml.lib <exit.obj _lock.obj> (.cinit)

	}
	
	codestart           : > BEGIN       PAGE = 0 


    bootramfuncs        : LOAD = RAMFLASH, 
                         RUN = RAML0BOOT_PRG, 
                         LOAD_START(_BootRamfuncsLoadStart),
                         LOAD_END(_BootRamfuncsLoadEnd),
                         RUN_START(_BootRamfuncsRunStart),
                         PAGE = 0
/*   IsrRamfuncs            : LOAD = FLASH,
                         RUN = RAML0_PRG, 
                         LOAD_START(_IsrRamfuncsLoadStart),
                         LOAD_END(_IsrRamfuncsLoadEnd),
                         RUN_START(_IsrRamfuncsRunStart),
                         PAGE = 0
 */
  /* csmpasswds          : > CSM_PWL_P0  PAGE = 0*/

   .appvec             : > APPVEC   PAGE = 0
   appcrc              : > APPCRC   PAGE = 0
   .bootvec            : > BOOTVEC  PAGE = 0
   FlashBoot           : > FLASH_BOOT      PAGE = 0
   RepairVar           : > REPAIR   	PAGE = 0
   csmpasswds          : > CSM_PWL     PAGE = 0
   csm_rsvd            : > CSM_RSVD    PAGE = 0

   /* Allocate uninitalized data sections: */

   .stack              : > RAMM1       PAGE = 1
   .ebss               : > RAML0_DATA       PAGE = 1
   /*.esysmem            : > RAML3       PAGE = 1*/

   /* Initalized sections go in Flash */
   /* For SDFlash to program these, they must be allocated to page 0 */
   .econst             : > FLASH      PAGE = 0
   .switch             : > FLASH      PAGE = 0
   .const              : > FLASH      PAGE = 0
   /* Allocate IQ math areas: */
   IQmath              : > FLASH      PAGE = 0            /* Math Code */
   IQmathTables        : > IQTABLES,   PAGE = 0, TYPE = NOLOAD
  
   /* .reset is a standard section used by the compiler.  It contains the */
   /* the address of the start of _c_int00 for C Code.   /*
   /* When using the boot ROM this section and the CPU vector */
   /* table is not needed.  Thus the default type is set here to  */
   /* DSECT  */
   .reset              : > RESET,      PAGE = 0, TYPE = DSECT
   vectors             : > VECTORS     PAGE = 0, TYPE = DSECT



}

/*
//===========================================================================
// End of file.
//===========================================================================
*/

