/*=========================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H7413Z
 *
 *  FILENAME : gbb_initial.c
 *  PURPOSE  : variables and parameters initialization	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2011-08-26      A000           DSP               Created.   Pre-research 
 *==========================================================================*/


#include "DSP280x_Device.h"		// DSP280x Headerfile Include File
//UINT16   s_u16SfoStatus;
//INT16 MEP_ScaleFactor;
/***************************************************************************
 *Function name: vDataInit()  
 *Description :  All global variable initialization
 *input:         void                                
 *global vars:   void 
 *output:        void 
 *CALLED BY:    main() :only once after power on
 ****************************************************************************/			
void vDataInit(void)
{
	UINT16	i;
	UINT16 *addr;
	
//	GpioDataRegs.GPASET.bit.GPIO5 = 1;	// testp1 set

	Watch_Dog_Kick();

   // because cmd change 

   // main ram clear zero
	for(i=MAIN_RAM_START; i<MAIN_RAM_END; i++)
	{
		addr = (unsigned int *)i;
		*addr = 0;
	}

	// main isr  ram clear zero
	for(i=MAIN_ISR_RAM_START; i<MAIN_ISR_RAM_END; i++)
	{
		addr = (unsigned int *)i;
		*addr = 0;
	}
	
	Watch_Dog_Kick();

    // non zero variable initialization
    uPermitOverAc = 1;
    permitadjest = 1;
    ucMsgType = 0x01;

    setvolt.fd = 53.5;
    dclimit.fd = 1.1;
    setlimit.fd = 1.1;
    setpower.fd = 1.0;

    tagRunTime.fd = 1000;
    tagRunTimeSet.fd = 1000;
    dcbaud_float.fd = 1.00;

    pfcvolt.fd = 420;
    pfcvolt1.fd = 421;
    volt_adj_delta.fd = 128;
    dcvolt_up.fd = 59.5;
    dcvolt_float.fd = 53.5;
    dclimit_float.fd = 1.22;
    dcpower_float.fd = 1.0;
    accurr_set.fd = 30.0;

    temp_up.fd = 90;
    uBarCode1 = 0x3030;
    uBarCode2 = 0x3030;
    uBarCode3 = 0x3030;
    uBarCode4 = 0x3030;
    uBarCode5 = 0x3030;
    uBarCode6 = 0x3030;
    uBarCode7 = 0x3030;
    uBarCode8 = 0x3030;
}

/******************************************************************************
 *Function name: EnableInterrupts()
 *Description :  enables the  CPU interrupts
 *input:         void
 *global vars:   void
 *output:        void
 *CALLED BY:     main()
 ******************************************************************************/

void EnableInterrupts(void)
{

    EPwm1Regs.ETSEL.bit.INTEN = 1;  // Enable ePWM1 INT
    EPwm5Regs.ETSEL.bit.INTEN = 1;  // Enable ePWM5 INT

    IER |= M_INT2;  // Enable  interrupt  INT2.y include EPWM1_TZINT
    IER |= M_INT3;  // Enable  interrupt  INT3.y include EPWM(1-7)_INT

    // Enable global Interrupts and higher priority real-time debug events:
    EINT;           // Enable Global interrupt INTM
    ERTM;           // Enable Global realtime interrupt DBGM

}


/******************************************************************************
 *Function name: vInitialInterrupts()
 *Description :  enables the  the PIE  interrupts
 *input:         void
 *global vars:   void
 *output:        void
 *CALLED BY:     main()
 ******************************************************************************/
void vInitialInterrupts(void)
{
    // This is needed to write to EALLOW protected registers
    EALLOW;
    PieVectTable.SCIRXINTA = &vSciDataR;
    PieVectTable.SCITXINTA = &vSciDataT;
    PieVectTable.ECAN0INTA = &vCanIsr;
    PieVectTable.TINT0 = &cpu_timer0_isr;
    EDIS;
    PieCtrlRegs.PIEIER9.bit.INTx1 = SCIARX_INT_ENABLE;
    PieCtrlRegs.PIEIER9.bit.INTx2 = 1;
    PieCtrlRegs.PIEIER9.bit.INTx5 = 1;
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
    IER |= M_INT9;
    IER |= M_INT1;
    EINT;
    ERTM;

}
/*************************************************************************
 *
 *  FUNCTION NAME: InitRAM
 *
 *  DESCRIPTION:
 *
 *  PARAMETERS:     NONE
 *
 *  RETURNS:        NONE
 *
 *  CAVEATS:        NONE
 *
 ************************************************************************/
#pragma CODE_SECTION(InitRAM,"FlashBoot");
void InitRAM(void)
{
    //------------8000H-----------------------
    Uint16 *uiPTmp;


    for(uiPTmp = ((Uint16 *)BOOT_RAM_START);uiPTmp<((Uint16 *)BOOT_RAM_END);uiPTmp ++)
    {
        *uiPTmp = 0;
    }

    for(uiPTmp = ((Uint16 *)BOOT_ISR_RAM_START);uiPTmp< ((Uint16 *)BOOT_ISR_RAM_END);uiPTmp++)
    {//don't include variants of bootloader, ie. _uiFromApplication,and uiMdlAddr
        *uiPTmp = 0;
    }

    return;
}

/*************************************************************************
 *
 *  FUNCTION NAME: Watch_Dog_Kick
 *
 *  DESCRIPTION:    kick watch dog
 *
 *  PARAMETERS:     NONE
 *
 *  RETURNS:        NONE
 *
 *  CAVEATS:        NONE
 *
 ************************************************************************/
#pragma CODE_SECTION(Watch_Dog_Kick,"FlashBoot");
void Watch_Dog_Kick (void)
{
    EALLOW;
    SysCtrlRegs.WDKEY = 0x0055;
    SysCtrlRegs.WDKEY = 0x00AA;
    EDIS;
    return;
}


//===========================================================================
// No more.
//===========================================================================

