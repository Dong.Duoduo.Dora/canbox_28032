/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_CanDriver.c
 *  PURPOSE  : rectifier communicate with controller and other rectifiers	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A00           zoulx             Created.   Pre-research 
 *============================================================================*/

#include "DSP280x_Device.h"		// DSP280x Headerfile Include File

/*******************************************************************************
*variables dfinition														  
*******************************************************************************/
//variables for can data read and write
static CANFRAME s_aCanRxBuf[CAN_RXBUF_SIZE];
static CANFRAME s_aCanTxBuf[CAN_TXBUF_SIZE];	
static CANFRAME *s_pCanRxIn;				
static CANFRAME *s_pCanRxOut;
static CANFRAME *s_pCanTxIn;				
static CANFRAME *s_pCanTxOut;
static UINT16   s_u16CanRxLength;
static UINT16   s_u16CanTxLength;

//variables for can data decode and encode
UINT16 g_u16PROTNO;
UINT16 g_u16PTP;
UINT16 g_u16DSTADDR;
UINT16 g_u16SRCADDR;
UINT16 g_u16CNT;
UINT16 g_u16ErrFlag;
UINT16 g_u16ErrType;
UINT16 g_u16MsgType;
UINT16 g_u16ValueType;
ubitfloat g_ubitfCanData;

ubitfloat g_iq10CanData;
ubitfloat g_iq12CanData;

CANFRAME  g_stCanTxData;
CANFRAME  g_stCanRxData;

/*******************************************************************************
*functions declare                                      
*******************************************************************************/
//receive mail box
static void sCanResetRx0(void);

//transmit mail box reset
static void sCanResetTx5(void);

//transmit mail box transmit data
void sCanHdSend5(CANFRAME *pdata);

//receive mail box
static void sCanHdRead0(CANFRAME *pdata);

//application for app
static void sCanCopy(CANFRAME *pSource,CANFRAME *pDestination);

/*******************************************************************************
*Description:      initial can receive and transmit buffer                           
*******************************************************************************/
void sCanBufferInitial(void)
{
	s_pCanRxIn = s_aCanRxBuf;
	s_pCanRxOut = s_aCanRxBuf;
	s_pCanTxIn = s_aCanTxBuf;
	s_pCanTxOut = s_aCanTxBuf;
	s_u16CanTxLength = 0;
	s_u16CanRxLength = 0;
}

/*******************************************************************************
*Description:      These fucntions reset mailbox                            
*******************************************************************************/
//mail box 0: receive
static void sCanResetRx0(void)
{
    struct ECAN_REGS ECanaShadow;

	// receive data by box 0 success,  Reset RMP0
    EALLOW;
    ECanaShadow.CANRMP.all = ECanaRegs.CANRMP.all;
	ECanaShadow.CANRMP.bit.RMP1 = 1;
    ECanaRegs.CANRMP.all = ECanaShadow.CANRMP.all;
    EDIS;
}

//mail box 4: transmit
void sCanResetTx4(void)
{
    struct ECAN_REGS ECanaShadow;

	// send data in box 4 success,  Reset TA4
    EALLOW;    
    ECanaShadow.CANTA.all = ECanaRegs.CANTA.all; 	
	ECanaShadow.CANTA.bit.TA4 = 1;		 
	ECanaRegs.CANTA.all = ECanaShadow.CANTA.all; 
    EDIS;
}

//mail box 4: transmit
static void sCanResetTx5(void)
{
    struct ECAN_REGS ECanaShadow;

	// send data in box 4 success,  Reset TA4
    EALLOW;    
    ECanaShadow.CANTA.all = ECanaRegs.CANTA.all; 	
	ECanaShadow.CANTA.bit.TA5 = 1;		 
	ECanaRegs.CANTA.all = ECanaShadow.CANTA.all; 
    EDIS;
}

/*******************************************************************************
*Description:      Read message from the mailbox                             
*******************************************************************************/
static void sCanHdRead0(CANFRAME *pdata)
{
    pdata->CanId.all = ECanaMboxes.MBOX0.MSGID.all;
    pdata->CanData0 = ECanaMboxes.MBOX0.MDL.word.LOW_WORD;
    pdata->CanData1 = ECanaMboxes.MBOX0.MDL.word.HI_WORD;
    pdata->CanData2 = ECanaMboxes.MBOX0.MDH.word.LOW_WORD;
    pdata->CanData3 = ECanaMboxes.MBOX0.MDH.word.HI_WORD;
//	CANFRAME s_pTemp;
//
//	s_pTemp.CanId.all = ECanaMboxes.MBOX1.MSGID.all;
//    s_pTemp.CanData0 = ECanaMboxes.MBOX1.MDL.word.LOW_WORD;
//    s_pTemp.CanData1 = ECanaMboxes.MBOX1.MDL.word.HI_WORD;
//    s_pTemp.CanData2 = ECanaMboxes.MBOX1.MDH.word.LOW_WORD;
//    s_pTemp.CanData3 = ECanaMboxes.MBOX1.MDH.word.HI_WORD;
//
//	if(ECanaRegs.CANRMP.all & 0x0002)
//    {
//        sCanResetRx0();
//
//        s_pTemp.CanId.all = ECanaMboxes.MBOX1.MSGID.all;
//	    s_pTemp.CanData0 = ECanaMboxes.MBOX1.MDL.word.LOW_WORD;
//	    s_pTemp.CanData1 = ECanaMboxes.MBOX1.MDL.word.HI_WORD;
//	    s_pTemp.CanData2 = ECanaMboxes.MBOX1.MDH.word.LOW_WORD;
//	    s_pTemp.CanData3 = ECanaMboxes.MBOX1.MDH.word.HI_WORD;
//    }
//
//	sCanCopy(&s_pTemp,pdata);
}

/*******************************************************************************
*Description:      transmit message to the mailbox                           
*******************************************************************************/
void    sCanHdSend4(CANFRAME *pdata)
{
	struct ECAN_REGS ECanaShadow;

    /*disale mailbox*/
    EALLOW;
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME4 = 0;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;

    /*CPU request to write to mailbox 4*/
    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.CDR = 1;
    ECanaShadow.CANMC.bit.MBNR = 4;
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;
    EDIS;

    /*write MSGID*/
    //ECanaMboxes.MBOX4.MSGID.all = pdata->CanId.all;

    /*write data length to MSGCTRL*/
    ECanaMboxes.MBOX4.MSGCTRL.bit.DLC = 8;
    
    /*write data to transmit mailbox*/
    ECanaMboxes.MBOX4.MDL.word.LOW_WORD = pdata->CanData0;
    ECanaMboxes.MBOX4.MDL.word.HI_WORD = pdata->CanData1;
    ECanaMboxes.MBOX4.MDH.word.LOW_WORD = pdata->CanData2;
    ECanaMboxes.MBOX4.MDH.word.HI_WORD = pdata->CanData3;

    /*Configure mailbox for transmit mailbox*/
    EALLOW;
    ECanaShadow.CANMD.all = ECanaRegs.CANMD.all;
    ECanaShadow.CANMD.bit.MD4 = 0;
    ECanaRegs.CANMD.all = ECanaShadow.CANMD.all;

    /*Enable transmit mailbox*/
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME4 = 1;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
    EDIS; 

    /*after write mailbox,CDR must clear*/
    EALLOW;
    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.CDR = 0;
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

    /*set transmit request to start transmit process*/
    ECanaShadow.CANTRS.all = ECanaRegs.CANTRS.all;
    ECanaShadow.CANTRS.bit.TRS4 = 1;
    ECanaRegs.CANTRS.all = ECanaShadow.CANTRS.all;
    EDIS;
}

void sCanHdSend5(CANFRAME *pdata)
{
	struct ECAN_REGS ECanaShadow;

    /*disale mailbox*/
    EALLOW;
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME5 = 0;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;

    /*CPU request to write to mailbox 4*/
    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.CDR = 1;
    ECanaShadow.CANMC.bit.MBNR = 5;
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;
    EDIS;

    /*write MSGID*/
    //ECanaMboxes.MBOX5.MSGID.all = pdata->CanId.all;

    /*write data length to MSGCTRL*/
    //ECanaMboxes.MBOX5.MSGCTRL.bit.DLC = 8;
    
    /*write data to transmit mailbox*/
    ECanaMboxes.MBOX5.MDL.word.LOW_WORD = pdata->CanData0;
    ECanaMboxes.MBOX5.MDL.word.HI_WORD = pdata->CanData1;
    ECanaMboxes.MBOX5.MDH.word.LOW_WORD = pdata->CanData2;
    ECanaMboxes.MBOX5.MDH.word.HI_WORD = pdata->CanData3;

    /*Configure mailbox for transmit mailbox*/
    EALLOW;
    ECanaShadow.CANMD.all = ECanaRegs.CANMD.all;
    ECanaShadow.CANMD.bit.MD5 = 0;
    ECanaRegs.CANMD.all = ECanaShadow.CANMD.all;

    /*Enable transmit mailbox*/
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME5 = 1;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
    EDIS; 

    /*after write mailbox,CDR must clear*/
    EALLOW;
    ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
    ECanaShadow.CANMC.bit.CDR = 0;
    ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

    /*set transmit request to start transmit process*/
    ECanaShadow.CANTRS.all = ECanaRegs.CANTRS.all;
    ECanaShadow.CANTRS.bit.TRS5 = 1;
    ECanaRegs.CANTRS.all = ECanaShadow.CANTRS.all;
    EDIS;
}

/*******************************************************************************
*Function name:		sCanCopy											  
*Parameters:		pSource:pointer to the source data						  
*					pDestination:pointer to the destination data			  
*Description:		This fucntion copy the content of psource to pDestination 
*******************************************************************************/
static void	sCanCopy(CANFRAME *pSource,CANFRAME *pDestination)
{
	pDestination->CanId.all = pSource->CanId.all;
	pDestination->CanData0 = pSource->CanData0;
	pDestination->CanData1 = pSource->CanData1;
	pDestination->CanData2 = pSource->CanData2;
	pDestination->CanData3 = pSource->CanData3;
}

/*******************************************************************************
*Function name: sCanRxISR											  
*Parameters:		none													  
*Description:		This fucntion should be executed in the can receiving 	  
*					interrupt,it save the received data to the queue 
*******************************************************************************/
void sCanRxISR0(void)
{	
	sCanResetRx0();
	
	if(s_u16CanRxLength == CAN_RXBUF_SIZE)
	{
		return;
	}
	else
	{
		sCanHdRead0(s_pCanRxIn);
		if(s_pCanRxIn == &s_aCanRxBuf[CAN_RXBUF_SIZE - 1])
		{
			s_pCanRxIn = s_aCanRxBuf;
		}
		else
		{
			s_pCanRxIn++;
		}
		s_u16CanRxLength++;
	}	
}

/*******************************************************************************
*Function name:		sCanRead											  
*Parameters:		pdata:the address where to save the data				  
*Returns:			CAN_RXBUF_EMPTY:there is no data in the receive buffer	  
*					CAN_RXBUF_RDY:data is read successfully from buffer
*Description:		This function get the data from receiving buffer and save
*					to user's buffer.it should be executed in app			  
*******************************************************************************/
UINT16	sCanRead(CANFRAME *pdata)
{
	if(s_u16CanRxLength == 0)
	{
		return(CAN_RXBUF_EMPTY);
	}
		
	sCanCopy(s_pCanRxOut,pdata);
	
	s_u16CanRxLength--;
	
	if(s_pCanRxOut == &s_aCanRxBuf[CAN_RXBUF_SIZE - 1])
	{
		s_pCanRxOut = s_aCanRxBuf;
	}
	else
	{
		s_pCanRxOut++;
	}
	
	return(CAN_RXBUF_RDY);
}

/********************************************************************************
*Function name:		sCanTxISR4													
*Parameters:		none														
*Description:		This fucntion should be executed in the can transmit 		
*					interrupt,it changes the status of transmition				
*********************************************************************************/
void sCanTxISR5(void)       
{
	sCanResetTx5();
	
	if(s_pCanTxOut == &s_aCanTxBuf[CAN_TXBUF_SIZE - 1])
	{
		s_pCanTxOut = s_aCanTxBuf;
	}
	else
	{
		s_pCanTxOut++;
	}

	if(s_u16CanTxLength > 1)
	{
		sCanHdSend5(s_pCanTxOut);		
	}
	s_u16CanTxLength--;	
}

/********************************************************************************
*Function name:		sCanWrite5													
*Parameters:		pdata:the start address to be sent							
*Returns:			cCanTxBusy:the can bus is busy								
*					cCanTxRdy:can bus is ready to transmit this frame			
*Description:		This fucntion send the data to the can registers			
*********************************************************************************/
void	sCanWrite5(CANFRAME	*pdata) 
{
	if(s_u16CanTxLength == CAN_TXBUF_SIZE)
	{
		sCanHdSend5(s_pCanTxOut);
		return;
	}	
	
	sCanCopy(pdata,s_pCanTxIn);
	
	s_u16CanTxLength++;
	
	if(s_pCanTxIn == &s_aCanTxBuf[CAN_TXBUF_SIZE - 1])
	{
		s_pCanTxIn = s_aCanTxBuf;
	}
	else
	{
		s_pCanTxIn++;
	}	
	
	if(s_u16CanTxLength == 1)
	{
		sCanHdSend5(s_pCanTxOut);
	}
}
/********************************************************************************
*Function name:     sCanWrite4
*Parameters:        pdata:the start address to be sent
*Returns:           cCanTxBusy:the can bus is busy
*                   cCanTxRdy:can bus is ready to transmit this frame
*Description:       This fucntion send the data to the can registers
*********************************************************************************/
void    sCanWrite4(CANFRAME *pdata)
{
    if(s_u16CanTxLength == CAN_TXBUF_SIZE)
    {
        sCanHdSend4(s_pCanTxOut);
        return;
    }

    sCanCopy(pdata,s_pCanTxIn);

    s_u16CanTxLength++;

    if(s_pCanTxIn == &s_aCanTxBuf[CAN_TXBUF_SIZE - 1])
    {
        s_pCanTxIn = s_aCanTxBuf;
    }
    else
    {
        s_pCanTxIn++;
    }

    if(s_u16CanTxLength == 1)
    {
        sCanHdSend4(s_pCanTxOut);
    }
}

/*******************************************************************************
*Function: sDecodeCanRxFrame
*Discription:The function describes how to decode a frame of can data
*******************************************************************************/
void sDecodeCanRxFrame(CANFRAME stRxFrameTemp)
{
	//Decode message ID
	g_u16PROTNO = stRxFrameTemp.CanId.bit.PROTNO;	
	g_u16PTP = stRxFrameTemp.CanId.bit.PTP;

	g_u16DSTADDR = (stRxFrameTemp.CanId.bit.DSTADDRH << 5) 
	               | stRxFrameTemp.CanId.bit.DSTADDRL;

	g_u16SRCADDR = stRxFrameTemp.CanId.bit.SRCADDR;
	g_u16CNT = stRxFrameTemp.CanId.bit.CNT;	

	//Decode message data0
	g_u16ErrType = stRxFrameTemp.CanData1 & 0x00ff;	
	g_u16MsgType = (stRxFrameTemp.CanData1 >> 8) & 0x007f;

	//Decode message data1
	g_u16ValueType = stRxFrameTemp.CanData0;
	
	//Decode message data2~3
    g_ubitfCanData.intdata[0].id = stRxFrameTemp.CanData2;
	g_ubitfCanData.intdata[1].id = stRxFrameTemp.CanData3;

	g_iq10CanData.lData = _IQ10(g_ubitfCanData.fd);
    g_iq12CanData.lData = _IQ12(g_ubitfCanData.fd);
}

/*******************************************************************************
*Function:  sEncodeCanTxID(),sEncodeCanTxData() 
*Description: These functions describe how to encode a frame of can data
*******************************************************************************/
void sEncodeCanTxID(UINT16 u16ProtNoTemp,UINT16 u16PTPTemp,
                    UINT16 u16DstAddrTemp,UINT16 u16CntTemp)
{
	//encode a frame of can data ID
	g_stCanTxData.CanId.bit.PROTNO = u16ProtNoTemp;
	g_stCanTxData.CanId.bit.PTP = u16PTPTemp;
	g_stCanTxData.CanId.bit.DSTADDRH = u16DstAddrTemp >> 5;
    g_stCanTxData.CanId.bit.DSTADDRL = u16DstAddrTemp & 0x001f;
	g_stCanTxData.CanId.bit.SRCADDR = 0;//g_u16MdlAddr;
	g_stCanTxData.CanId.bit.CNT = u16CntTemp;
	
	g_stCanTxData.CanId.all |= 0xc0000003;
}

/*******************************************************************************
*Function:  sEncodeCanTxID(),sEncodeCanTxData()
*Description: These functions describe how to encode a frame of can data
*******************************************************************************/
void sEncodeCanTxID1(UINT16 u16ProtNoTemp,UINT16 u16PTPTemp,
                    UINT16 u16DstAddrTemp,UINT16 u16CntTemp)
{
    //encode a frame of can data ID
    //g_stCanTxData.CanId.bit.PROTNO = u16ProtNoTemp;
    g_stCanTxData.CanId.bit.PTP = u16PTPTemp;
    g_stCanTxData.CanId.bit.DSTADDRH = u16DstAddrTemp >> 5;
    g_stCanTxData.CanId.bit.DSTADDRL = u16DstAddrTemp & 0x001f;
    g_stCanTxData.CanId.bit.SRCADDR = 0;//g_u16MdlAddr;
    g_stCanTxData.CanId.bit.CNT = u16CntTemp;

    g_stCanTxData.CanId.all |= 0xc0000003;
}

void sEncodeCanTxData(UINT16 u16MsgTypeTemp,UINT16 u16ErrTypeTemp, 
                      UINT16 u16ValueTypeTemp,ubitfloat ubitfDataTemp)
{

	g_stCanTxData.CanData1 = (u16MsgTypeTemp << 8) + u16ErrTypeTemp;
	
	//if( (u16ErrTypeTemp != NORMAL) && (u16ErrTypeTemp != ADDR_IDENTIFY) )
	if( (u16ErrTypeTemp == ADDR_FAIL) || (u16ErrTypeTemp == COMM_FAIL))		// ls 20120409
	{
    	g_stCanTxData.CanData1 = g_stCanTxData.CanData1 | 0x8000;
    }

	//encode a frame of can data1
	g_stCanTxData.CanData0 = u16ValueTypeTemp;

	//encode a frame of can data2~3	
	g_stCanTxData.CanData2 = ubitfDataTemp.intdata[0].id;
    g_stCanTxData.CanData3 = ubitfDataTemp.intdata[1].id;
}

void sEncodeCanTxIQ10Data(UINT16 u16MsgTypeTemp,UINT16 u16ErrTypeTemp, 
                      UINT16 u16ValueTypeTemp,ubitfloat ubitfDataTemp)
{
	g_stCanTxData.CanData1 = (u16MsgTypeTemp << 8) + u16ErrTypeTemp;
	
	//if( (u16ErrTypeTemp != NORMAL) && (u16ErrTypeTemp != ADDR_IDENTIFY) )
	if( (u16ErrTypeTemp == ADDR_FAIL) || (u16ErrTypeTemp == COMM_FAIL))		// ls 20120409
	{
    	g_stCanTxData.CanData1 = g_stCanTxData.CanData1 | 0x8000;
    }

	//encode a frame of can data1
	g_stCanTxData.CanData0 = u16ValueTypeTemp;

	//encode a frame of can data2~3
	//ubitfDataTemp.fd =  _IQ10toF(ubitfDataTemp.lData);
	g_stCanTxData.CanData2 = ubitfDataTemp.intdata[0].id;
    g_stCanTxData.CanData3 = ubitfDataTemp.intdata[1].id;
}

void sEncodeCanTxU16Data(UINT16 CanData0, UINT16 CanData1, UINT16 CanData2, UINT16 CanData3)
{
    g_stCanTxData.CanData0 = CanData0;
    g_stCanTxData.CanData1 = CanData1;
    g_stCanTxData.CanData2 = CanData2;
    g_stCanTxData.CanData3 = CanData3;
}

void sEncodeMailBox5ID(unsigned int num,unsigned int len)
{
    struct ECAN_REGS ECanaShadow;
    //ECanaRegs.CANME.all = ECanaRegs.CANME.all & 0xFFDF;
    EALLOW;
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME5 = 0;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
    EDIS;
    asm (" NOP ");

    ECanaMboxes.MBOX5.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX5.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX5.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX5.MSGID.bit.PROTNO = 0x1D0 + num;
    ECanaMboxes.MBOX5.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX5.MSGID.bit.DSTADDRH = address/32;       // first, only addr
    ECanaMboxes.MBOX5.MSGID.bit.DSTADDRL = address%32;       //
    ECanaMboxes.MBOX5.MSGID.bit.SRCADDR = 0x0E0;
    ECanaMboxes.MBOX5.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX5.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX5.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX5.MSGCTRL.all = len;

    //ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x0020;
    EALLOW;
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.bit.ME5 = 1;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
    EDIS;
}






