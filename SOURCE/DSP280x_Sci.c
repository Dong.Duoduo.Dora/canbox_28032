// TI File $Revision: /main/2 $
// Checkin $Date: December 2, 2004   11:59:53 $
//###########################################################################
//
// FILE:	DSP280x_Sci.c
//
// TITLE:	DSP280x SCI Initialization & Support Functions.
//
//###########################################################################
// $TI Release: DSP280x V1.30 $
// $Release Date: February 10, 2006 $
//###########################################################################

#include "DSP280x_Device.h"     // DSP280x Headerfile Include File

void scia_fifo_init(void);
void scia_enableTX_disableRX(void);
void scia_enableRX_disableTX(void);
//---------------------------------------------------------------------------
// InitSci: 
//---------------------------------------------------------------------------
// This function initializes the SCI(s) to a known state.

void InitSci(void)
{
	// Initialize SCI-A:

	InitSciaGpio();
	//38400,odd parity,8 data bits,one stop bit
	SciaRegs.SCICCR.all = 0x0027;   // 1 stop, odd parity,8 char, async mode, idle-line
	SciaRegs.SCICTL1.all = 0x0001;  // bit0,enable RX, bit5,reset SCI, bit1,disable TX
	SciaRegs.SCICTL2.all = 0x0000;  // bit0:TX INT  bit1:RX/BK INT all disable
    SciaRegs.SCIFFTX.all = 0xE040;  //SCI reset,SCI FIFO enable,txFIFO reset,disable txFIFO interrupt
    SciaRegs.SCIFFRX.all = 0x2061;  //reset receive FIFO,clear RXFFINT flag,enabled recFIFO interrupt
    SciaRegs.SCIFFCT.all = 0x0;     //

	//SciaRegs.SCIHBAUD = 0x0000;   // 9600 baud @LSPCLK = 15MHz.
	//SciaRegs.SCILBAUD = 0x00C1;       //LSPCLK/(BRR+1)/8=9600,==>BRR=194
	SciaRegs.SCIHBAUD = 0x0000;   // 38400 baud @LSPCLK = 15MHz.
	SciaRegs.SCILBAUD = 0x0030;       //LSPCLK/(BRR+1)/8=38400,==>BRR=48
	//SciaRegs.SCIHBAUD = 0x0001;  	// 9600 baud @LSPCLK = 25MHz.
	//SciaRegs.SCILBAUD = 0x0044;		//LSPCLK/(BRR+1)/8=9600,==>BRR=324
	//SciaRegs.SCIHBAUD = 0x0000;  		// 19200 baud @LSPCLK = 25MHz.
	//SciaRegs.SCILBAUD = 0x00A2;		//LSPCLK/(BRR+1)/8=19200,==>BRR=162
	//SciaRegs.SCILBAUD = 0x0060;		// 15M/(BRR+1)/8=19200,==>BRR=96
	
	SciaRegs.SCIRXST.all = 0x00;   //Read access only
	SciaRegs.SCIRXEMU    = 0x00;   //Read access only
	SciaRegs.SCIRXBUF.all= 0x00;
	SciaRegs.SCITXBUF    = 0x00;
	SciaRegs.SCIPRI.all  = 0x10;  // 1 0 Complete current receive/transmit sequence before stopping

	SciaRegs.SCICTL1.all = 0x0021;  // Relinquish SCI from Reset

    //SciaRegs.SCICTL2.bit.RXBKINTENA = 1;  //使能SCIa接收中断
    //SciaRegs.SCICTL2.bit.TXINTENA = 0;    //禁止SCIa发送中断

    //scia_fifo_init();
}	

void InitSciaGpio()
{
   EALLOW;

/* Enable internal pull-up for the selected pins */
// Pull-ups can be enabled or disabled disabled by the user.  
// This will enable the pullups for the specified pins.

	GpioCtrlRegs.GPAPUD.bit.GPIO28 = 0;    // Enable pull-up for GPIO28 (SCIRXDA)
	GpioCtrlRegs.GPAPUD.bit.GPIO29 = 0;	   // Enable pull-up for GPIO29 (SCITXDA)

/* Set qualification for selected pins to asynch only */
// Inputs are synchronized to SYSCLKOUT by default.  
// This will select asynch (no qualification) for the selected pins.

	GpioCtrlRegs.GPAQSEL2.bit.GPIO28 = 3;  // Asynch input GPIO28 (SCIRXDA)

/* Configure SCI-A pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be SCI functional pins.

	GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 1;   // Configure GPIO28 for SCIRXDA operation
	GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 1;   // Configure GPIO29 for SCITXDA operation
	
    EDIS;
}

void scia_fifo_init()
{
    SciaRegs.SCIFFTX.all=0xE040; //1110 0000 0100 0000B
    SciaRegs.SCIFFRX.all=0x2044; //0110 0000 0100 1111B
    SciaRegs.SCIFFCT.all=0x0;    //

}

void scia_enableTX_disableRX()
{
    SciaRegs.SCICTL1.all = 0x0002;
    //SciaRegs.SCICTL2.bit.RXBKINTENA = 0;  //禁止SCIa接收中断
    //SciaRegs.SCICTL2.bit.TXINTENA = 1;    //使能SCIa发送中断
    SciaRegs.SCIFFRX.bit.RXFFIENA = 0;    //禁止SCIa FIFO接收中断
    SciaRegs.SCIFFTX.bit.TXFFIENA = 1;    //使能SCIa FIFO发送中断
    SciaRegs.SCICTL1.all = 0x0022;
}

void scia_enableRX_disableTX()
{
    SciaRegs.SCICTL1.all = 0x0001;
    //SciaRegs.SCICTL2.bit.RXBKINTENA = 1;  //使能SCIa接收中断
    //SciaRegs.SCICTL2.bit.TXINTENA = 0;    //禁止SCIa发送中断
    SciaRegs.SCIFFRX.bit.RXFFIENA = 1;    //使能SCIa FIFO接收中断
    SciaRegs.SCIFFTX.bit.TXFFIENA = 0;    //禁止SCIa FIFO发送中断
    SciaRegs.SCICTL1.all = 0x0021;
}
	
//===========================================================================
// End of file.
//===========================================================================
