/*===========================================================================*
 *         Copyright(c) 2004-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H7413Z
 *
 *  FILENAME : gbb_main.c
 *  PURPOSE  : DSP280x  main Functions.
 *				Running on TMS320LF28032                  
 *				External clock is 20MHz, PLL * 6/2 , CPU-Clock 60MHz	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2011-08-26      9000           DSP               Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    detail information refer to gbb_main.h      
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    void vMtimerTreat(void)  
 *    
 *==========================================================================*/

#include "DSP280x_Device.h"		// DSP280x Headerfile Include File


/*****************************************************************************
*variables definition:these variables only can use in this file 														  
*****************************************************************************/
//static UINT16	s_u16BaseTimer; 		// base timer
/*****************************************************************************
*functions declare:these functions only can use in this file                                    
*****************************************************************************/
//the main function
void main(void);

//Time base and timing treatment, every 5ms
void vMtimerTreat(void);

/*****************************************************************************
 *FUNCTION: main()  
 *PURPOSE:  this function is the main procedure of the system 
 *INTPUT:         void
 *global vars:   void 
 *RETURN:
 *CALLS:
 *CALLED BY:
 *Author:
 *Date:     
 ****************************************************************************/
void main(void)
{
	//Clear all interrupt, initialize PIE vector table:Disable CPU interrupt 
	DINT;
	InitSysCtrl();
	//ram initialize
	InitRAM();

	//Disable CPU interrupts and clear all CPU interrupt flags
	IER = 0x0000;
	IFR = 0x0000;
    
	//Initialize PIE control registers:detail info in DSP280x_PieCtrl.c file
	InitPieCtrl();

	//Initialize PIE vector table with pointers to the shell ISR  
	InitPieVectTable();

	//note zlm  20150609
	/*  
	Initialize the Flash_CPUScaleFactor variable to SCALE_FACTOR
    Initialize the callback function pointer or set it to NULL
    */
   	EALLOW;
   	Flash_CPUScaleFactor = SCALE_FACTOR;
	EDIS;

   	EALLOW;
   	Flash_CallbackPtr = NULL;//&MyCallbackFunction; 
   	EDIS; 

   	InitGpio();

	// This function Initial and  enables the PIE  interrupts
	vInitialInterrupts();

	MemCopy(&BootRamfuncsLoadStart, &BootRamfuncsLoadEnd,
				   &BootRamfuncsRunStart);
  	//MemCopy(&IsrRamfuncsLoadStart, &IsrRamfuncsLoadEnd,
  	//              &IsrRamfuncsRunStart);
    
	//for SFO variable initialization
    //HrmstepInit();
    
	// Initialize other periphereral:detail info in DSP280x_InitPeripherals.c
	//InitPeripherals();
	InitPeripherals();

	//enable Watchdog
	EnableDog();

	//variable initialization
 	vDataInit();

	sCanBufferInitial();

	//EnableInterrupts();

	for(; ;)
	{
        if (g_u8receive_ok)
        {
            vSciDataParsing();
            g_u8receive_ok = 0;
        }

        Watch_Dog_Kick();
	}
}

/*****************************************************************************
 *FUNCTION: vMtimerTreat()  
 *PURPOSE: Time base and timing treatment, every 5ms 
 *INPUT: void
 *global vars: g_u16RunFlag:run flag of  all functions
 *RETURN: void
 *CALLS: 
 *CALLED BY: main() 
 *Author:
 *Date:
 ****************************************************************************/
void vMtimerTreat(void)
{	
	// 5mS flag 			
		
	if (mGet5msTiming())			
	{
		//clear 5ms flag
		mClr5msTimFlag();

		// avoid watchdog reset, feed dog
		Watch_Dog_Kick();
	}
}

interrupt void cpu_timer0_isr(void)
{
   can_overtime_ok++;
   if (can_overtime_ok >= 100)
   {
       can_overtime_ok = 100;
   }

   g_u16light1_time++;
   g_u16light2_time++;
   g_u16light3_time++;
   g_u16light4_time++;

   if(g_u16light1_time >= 1000)
   {
       g_u16light1_time = 0;
       g_u8uLight_status = g_u8uLight_status & 0xfb;  //1011b
   }
   if(g_u16light2_time >= 1000)
   {
       g_u16light2_time = 0;
       g_u8uLight_status = g_u8uLight_status & 0xf7;  //0111b
   }
   if(g_u16light3_time >= 500)
   {
       g_u16light3_time = 0;
       g_u8uLight_status = g_u8uLight_status & 0xfe;  //1110
   }
   if(g_u16light4_time >= 500)
   {
       g_u16light4_time = 0;
       g_u8uLight_status = g_u8uLight_status & 0xfd;  //1101
   }

   CpuTimer0.InterruptCount++;
   if (CpuTimer0.InterruptCount >= 30)
   {
       if (CpuTimer0.InterruptCount >= 60)
       {
           CpuTimer0.InterruptCount = 0;
       }

       mGreenLed3Off();//CANBOX working led

       if(g_u8uLight_status & 0x08)//SCI send led
       {
           mRedLed1On();
       }
       else
       {
           mRedLed1Off();
       }

       if(g_u8uLight_status & 0x02)//can mailbox0 receive led
       {
           mRedLed2On();
       }
       else
       {
           mRedLed2Off();
       }
   }
   else
   {
       mGreenLed3On();//CANBOX working led

       if(g_u8uLight_status & 0x04)//SCI receive led
       {
           mGreenLed1On();
       }
       else
       {
           mGreenLed1Off();
       }

       if(g_u8uLight_status & 0x01)//can mailbox1 receive led
       {
           mGreenLed2On();
       }
       else
       {
           mGreenLed2Off();
       }
   }

   g_u16CanIntTimer++;
   if (g_u16CanIntTimer >= 1000)
   {
       if (u16MdlType == HVDC)
       {
           hvdc_can_init();
       }
       else if(u16MdlType == Converter)//zlm 20120406 add Converter
       {
           Converter_can_init();
       }
       else if(u16MdlType == IS_RECT)  //mhp 20130329 add IS rectifiier
       {
           ISrect_can_init();
       }
       else if(u16MdlType == MPPT) //mhp 201300925 add MPPT rectifiier
       {
           MPPT_can_init();
       }
       else if(u16MdlType == MPPT_HV)  //mhp 20130329 add IS rectifiier
       {
           MPPT_can_init();
       }
       else if(u16MdlType == EPA)
       {
           EPA_can_init();
       }
       else
       {
           InitECanaID();
       }

       g_u16CanIntTimer = 0;
   }

   PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;  // Acknowledge this interrupt to receive more interrupts from group 1
}


//===========================================================================
// No more.
//===========================================================================

