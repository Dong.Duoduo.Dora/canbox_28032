// TI File $Revision: /main/1 $
// Checkin $Date: December 1, 2004   11:11:34 $
//###########################################################################
//
// FILE:	DSP280x_CpuTimers.c
//
// TITLE:	CPU 32-bit Timers Initialization & Support Functions.
//
// NOTES:   CpuTimer1 and CpuTimer2 are reserved for use with DSP BIOS and
//          other realtime operating systems.  
//
//          Do not use these two timers in your application if you ever plan
//          on integrating DSP-BIOS or another realtime OS. 
//
//          For this reason, the code to manipulate these two timers is
//          commented out and not used in these examples.
//           
//###########################################################################
// $TI Release: DSP280x V1.30 $
// $Release Date: February 10, 2006 $
//###########################################################################

#include "DSP280x_Device.h"     // Headerfile Include File

struct CPUTIMER_VARS CpuTimer0;

// CpuTimer 1 and CpuTimer2 are reserved for DSP BIOS & other RTOS
//struct CPUTIMER_VARS CpuTimer1;
//struct CPUTIMER_VARS CpuTimer2;

//---------------------------------------------------------------------------
// InitCpuTimers: 
//---------------------------------------------------------------------------
// This function initializes all three CPU timers to a known state.
//
void InitCpuTimers(void)
{
    // CPU Timer 0
	// Initialize address pointers to respective timer registers:
	CpuTimer0.RegsAddr = &CpuTimer0Regs;
	// Initialize timer period to maximum:	
	CpuTimer0Regs.PRD.all  = 0x493E0;
		
	// Initialize pre-scale counter to divide by 1 (SYSCLKOUT):	
	CpuTimer0Regs.TPR.all  = 0;
	CpuTimer0Regs.TPRH.all = 0;
	CpuTimer0Regs.TIM.all = 0;
	// 1 = Stop timer, 0 = Start/Restart Timer
	CpuTimer0Regs.TCR.bit.TSS = 0;
	// Reload all counter register with period value:
	CpuTimer0Regs.TCR.bit.TRB = 1;
	// 0 = Disable/ 1 = Enable Timer Interrupt
	CpuTimer0Regs.TCR.bit.TIE = 1;
	// Reset interrupt counters:
	CpuTimer0.InterruptCount = 0;	             	

}	

//
// ConfigCpuTimer - This function initializes the selected timer to the period
// specified by the "Freq" and "Period" parameters. The "Freq" is entered
// as "MHz" and the "Period" in "uSeconds". The timer is held in the stopped
// state after configuration.
void ConfigCpuTimer(struct CPUTIMER_VARS *Timer, float Freq, float Period)
{
    Uint32     PeriodInClocks;

    // Initialize timer period
    Timer->CPUFreqInMHz = Freq;
    Timer->PeriodInUSec = Period;
    PeriodInClocks = (long) (Freq * Period);

    // Counter decrements PRD+1 times each period
    Timer->RegsAddr->PRD.all = PeriodInClocks - 1;

    // Set pre-scale counter to divide by 1 (SYSCLKOUT)
    Timer->RegsAddr->TPR.all  = 0;
    Timer->RegsAddr->TPRH.all  = 0;

    // Initialize timer control register
    // 1 = Stop timer, 0 = Start/Restart Timer
    Timer->RegsAddr->TCR.bit.TSS = 1;

    Timer->RegsAddr->TCR.bit.TRB = 1;       // 1 = reload timer
    Timer->RegsAddr->TCR.bit.SOFT = 0;
    Timer->RegsAddr->TCR.bit.FREE = 0;      // Timer Free Run Disabled

    // 0 = Disable/ 1 = Enable Timer Interrupt
    Timer->RegsAddr->TCR.bit.TIE = 1;

    // Reset interrupt counter
    Timer->InterruptCount = 0;
}

//===========================================================================
// End of file.
//===========================================================================
