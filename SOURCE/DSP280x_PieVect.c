//#########################################################
//
// FILE:	DSP280x_PieVect.c
//
// TITLE:	DSP280x Devices PIE Vector Table Initialization Functions.
//
//#########################################################
// Running on TMS320LF280xA   DCDC part   h741au1(1.00)               
// External clock is 20MHz, PLL * 10/2 , CPU-Clock 100 MHz	      
// Date: from May 23, 2005 to Oct 30, 2006  , (C) www & mhp & mj & lyg
// Version:1.00     Change Date: May 24, 2005 , 
//#########################################################

#include "DSP280x_Device.h"     // DSP280x Headerfile Include File

//---------------------------------------------------------------------------
// InitPieVectTable: 
//---------------------------------------------------------------------------
// This function initializes the PIE vector table to a known state.
// This function must be executed after boot time.
//

void InitPieVectTable(void)
{

	int16	i;
	Uint32 *Source = (void *) &USER_ISR;//USER_ISR;
	Uint32 *Dest = (void *) &PieVectTable;
		
	EALLOW;	
	for(i=0; i < 128; i++)
		*Dest++ = (Uint32)Source;
	EDIS;
	// Enable the PIE Vector Table
	PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
			
}

//===========================================================================
// End of file.
//===========================================================================
