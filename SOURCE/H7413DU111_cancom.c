/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_cancom.c
 *  PURPOSE  : rectifier communicate with controller and other rectifiers	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research 
 *	
 *============================================================================*/
#include "DSP280x_Device.h"		// DSP280x Headerfile Include File
/******************************************************************
    函数名称 :  get_data_all
    入口参数 :  无
    出口参数 :  无
    函数功能 :  读系统配置 ，该命令对模块监控为空命令
    子函数   : 无
******************************************************************/
unsigned char can_com_ok = 0;
unsigned char can_overtime_ok = 0;
void CanRxMb0(void);
void CanRxMb1(void);
void CanRxMb2(void);

interrupt void vCanIsr(void)
{
    //transmit mailbox
    if(ECanaRegs.CANTA.all & 0x0020)
    {
        ECanaRegs.CANTA.all |= 0x0020;   // Clear TAn bits
    }
    else if(ECanaRegs.CANTA.all & 0x0010)
    {
        ECanaRegs.CANTA.all |= 0x0010;   // Clear TAn bits
    }
    else if(ECanaRegs.CANTA.all & 0x0008)
    {
        ECanaRegs.CANTA.all |= 0x0008;   // Clear TAn bits
    }
    //receive mailbox
    if(ECanaRegs.CANRMP.all & 0x0001)//0x070
    {
        CanRxMb0();
        ECanaRegs.CANRMP.all |= 0x0001;  // Clear all RMPn bits
    }
    if(ECanaRegs.CANRMP.all & 0x0002)//0x060
    {
        CanRxMb1();
        ECanaRegs.CANRMP.all |= 0x0002;  // Clear all RMPn bits
    }
    if(ECanaRegs.CANRMP.all & 0x0004)//0x1CA
    {
        CanRxMb2();
        ECanaRegs.CANRMP.all |= 0x0004;  // Clear all RMPn bits
    }
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9; // Issue PIE ack
    ECanaRegs.CANGIF0.bit.GMIF0 = 1; // Clear all interrupt flag bits
}

void CanRxMb0(void)
{
    unsigned int   m,n,ShareCurrTemp1,ShareCurrTemp2;
    unsigned char address1;
    ubitfloat      dccurr1;
    int           delta1;
    unsigned char  ucMsgTypeRx;

    n = ECanaMboxes.MBOX0.MDL.word.LOW_WORD;
    m = ECanaMboxes.MBOX0.MDL.word.HI_WORD;
    dccurr1.intdata[0].id = ECanaMboxes.MBOX0.MDH.word.LOW_WORD;
    dccurr1.intdata[1].id = ECanaMboxes.MBOX0.MDH.word.HI_WORD;

    address1 = (ECanaMboxes.MBOX0.MSGID.all & 0x07f8) >> 3;
    ucMsgTypeRx = (m & 0x7f00) >> 8;

    if (((ucMsgTypeRx == 0x01) || (ucMsgTypeRx == 0x11)) && (address1 <= 110))
    {
        m = n;
        m = m & 0x0fff;
        if ( m >= 0x0800 )
        {
            m = m | 0xf000;
        }
        delta1 = m;
        n = (n&0xf000)>>12;
        mokuai_delta[address1] = (float)delta1;
        mokuai_status[address1] = n;
        if(((g_u16MdlType == RECT)||(g_u16MdlType == EPA)) && (u16ShareMode == REAL_CURR))       //ls 20110527
        {
            mokuai_current[address1] = dccurr1.fd;
        }

        else if((g_u16MdlType == HVDC) || (g_u16MdlType == Converter)
            || (g_u16MdlType == MPPT) || (g_u16MdlType == MPPT_HV)
            ||(((g_u16MdlType == RECT)||(g_u16MdlType == EPA)) && (u16ShareMode == LOAD_PCT)))
        {
            ShareCurrTemp1 = ((((long)dccurr1.intdata[0].id)*(dccurr1.intdata[1].id))>>15); //ls 2011125
            mokuai_current[address1] = (float)ShareCurrTemp1 / 100;
        }

        mokuai_times[address1] = mokuai_times[address1]+1;
    }
    else if((ucMsgTypeRx == 0x02) || (ucMsgTypeRx == 0x12))
    {
        mokuai_main_address = address1;
        if(((g_u16MdlType == RECT) || (g_u16MdlType == EPA)) && (u16ShareMode == REAL_CURR))       /*ls 20110527*/
        {
            avergae_current = dccurr1.fd;
        }
        else if((g_u16MdlType == HVDC) || (g_u16MdlType == Converter)
            || (g_u16MdlType == MPPT)  || (g_u16MdlType == MPPT_HV)
            || (((g_u16MdlType == RECT) || (g_u16MdlType == EPA)) && (u16ShareMode == LOAD_PCT)))
        {
            ShareCurrTemp2 = ((((long)dccurr1.intdata[0].id)*(dccurr1.intdata[1].id))>>15);
            avergae_current = (float)ShareCurrTemp2 / 100;
        }
    }

    g_u8uLight_status = g_u8uLight_status|0x02;
    g_u16light3_time = 0;
}

void CanRxMb1(void)
{
    unsigned int  ValueType;
    unsigned char address1;
    ubitfloat      dccurr1;
    unsigned char  ucErrType;

    g_u16CanIntTimer = 0;

    ValueType = ECanaMboxes.MBOX1.MDL.word.LOW_WORD;
    ucErrType = ECanaMboxes.MBOX1.MDL.word.HI_WORD & 0x00ff;
    dccurr1.intdata[0].id = ECanaMboxes.MBOX1.MDH.word.LOW_WORD;
    dccurr1.intdata[1].id = ECanaMboxes.MBOX1.MDH.word.HI_WORD;

    address1 = (ECanaMboxes.MBOX1.MSGID.all & 0x07f8) >> 3;

    if(address == address1)
    {
        g_u8uLight_status = g_u8uLight_status | 0x01;
        g_u16light3_time = 0;

        if ( ucErrType == 0xf3 )
        {
            can_com_ok = 3;     /* 数据校验错误 */
        }
        else
        {
            if ( ucErrType == 0xf2 )
                can_com_ok = 2;     /* 命令无效 */
            else if ( ucErrType == 0xf4 )
                can_com_ok = 4;     /* 无错误，正常响应 */
            else
                can_com_ok = 5;
            /* Converter采用ACU+ 监控，具体协议号请参见文档：
            GBB CAN通讯协议之北美变换器模块协议V1 01 */
            if (g_u16MdlType == Converter) //zlm for Converter protocol
            {
                switch (ValueType) /*接收到 Converter 的信息*/
                {
                    case GET_VOLT://1
                        dcvolt.fd = dccurr1.fd;
                        break;

                    case 2://2
                        dccurr.fd = dccurr1.fd;
                        dccurr_disp.fd = dccurr.fd ;
                        break;

                    case 3://4
                        temperature.fd = dccurr1.fd;
                        break;

                    case 8://3://zlm 20120418
                        dclimit.fd = dccurr1.fd;
                        break;

                    case 9://6: //zlm 20120418
                        dcvolt_up.fd = dccurr1.fd;
                        break;

                    case 0x04://40
                        status = dccurr1.intdata[1].id;
                        status1= dccurr1.intdata[0].id;
                        break;

                    case 0x0a://0x0f://zlm 20120418
                        dcvolt_float.fd = dccurr1.fd;
                        break;

                    case 0x0f://8://zlm 20120418
                        pfcvolt.fd = dccurr1.fd;
                        break;

                    case 0x10: //input Voltage (采用原来0x05位置)
                        acvolt.fd = dccurr1.fd;
                        break;

                    case 0x11://9://zlm 20120418
                        temp_up.fd = dccurr1.fd;
                        break;

                    case 0x19://0x0b:
                        temp_disp.fd = dccurr1.fd;
                        break;

                    case 0x21:  //54
                        uNodeIdH = dccurr1.intdata[1].id;
                        uNodeIdL = dccurr1.intdata[0].id;
                        break;

                    case 0x05: //55
                    case 0x20:
                        uNodeidH = dccurr1.intdata[1].id;
                        uNodeidL = dccurr1.intdata[0].id;
                        break;

                    case 0x27:  //56
                        softwarever=dccurr1.intdata[0].id;
                        hardwarever=dccurr1.intdata[1].id;
                        break;

                    case 0x06:  //58        /* mhp 04028 */
                        tagRunTime.fd = dccurr1.fd;
                        break;

                    case 0x22:  //5A
                        uBarCode1=dccurr1.intdata[1].id;
                        uBarCode2=dccurr1.intdata[0].id;
                        break;

                    case 0x23:  //5B
                        uBarCode3=dccurr1.intdata[1].id;
                        uBarCode4=dccurr1.intdata[0].id;
                        break;

                    case 0x24:  //5C
                        uBarCode5=dccurr1.intdata[1].id;
                        uBarCode6=dccurr1.intdata[0].id;
                        break;

                    case 0x25:  //5D
                        uBarCode7=dccurr1.intdata[1].id;
                        uBarCode8=dccurr1.intdata[0].id;
                        break;

                    case 0xe0:          /* mhp 04028 */
                        uDcReadRamCon.fd = dccurr1.fd;
                        break;

                    case 0xe8:          /* mhp 04028 */
                        uDcReadRamCon.fd = dccurr1.fd;
                        break;

                    case 0xe4:          /* mhp 04028 */
                        uAcReadRamCon.fd = dccurr1.fd;
                        break;

                    case 0xe9:          /* mhp 04028 */
                        uAcReadRamCon.fd = dccurr1.fd;
                        break;

                    case 0xea:         /*ls 110526*/
                        uDcReadEemCon.fd = dccurr1.fd;
                        break;

                    case 0xeb:         /*ls 110526*/
                        uAcReadEemCon.fd = dccurr1.fd;
                        break;

                    case 0xf3 :        //ls 20150728
                        uFaultRecEemCon.fd = dccurr1.fd;
                        break;

                    case 0xd0:
                        currin.fd = dccurr1.fd;
                        break;

                    case 0xd3:
                        powerin.fd = dccurr1.fd;
                        break;

                    default:
                        break ;
                }
            }
            else  //REC or HVDC or MPPT or IS or MPPT_HV
            {
                switch (ValueType)
                {
                    case 1:
                        dcvolt.fd = dccurr1.fd;
                        break;

                    case 2:
                        dccurr.fd = dccurr1.fd;
                        break;

                    case 3:
                        dclimit.fd = dccurr1.fd;
                        break;

                    case 4:
                        temperature.fd = dccurr1.fd;
                        break;

                    case 5:
                        acvolt.fd = dccurr1.fd;
                        break;

                    case 6:
                        dcvolt_up.fd = dccurr1.fd;
                        break;

                    case 7:
                        dccurr_disp.fd = dccurr1.fd;
                        break;

                    case 8:
                        pfcvolt.fd = dccurr1.fd;
                        break;

                    case 9:
                        if ((g_u16MdlType == MPPT) || (g_u16MdlType == MPPT_HV))
                        {
                            temp_pfc.fd = dccurr1.fd;
                        }
                        else
                        {
                            temp_up.fd = dccurr1.fd;
                        }
                        break;

                    case 0x0a:
                        pfcvolt1.fd = dccurr1.fd;
                        break;

                    case 0x0b:
                        temp_disp.fd = dccurr1.fd;
                        break;

                    case 0x0c:
                        ac1volt.fd = dccurr1.fd;
                        break;

                    case 0x0d:
                        ac2volt.fd = dccurr1.fd;
                        break;

                    case 0x0e:
                        ac3volt.fd = dccurr1.fd;
                        break;

                    case 0x0f:
                        dcvolt_float.fd = dccurr1.fd;
                        break;

                    case 0x10:
                        if ((u16MdlType == MPPT) || (u16MdlType == MPPT_HV))
                        {
                            currin.fd = dccurr1.fd;
                        }
                        else
                        {
                            temp_pfc.fd = dccurr1.fd;
                        }
                        break;

                    // mhp 20130329 add for IS
                    //Byte 7: 广播地址 Byte 6: 逻辑地址
                    case 0x15:
                        u16ISAddr = dccurr1.intdata[0].id;
                        break;

                    case 0x16:
                        if (u16MdlType == IS_RECT)
                        {
                            dcvolthandget.fd = dccurr1.fd;
                        }
                        break;

                    case 0x17:
                        if (u16MdlType == IS_RECT)
                        {
                            dcvoltdnget.fd = dccurr1.fd;
                        }
                        break;

                    case 0x40:
                        status = dccurr1.intdata[1].id;
                        status1= dccurr1.intdata[0].id;
                        break;

                        // mhp add 20130428
                    case 0x42:
                        status2 = dccurr1.intdata[1].id;
                        status3 = dccurr1.intdata[0].id;
                        break;

                    case 0x43:
                        u16RecRunInfH = dccurr1.intdata[1].id;
                        u16RecRunInfL = dccurr1.intdata[0].id;
                        break;

                    case 0x44:
                        u16EpromErrH = dccurr1.intdata[1].id;
                        u16EpromErrL = dccurr1.intdata[0].id;
                        break;

                    case 0x50:          /* mhp 04028 */
                        //delta = (int)(dccurr1.fd);
                        break;

                    case 0x54:
                        uNodeIdH=dccurr1.intdata[1].id;
                        uNodeIdL=dccurr1.intdata[0].id;
                        break;

                    case 0x55:
                        uNodeidH=dccurr1.intdata[1].id;
                        uNodeidL=dccurr1.intdata[0].id;
                        break;

                    case 0x56:
                        softwarever=dccurr1.intdata[0].id;
                        hardwarever=dccurr1.intdata[1].id;
                        break;

                    case 0x57:          // mhp 20130401
                        if (u16MdlType == IS_RECT)
                        {
                            u16WrIdLmtH = dccurr1.intdata[0].id;
                            u16WrIdLmtL = dccurr1.intdata[1].id;
                            if ((u16WrIdLmtH == 0x55AA) && (u16WrIdLmtL == 0x55AA))
                            {
                                u16ISIdUnlock = 1;
                            }
                            else
                            {
                                u16ISIdUnlock = 0;
                            }
                        }
                        else if ((u16MdlType == RECT) || (u16MdlType == HVDC)
                            || (u16MdlType == MPPT_HV) || (u16MdlType == EPA))
                        {
                            pfcswver = dccurr1.intdata[0].id;
                            pfcswbom = dccurr1.intdata[1].id;
                        }
                        break;

                    case 0x58:          /* mhp 04028 */
                        tagRunTime.fd = dccurr1.fd;
                        break;

                    case 0x5A:
                        uBarCode1=dccurr1.intdata[1].id;
                        uBarCode2=dccurr1.intdata[0].id;
                        break;

                    case 0x5B:
                        uBarCode3=dccurr1.intdata[1].id;
                        uBarCode4=dccurr1.intdata[0].id;
                        break;

                    case 0x5C:
                        uBarCode5=dccurr1.intdata[1].id;
                        uBarCode6=dccurr1.intdata[0].id;
                        break;

                    case 0x5D:
                        uBarCode7=dccurr1.intdata[1].id;
                        uBarCode8=dccurr1.intdata[0].id;
                        break;

                    case 0xd0:
                        currin.fd = dccurr1.fd;
                        break;

                    case 0xd1:
                        currinB.fd = dccurr1.fd;
                        break;

                    case 0xd2:
                        currinC.fd = dccurr1.fd;
                        break;

                    case 0xd3:
                        powerin.fd = dccurr1.fd;
                        break;

                    case 0xd4:
                        e_power.fd = dccurr1.fd;
                        break;

                    case 0xd5:
                        frequencyin.fd = dccurr1.fd;
                        break;

                    case 0xd6:
                        efficiency.fd = dccurr1.fd;
                        break;

                    case 0xe0:          /* mhp 04028 */
                        uDcReadRamCon.fd = dccurr1.fd;
                        break;

                    case 0xe8:          /* mhp 04028 */
                        uDcReadRamCon.fd = dccurr1.fd;
                        break;

                    case 0xe4:          /* mhp 04028 */
                        uAcReadRamCon.fd = dccurr1.fd;
                        break;

                    case 0xe9:          /* mhp 04028 */
                        uAcReadRamCon.fd = dccurr1.fd;
                        break;

                    case 0xea:         /*ls 110526*/
                        uDcReadEemCon.fd = dccurr1.fd;
                        break;

                    case 0xeb:         /*ls 110526*/
                        uAcReadEemCon.fd = dccurr1.fd;
                        break;

                    case 0xf3 :        //ls 20150728
                        uFaultRecEemCon.fd = dccurr1.fd;
                        break;

                    default:
                        break ;
                }
            }
        }
    }
}

void CanRxMb2(void)
{
    u16LoadSize = ECanaMboxes.MBOX2.MDH.word.LOW_WORD;    // size
    u16LoadAddrLL = ECanaMboxes.MBOX2.MDH.word.HI_WORD;    //addrL

    u16LoadStatus = ECanaMboxes.MBOX2.MDL.word.HI_WORD;  //download status
    u16LoadAddrH = ECanaMboxes.MBOX2.MDL.word.LOW_WORD;  //addrH

    g_u16DataBuffer0[1] = SGET_DOWNLOAD_ANSWER;
    Flag_TrigFd = 1;
    vSciDataParsing();
}

unsigned char get_can_data(unsigned char start_cid,unsigned char len)
{
    unsigned char checki,i,k,lenlen;

    checki = 0;

    if(g_u16MdlType == Converter)//zlm for converter
    {
        if(ucMsgType==0x00)
            lenlen=7;
        else if(ucMsgType==0x10)
            lenlen=4;
        else if(ucMsgType==0x20)
            lenlen=7;
        else
            lenlen=1;
     }
     else
     {
        if(ucMsgType == 0x00)
            lenlen=9;
        else if(ucMsgType == 0x10)
            lenlen=4;
        else if(ucMsgType == 0x20)
            lenlen=11;
        else if(ucMsgType == 0x30)
            lenlen=9;
        else
            lenlen=1;
     }

    for (i=0; i<len; i++)
    {
        can_com_ok = 0;
        can_overtime_ok = 0;
        com_can(start_cid + i);

        for (k=0; k<lenlen; k++)
        {
            while (can_com_ok == 0)
            {
                if(can_overtime_ok >= 20) //20
                {
                    if((ucMsgType == 0x10)||(ucMsgType == 0x20)||(ucMsgType == 0x30))
                    {
                        can_com_ok = 5;
                    }
                }
                if (can_overtime_ok >= 100)
                {
                    can_com_ok = 1;
                }
                Watch_Dog_Kick();
            }

            if ( can_com_ok == 1 )
            {
                checki = 0xf1;
                g_u8boardaddress = 1;
                //CAN_IFR = 0x10FF;
                //CAN_TCR = 0x4000;
                k=lenlen;
                break;
            }
//            else if ( can_com_ok == 2 )
//            {
//                if((tmpMsgType!=0x00)&&(tmpMsgType!=0x10)&&(tmpMsgType!=0x20)&&(tmpMsgType!=0x30))
//                {
//                    checki = 0xf2;
//                    k=lenlen;
//                    break;
//                }
//            }
//            else if ( can_com_ok == 3 )
//            {
//                checki = 0xf3;              /* CRCCOM，CRC校验错误//超时??? */
//                k=lenlen;
//                break;
//            }
//            else if ( can_com_ok == 4 )
//            {
//                checki = 0xf4;              /* CRCCOM，CRC校验错误//??? */
//                k=lenlen;
//                break;
//            }
            can_com_ok=0;
            can_overtime_ok=0;
        }
    }
    if(checki)
        g_u16DataBuffer0[1] = checki ;
    return checki;
}


/******************************************************************
    函数名称 :  can_transmit
    入口参数 :  无
    出口参数 :  无
    函数功能 :  监控模块发送命令
    子函数   :     无
    修改  :       mhp 20031223
******************************************************************/
void com_can(UINT16 value_type)
{
    UINT16 value_type_temp = 0;
    UINT16 temp = 0;
    ubitfloat send_data;

    g_u16ErrType = NORMAL;

    if(ucMsgType==0x00)
    {
        if (openstatus_send)
            temp = temp | 0x8000;
        if (resetstatus_send)
            temp = temp | 0x4000;
        if (walkinstatus_send)
            temp = temp | 0x2000;
        if (fullspeedstatus_send)
            temp = temp | 0x1000;
        if (twinklestatus_send)
            temp = temp | 0x0800;
        if (ovrelaystatus_send)
            temp = temp | 0x0400;
        if (uPermitOverAc == 0)
            temp = temp | 0x0100;

        value_type_temp = temp | (UINT16)(volt_adj_delta.fd);
        send_data.intdata[0].id = tagRunTimeSet.intdata[0].id;
        send_data.intdata[1].id = tagRunTimeSet.intdata[1].id;
    }
    else if(ucMsgType==0x10)
    {
        if (acfault_send)
        {
            temp = temp |0x0100;
        }

        value_type_temp = temp;
        send_data.intdata[0].id = tagRunTimeSet.intdata[0].id;
        send_data.intdata[1].id = tagRunTimeSet.intdata[1].id;
    }
    else
    {
        value_type_temp = value_type;
        if(u16MdlType == Converter) //zlm add Converter protocol
        {
            switch (value_type_temp)
            {
            case 0x16 :
                if(estop_permit==1)
                {
                    send_data.intdata[0].id = 0x5A5A;
                    send_data.intdata[1].id = 0;
                }
                else
                {
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = 0xFFFF;
                }
                break;

            case 0x15 ://0x18 :
                send_data.intdata[0].id = dcpower_float.intdata[0].id;
                send_data.intdata[1].id = dcpower_float.intdata[1].id;
                break;

            case 0x0b ://0x19 :
                send_data.intdata[0].id = dclimit_float.intdata[0].id;
                send_data.intdata[1].id = dclimit_float.intdata[1].id;
                break;

            case 0x18 ://0x1a :
                send_data.intdata[0].id = accurr_set.intdata[0].id;
                send_data.intdata[1].id = accurr_set.intdata[1].id;
                break;

            case 0x17 ://0x20 :
                send_data.intdata[0].id = setpower.intdata[0].id;
                send_data.intdata[1].id = setpower.intdata[1].id;
                break;

            case 0x07 ://0x21 :
                send_data.intdata[0].id = setvolt.intdata[0].id;
                send_data.intdata[1].id = setvolt.intdata[1].id;
                break;

            case 0x08 ://0x22 :
                send_data.intdata[0].id = setlimit.intdata[0].id;
                send_data.intdata[1].id = setlimit.intdata[1].id;
                break;

            case 0x0C ://0x23 :
                send_data.intdata[0].id = dcvolt_up.intdata[0].id;
                send_data.intdata[1].id = dcvolt_up.intdata[1].id;
                break;

            case 0x0A ://0x24 :
                send_data.intdata[0].id = dcvolt_float.intdata[0].id;
                send_data.intdata[1].id = dcvolt_float.intdata[1].id;
                break;

            case 0x11 ://0x25 :
                send_data.intdata[0].id = temp_up.intdata[0].id;
                send_data.intdata[1].id = temp_up.intdata[1].id;
                break;

            case 0x12 ://0x26 :
                send_data.intdata[0].id = volt_adj_delta.intdata[0].id;
                send_data.intdata[1].id = volt_adj_delta.intdata[1].id;
                break;

            case 0x0F ://0x27 :
                send_data.intdata[0].id = setpfcvolt.intdata[0].id;
                send_data.intdata[1].id = setpfcvolt.intdata[1].id;
                break;

            case 0x13 ://0x28 :
                send_data.intdata[0].id = tagReontime.intdata[0].id;
                send_data.intdata[1].id = tagReontime.intdata[1].id;
                break;

            case 0x14 ://0x29 :
                send_data.intdata[0].id = tagSstartTime.intdata[0].id;
                send_data.intdata[1].id = tagSstartTime.intdata[1].id;
                break;

            /*case 0x2a : 不控制顺序起机时间
                send_data.intdata[0].id = tagOpenTime.intdata[0].id1;
                send_data.intdata[1].id = tagOpenTime.intdata[1].id1;
                break;

            case 0x2b :     不控制CAN初始化
                send_data.intdata[0].id = dcbaud_float.intdata[0].id1;
                send_data.intdata[1].id = dcbaud_float.intdata[1].id1;
                break;

            case 0x2f : 不控制降额运行
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = AcLimPowModeFlag;
                break;*/

            case 0x80 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = openstatus_send;
                break;

            case 0x81 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = resetstatus_send;
                break;

            case 0x82 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = walkinstatus_send;
                break;

            case 0x83 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = fullspeedstatus_send;
                break;

            case 0x84 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = twinklestatus_send;
                break;

            case 0x85 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = ovrelaystatus_send;
                break;

            case 0x87 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = permitadjest;
                break;

            case 0x88 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = bigfan;
                break;

            case 0x89 :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = overvoltprot;
                break;

            case 0x8A :
                if(uPermitOverAc==1)
                {
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = 0;
                }
                else
                {
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = 1;
                }
                break;

            case 0x8B :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = regstatus_send;
                break;

            case 0x8D :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = acfault_send;
                break;

            case 0x8E :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = uPermitOverDc;
                break;

            case 0x60 :
                send_data.intdata[0].id = dcvoltsamp_sys_a.intdata[0].id;
                send_data.intdata[1].id = dcvoltsamp_sys_a.intdata[1].id;
                break;

            case 0x61 :
                send_data.intdata[0].id = dcvoltsamp_sys_b.intdata[0].id;
                send_data.intdata[1].id = dcvoltsamp_sys_b.intdata[1].id;
                break;

            case 0x62 :
                send_data.intdata[0].id = dccurrsamp_sys_a.intdata[0].id;
                send_data.intdata[1].id = dccurrsamp_sys_a.intdata[1].id;
                break;

            case 0x63 :
                send_data.intdata[0].id = dccurrsamp_sys_b.intdata[0].id;
                send_data.intdata[1].id = dccurrsamp_sys_b.intdata[1].id;
                break;

            case 0x64 :
                send_data.intdata[0].id = dcvoltcontl_sys_a.intdata[0].id;
                send_data.intdata[1].id = dcvoltcontl_sys_a.intdata[1].id;
                break;

            case 0x65 :
                send_data.intdata[0].id = dcvoltcontl_sys_b.intdata[0].id;
                send_data.intdata[1].id = dcvoltcontl_sys_b.intdata[1].id;
                break;

            case 0x66 :
                send_data.intdata[0].id = dccurrcontl_sys_a.intdata[0].id;
                send_data.intdata[1].id = dccurrcontl_sys_a.intdata[1].id;
                break;

            case 0x67 :
                send_data.intdata[0].id = dccurrcontl_sys_b.intdata[0].id;
                send_data.intdata[1].id = dccurrcontl_sys_b.intdata[1].id;
                break;

            case 0x68 :
                send_data.intdata[0].id = acvoltsamp_sys_a.intdata[0].id;
                send_data.intdata[1].id = acvoltsamp_sys_a.intdata[1].id;
                break;

            case 0x69 :
                send_data.intdata[0].id = acvoltsamp_sys_b.intdata[0].id;
                send_data.intdata[1].id = acvoltsamp_sys_b.intdata[1].id;
                break;

            case 0x6a :
                send_data.intdata[0].id = ac1voltsamp_sys_a.intdata[0].id;
                send_data.intdata[1].id = ac1voltsamp_sys_a.intdata[1].id;
                break;

            case 0x6b :
                send_data.intdata[0].id = ac1voltsamp_sys_b.intdata[0].id;
                send_data.intdata[1].id = ac1voltsamp_sys_b.intdata[1].id;
                break;

            case 0x6c :
                send_data.intdata[0].id = pfcvoltsamp_sys.intdata[0].id;
                send_data.intdata[1].id = pfcvoltsamp_sys.intdata[1].id;
                break;

            case 0x6d :
                send_data.intdata[0].id = pfcvoltcon_sys.intdata[0].id;
                send_data.intdata[1].id = pfcvoltcon_sys.intdata[1].id;
                break;

            case 0x6e:
                send_data.intdata[0].id = tempsamp_sys.intdata[0].id;
                send_data.intdata[1].id = tempsamp_sys.intdata[1].id;
                break;

            case 0x70 :
                send_data.intdata[0].id = dcpowercontl_sys_a.intdata[0].id;
                send_data.intdata[1].id = dcpowercontl_sys_a.intdata[1].id;
                break;

            case 0x71 :
                send_data.intdata[0].id = dcpowercontl_sys_b.intdata[0].id;
                send_data.intdata[1].id = dcpowercontl_sys_b.intdata[1].id;
                break;

            case 0x72 :
                send_data.intdata[0].id = ac2voltsamp_sys_a.intdata[0].id;
                send_data.intdata[1].id = ac2voltsamp_sys_a.intdata[1].id;
                break;

            case 0x73 :
                send_data.intdata[0].id = ac2voltsamp_sys_b.intdata[0].id;
                send_data.intdata[1].id = ac2voltsamp_sys_b.intdata[1].id;
                break;

            case 0x78 :
                send_data.intdata[0].id = fancon_sys_a.intdata[0].id;
                send_data.intdata[1].id = fancon_sys_a.intdata[1].id;
                break;

            case 0x79 :
                send_data.intdata[0].id = fancon_sys_b.intdata[0].id;
                send_data.intdata[1].id = fancon_sys_b.intdata[1].id;
                break;

            case 0xe0 :
                send_data.intdata[0].id = uDcReadRamAdd.intdata[0].id;
                send_data.intdata[1].id = uDcReadRamAdd.intdata[1].id;
                break;

            case 0xe8 :
                send_data.intdata[0].id = uDcReadRamAdd.intdata[0].id;
                send_data.intdata[1].id = uDcReadRamAdd.intdata[1].id;
                break;

            case 0xe1 :
                send_data.intdata[0].id = uDcWriteRamCon.intdata[0].id;
                send_data.intdata[1].id = uDcWriteRamCon.intdata[1].id;
                break;

            case 0xe2 :
                send_data.intdata[0].id = uDcWriteEemAdd.intdata[0].id;
                send_data.intdata[1].id = uDcWriteEemAdd.intdata[1].id;
                break;

            case 0xe3 :
                send_data.intdata[0].id = uDcWriteEemCon.intdata[0].id;
                send_data.intdata[1].id = uDcWriteEemCon.intdata[1].id;
                break;

            case 0xe4 :
                send_data.intdata[0].id = uAcReadRamAdd.intdata[0].id;
                send_data.intdata[1].id = uAcReadRamAdd.intdata[1].id;
                break;

            case 0xe9 :
                send_data.intdata[0].id = uAcReadRamAdd.intdata[0].id;
                send_data.intdata[1].id = uAcReadRamAdd.intdata[1].id;
                break;

            case 0xe5 :
                send_data.intdata[0].id = uAcWriteRamCon.intdata[0].id;
                send_data.intdata[1].id = uAcWriteRamCon.intdata[1].id;
                break;

            case 0xe6 :
                send_data.intdata[0].id = uAcWriteEemAdd.intdata[0].id;
                send_data.intdata[1].id = uAcWriteEemAdd.intdata[1].id;
                break;

            case 0xe7 :
                send_data.intdata[0].id = uAcWriteEemCon.intdata[0].id;
                send_data.intdata[1].id = uAcWriteEemCon.intdata[1].id;
                break;

            case 0xea :        /*ls 110526*/
                send_data.intdata[0].id = uDcReadEemAdd.intdata[0].id;
                send_data.intdata[1].id = uDcReadEemAdd.intdata[1].id;
                break;

            case 0xeb :        /*ls 110526*/
                send_data.intdata[0].id = uAcReadEemAdd.intdata[0].id;
                send_data.intdata[1].id = uAcReadEemAdd.intdata[1].id;
                break;

            case 0xa0:          /* set Mdl node ID */
                send_data.intdata[0].id = uNodeIdL;
                send_data.intdata[1].id = uNodeIdH;
                break;

            case 0xa1:          /* set Mdl node ID1 */
                send_data.intdata[0].id = uNodeidL;
                send_data.intdata[1].id = uNodeidH;
                break;

            case 0xa2:          /* set Mdl hw/sw version number */
                send_data.intdata[0].id = softwarever;
                send_data.intdata[1].id = hardwarever;
                break;

            case 0xa4:          /* set the whole run time */
                send_data.intdata[0].id = tagRunTimeSet.intdata[0].id;
                send_data.intdata[1].id = tagRunTimeSet.intdata[1].id;
                break;

            case 0xa5:          /* set the whole maintain times */
                send_data.intdata[0].id = tagMaintainTimes.intdata[0].id;
                send_data.intdata[1].id = tagMaintainTimes.intdata[1].id;
                break;

            case 0xa6:          /* set the first maintain time and contain */
                send_data.intdata[0].id = uBarCode2;
                send_data.intdata[1].id = uBarCode1;
                break;

            case 0xa7:          /* set the second tmaintain time and contain */
                send_data.intdata[0].id = uBarCode4;
                send_data.intdata[1].id = uBarCode3;
                break;

            case 0xa8:          /* set the third maintain time and contain */
                send_data.intdata[0].id = uBarCode6;
                send_data.intdata[1].id = uBarCode5;
                break;

            case 0xa9:          /* set the fourth tmaintain time and contain */
                send_data.intdata[0].id = uBarCode8;
                send_data.intdata[1].id = uBarCode7;
                break;

            case 0xf3 :        //ls 20150728
                send_data.intdata[0].id = uFaultRecEemAdd.intdata[0].id;
                send_data.intdata[1].id = uFaultRecEemAdd.intdata[1].id;
                break;

            // V314add mhp 20130426
            case 0xFC :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = uSetCpuRst;
                break;

            case 0xFE :
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = download_permit;
                break;

            case 0xFF :
                send_data.intdata[0].id = downloadcon;
                send_data.intdata[1].id = downloadadd;
                break;

            default:
                send_data.intdata[0].id = 0;
                send_data.intdata[1].id = 0;
                break ;
            }
        }
        else
        {
            switch (value_type_temp)
            {
                case 0x16 :
                    if(estop_permit==1)
                    {
                        send_data.intdata[0].id = 0x5A5A;
                        send_data.intdata[1].id = 0;
                    }
                    else
                    {
                        send_data.intdata[0].id = 0;
                        send_data.intdata[1].id = 0xFFFF;
                    }
                    break;

                case 0x18 :
                    send_data.intdata[0].id = dcpower_float.intdata[0].id;
                    send_data.intdata[1].id = dcpower_float.intdata[1].id;
                    break;

                case 0x19 :
                    send_data.intdata[0].id = dclimit_float.intdata[0].id;
                    send_data.intdata[1].id = dclimit_float.intdata[1].id;
                    break;

                case 0x1a :
                    send_data.intdata[0].id = accurr_set.intdata[0].id;
                    send_data.intdata[1].id = accurr_set.intdata[1].id;
                    break;

                case 0x20 :
                    send_data.intdata[0].id = setpower.intdata[0].id;
                    send_data.intdata[1].id = setpower.intdata[1].id;
                    break;

                case 0x21 :
                    send_data.intdata[0].id = setvolt.intdata[0].id;
                    send_data.intdata[1].id = setvolt.intdata[1].id;
                    break;

                case 0x22 :
                    send_data.intdata[0].id = setlimit.intdata[0].id;
                    send_data.intdata[1].id = setlimit.intdata[1].id;
                    break;

                case 0x23 :
                    send_data.intdata[0].id = dcvolt_up.intdata[0].id;
                    send_data.intdata[1].id = dcvolt_up.intdata[1].id;
                    break;

                case 0x24 :
                    send_data.intdata[0].id = dcvolt_float.intdata[0].id;
                    send_data.intdata[1].id = dcvolt_float.intdata[1].id;
                    break;

                case 0x25 :
                    send_data.intdata[0].id = temp_up.intdata[0].id;
                    send_data.intdata[1].id = temp_up.intdata[1].id;
                    break;

                case 0x26 :
                    send_data.intdata[0].id = volt_adj_delta.intdata[0].id;
                    send_data.intdata[1].id = volt_adj_delta.intdata[1].id;
                    break;

                case 0x27 :
                    send_data.intdata[0].id = setpfcvolt.intdata[0].id;
                    send_data.intdata[1].id = setpfcvolt.intdata[1].id;
                    break;

                case 0x28 :
                    send_data.intdata[0].id = tagReontime.intdata[0].id;
                    send_data.intdata[1].id = tagReontime.intdata[1].id;
                    break;

                case 0x29 :
                    send_data.intdata[0].id = tagSstartTime.intdata[0].id;
                    send_data.intdata[1].id = tagSstartTime.intdata[1].id;
                    break;

                case 0x2a :
                    send_data.intdata[0].id = tagOpenTime.intdata[0].id;
                    send_data.intdata[1].id = tagOpenTime.intdata[1].id;
                    break;

                case 0x2b :
                    send_data.intdata[0].id = dcbaud_float.intdata[0].id;
                    send_data.intdata[1].id = dcbaud_float.intdata[1].id;
                    break;

                // 20130329 mhp add for IS rect
                case 0x2c :
                    send_data.intdata[0].id = dcvoltset.intdata[0].id;
                    send_data.intdata[1].id = dcvoltset.intdata[1].id;
                    break;

                case 0x2d :
                    if(g_u16MdlType == IS_RECT)
                    {
                        send_data.intdata[0].id = dcvoltdn.intdata[0].id;
                        send_data.intdata[1].id = dcvoltdn.intdata[1].id;
                    }
                    else
                    {
                        send_data.intdata[0].id = fandutyset.intdata[0].id;
                        send_data.intdata[1].id = fandutyset.intdata[1].id;
                    }
                    break;

                case 0x2f :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = (DCinMarnFlag << 4)| (AcLimPowModeFlag << 2);
                    break;

                case 0x30 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = openstatus_send;
                    break;

                case 0x31 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = resetstatus_send;
                    break;

                case 0x32 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = walkinstatus_send;
                    break;

                case 0x33 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = fullspeedstatus_send;
                    break;

                case 0x34 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = twinklestatus_send;
                    break;

                case 0x35 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = ovrelaystatus_send;
                    break;

                case 0x9F :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = ovrelaystatus_send;
                    break;


                case 0x37 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = permitadjest;
                    break;

                case 0x38 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = bigfan;
                    break;

                case 0x39 :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = overvoltprot;
                    break;

                case 0x3A :
                    if(uPermitOverAc==1)
                        {
                        send_data.intdata[0].id = 0;
                        send_data.intdata[1].id = 0;
                        }
                    else
                        {
                        send_data.intdata[0].id = 0;
                        send_data.intdata[1].id = 1;
                        }
                    break;

                case 0x3B :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = regstatus_send;
                    break;

                case 0x3D :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = acfault_send;
                    break;

                case 0x3E :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = uPermitOverDc;
                    break;

                case 0x60 :
                    send_data.intdata[0].id = dcvoltsamp_sys_a.intdata[0].id;
                    send_data.intdata[1].id = dcvoltsamp_sys_a.intdata[1].id;
                    break;

                case 0x61 :
                    send_data.intdata[0].id = dcvoltsamp_sys_b.intdata[0].id;
                    send_data.intdata[1].id = dcvoltsamp_sys_b.intdata[1].id;
                    break;

                case 0x62 :
                    send_data.intdata[0].id = dccurrsamp_sys_a.intdata[0].id;
                    send_data.intdata[1].id = dccurrsamp_sys_a.intdata[1].id;
                    break;

                case 0x63 :
                    send_data.intdata[0].id = dccurrsamp_sys_b.intdata[0].id;
                    send_data.intdata[1].id = dccurrsamp_sys_b.intdata[1].id;
                    break;

                case 0x64 :
                    send_data.intdata[0].id = dcvoltcontl_sys_a.intdata[0].id;
                    send_data.intdata[1].id = dcvoltcontl_sys_a.intdata[1].id;
                    break;

                case 0x65 :
                    send_data.intdata[0].id = dcvoltcontl_sys_b.intdata[0].id;
                    send_data.intdata[1].id = dcvoltcontl_sys_b.intdata[1].id;
                    break;

                case 0x66 :
                    send_data.intdata[0].id = dccurrcontl_sys_a.intdata[0].id;
                    send_data.intdata[1].id = dccurrcontl_sys_a.intdata[1].id;
                    break;

                case 0x67 :
                    send_data.intdata[0].id = dccurrcontl_sys_b.intdata[0].id;
                    send_data.intdata[1].id = dccurrcontl_sys_b.intdata[1].id;
                    break;

                case 0x68 :
                    send_data.intdata[0].id = acvoltsamp_sys_a.intdata[0].id;
                    send_data.intdata[1].id = acvoltsamp_sys_a.intdata[1].id;
                    break;

                case 0x69 :
                    send_data.intdata[0].id = acvoltsamp_sys_b.intdata[0].id;
                    send_data.intdata[1].id = acvoltsamp_sys_b.intdata[1].id;
                    break;

                case 0x6a :
                    send_data.intdata[0].id = ac1voltsamp_sys_a.intdata[0].id;
                    send_data.intdata[1].id = ac1voltsamp_sys_a.intdata[1].id;
                    break;

                case 0x6b :
                    send_data.intdata[0].id = ac1voltsamp_sys_b.intdata[0].id;
                    send_data.intdata[1].id = ac1voltsamp_sys_b.intdata[1].id;
                    break;

                case 0x6c :
                    send_data.intdata[0].id = pfcvoltsamp_sys.intdata[0].id;
                    send_data.intdata[1].id = pfcvoltsamp_sys.intdata[1].id;
                    break;

                case 0x6d :
                    send_data.intdata[0].id = pfcvoltcon_sys.intdata[0].id;
                    send_data.intdata[1].id = pfcvoltcon_sys.intdata[1].id;
                    break;

                case 0x6e:
                    send_data.intdata[0].id = tempsamp_sys.intdata[0].id;
                    send_data.intdata[1].id = tempsamp_sys.intdata[1].id;
                    break;

                case 0x70 :
                    send_data.intdata[0].id = dcpowercontl_sys_a.intdata[0].id;
                    send_data.intdata[1].id = dcpowercontl_sys_a.intdata[1].id;
                    break;

                case 0x71 :
                    send_data.intdata[0].id = dcpowercontl_sys_b.intdata[0].id;
                    send_data.intdata[1].id = dcpowercontl_sys_b.intdata[1].id;
                    break;

                case 0x72 :
                    send_data.intdata[0].id = ac2voltsamp_sys_a.intdata[0].id;
                    send_data.intdata[1].id = ac2voltsamp_sys_a.intdata[1].id;
                    break;

                case 0x73 :
                    send_data.intdata[0].id = ac2voltsamp_sys_b.intdata[0].id;
                    send_data.intdata[1].id = ac2voltsamp_sys_b.intdata[1].id;
                    break;

                case 0x76 :
                    send_data.intdata[0].id = currinsamp_sys_a.intdata[0].id;
                    send_data.intdata[1].id = currinsamp_sys_a.intdata[1].id;
                    break;

                case 0x77 :
                    send_data.intdata[0].id = currinsamp_sys_b.intdata[0].id;
                    send_data.intdata[1].id = currinsamp_sys_b.intdata[1].id;
                    break;

                case 0x78 :
                    send_data.intdata[0].id = fancon_sys_a.intdata[0].id;
                    send_data.intdata[1].id = fancon_sys_a.intdata[1].id;
                    break;

                case 0x79 :
                    send_data.intdata[0].id = fancon_sys_b.intdata[0].id;
                    send_data.intdata[1].id = fancon_sys_b.intdata[1].id;
                    break;

                case 0x80 :
                    send_data.intdata[0].id = currinBsamp_sys_a.intdata[0].id;
                    send_data.intdata[1].id = currinBsamp_sys_a.intdata[1].id;
                    break;

                case 0x81 :
                    send_data.intdata[0].id = currinBsamp_sys_b.intdata[0].id;
                    send_data.intdata[1].id = currinBsamp_sys_b.intdata[1].id;
                    break;

                case 0x82 :
                    send_data.intdata[0].id = currinCsamp_sys_a.intdata[0].id;
                    send_data.intdata[1].id = currinCsamp_sys_a.intdata[1].id;
                    break;

                case 0x83 :
                    send_data.intdata[0].id = currinCsamp_sys_b.intdata[0].id;
                    send_data.intdata[1].id = currinCsamp_sys_b.intdata[1].id;
                    break;

                case 0x9E :
                    send_data.intdata[0].id = u16LEDFlag;
                    send_data.intdata[1].id = 0;
                    break;

                case 0xe0 :
                    send_data.intdata[0].id = uDcReadRamAdd.intdata[0].id;
                    send_data.intdata[1].id = uDcReadRamAdd.intdata[1].id;
                    break;

                case 0xe8 :
                    send_data.intdata[0].id = uDcReadRamAdd.intdata[0].id;
                    send_data.intdata[1].id = uDcReadRamAdd.intdata[1].id;
                    break;

                case 0xe1 :
                    send_data.intdata[0].id = uDcWriteRamCon.intdata[0].id;
                    send_data.intdata[1].id = uDcWriteRamCon.intdata[1].id;
                    break;

                case 0xe2 :
                    send_data.intdata[0].id = uDcWriteEemAdd.intdata[0].id;
                    send_data.intdata[1].id = uDcWriteEemAdd.intdata[1].id;
                    break;

                case 0xe3 :
                    send_data.intdata[0].id = uDcWriteEemCon.intdata[0].id;
                    send_data.intdata[1].id = uDcWriteEemCon.intdata[1].id;
                    break;

                case 0xe4 :
                    send_data.intdata[0].id = uAcReadRamAdd.intdata[0].id;
                    send_data.intdata[1].id = uAcReadRamAdd.intdata[1].id;
                    break;

                case 0xe9 :
                    send_data.intdata[0].id = uAcReadRamAdd.intdata[0].id;
                    send_data.intdata[1].id = uAcReadRamAdd.intdata[1].id;
                    break;

                case 0xe5 :
                    send_data.intdata[0].id = uAcWriteRamCon.intdata[0].id;
                    send_data.intdata[1].id = uAcWriteRamCon.intdata[1].id;
                    break;

                case 0xe6 :
                    send_data.intdata[0].id = uAcWriteEemAdd.intdata[0].id;
                    send_data.intdata[1].id = uAcWriteEemAdd.intdata[1].id;
                    break;

                case 0xe7 :
                    send_data.intdata[0].id = uAcWriteEemCon.intdata[0].id;
                    send_data.intdata[1].id = uAcWriteEemCon.intdata[1].id;
                    break;

                case 0xea :        /*ls 110526*/
                    send_data.intdata[0].id = uDcReadEemAdd.intdata[0].id;
                    send_data.intdata[1].id = uDcReadEemAdd.intdata[1].id;
                    break;

                case 0xeb :        /*ls 110526*/
                    send_data.intdata[0].id = uAcReadEemAdd.intdata[0].id;
                    send_data.intdata[1].id = uAcReadEemAdd.intdata[1].id;
                    break;

                case 0xa0:          /* set Mdl node ID */
                    send_data.intdata[0].id = uNodeIdL;
                    send_data.intdata[1].id = uNodeIdH;
                    break;

                case 0xa1:          /* set Mdl node ID1 */
                    //send_data.intdata[0].id = uNodeId1L;
                    //send_data.intdata[1].id = uNodeId1H;
                    break;

                case 0xa2:          /* set Mdl hw/sw version number */
                    send_data.intdata[0].id = softwarever;
                    send_data.intdata[1].id = hardwarever;
                    break;

                // 20130329 mhp add for IS rect
                case 0xA3 :
                    send_data.intdata[0].id = u16EnWrIDLo;
                    send_data.intdata[1].id = u16EnWrIDHi;
                    break;

                case 0xa4:          /* set the whole run time */
                    send_data.intdata[0].id = tagRunTimeSet.intdata[0].id;
                    send_data.intdata[1].id = tagRunTimeSet.intdata[1].id;
                    break;

                case 0xa5:          /* set the whole maintain times */
                    send_data.intdata[0].id = tagMaintainTimes.intdata[0].id;
                    send_data.intdata[1].id = tagMaintainTimes.intdata[1].id;
                    break;

                case 0xa6:          /* set the first maintain time and contain */
                    send_data.intdata[0].id = uBarCode2;
                    send_data.intdata[1].id = uBarCode1;
                    break;

                case 0xa7:          /* set the second tmaintain time and contain */
                    send_data.intdata[0].id = uBarCode4;
                    send_data.intdata[1].id = uBarCode3;
                    break;

                case 0xa8:          /* set the third maintain time and contain */
                    send_data.intdata[0].id = uBarCode6;
                    send_data.intdata[1].id = uBarCode5;
                    break;

                case 0xa9:          /* set the fourth tmaintain time and contain */
                    send_data.intdata[0].id = uBarCode8;
                    send_data.intdata[1].id = uBarCode7;
                    break;

                case 0xEC:          /* clear calibration parameter */
                    send_data.intdata[0].id = u16ClrEpromFlag;
                    send_data.intdata[1].id = 0;
                    break;

                case 0xf3 :        //ls 20150728
                    send_data.intdata[0].id = uFaultRecEemAdd.intdata[0].id;
                    send_data.intdata[1].id = uFaultRecEemAdd.intdata[1].id;
                break;

                // V314add mhp 20130426
                case 0xFC :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = uSetCpuRst;
                    break;

                case 0xFE :
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = download_permit;
                    break;

                case 0xFF :
                    send_data.intdata[0].id = downloadcon;
                    send_data.intdata[1].id = downloadadd;
                    break;

                default:
                    send_data.intdata[0].id = 0;
                    send_data.intdata[1].id = 0;
                    break ;
                }
            }
    }
    sEncodeCanTxData(ucMsgType, g_u16ErrType, value_type_temp, send_data);
    sCanHdSend4(&g_stCanTxData);
}

void DownloadAddrSend(UINT16 data_len)
{
    if(data_len == 4)
    {
        //CAN_MBX5B = u16LoadAddr;
        //CAN_MBX5A = u16LoadSize;
        sEncodeCanTxU16Data(u16LoadSize, u16LoadAddr, 0, 0);
    }
    else if(data_len == 6)
    {
//        CAN_MBX5B = u16LoadAddr;
//        CAN_MBX5A = u16LoadAddrL;
//        CAN_MBX5D = u16LoadSize;
        sEncodeCanTxU16Data(u16LoadAddrL, u16LoadAddr, 0, u16LoadSize);
    }
    else if(data_len == 8)
    {
//        CAN_MBX5B = u16LoadAddr;
//        CAN_MBX5A = u16LoadAddrL;
//        CAN_MBX5D = u16LoadSize;
//        CAN_MBX5C = u16LoadAddrH;
        sEncodeCanTxU16Data(u16LoadAddrL, u16LoadAddr, u16LoadAddrH, u16LoadSize);
    }
    else if(data_len == 2)
    {
        //CAN_MBX5B = u16LoadAddr;
        sEncodeCanTxU16Data(0, u16LoadAddr, 0, 0);
    }
    sCanHdSend5(&g_stCanTxData);
}


