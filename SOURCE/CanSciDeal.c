/*******************************************************************
 * 					Emerson Network Power Co., Ltd.
 * 
 *	FILENAME: H1P18U211_PFCSciDownload.c
 *
 *	DESCRIPTION: For the 2nd DSP bootdownload in two DSPs system	
 *
 *	AUTHOR:	pevin
 *
 *	HISTORY: V1.0.0(2011.10.31)
 *	
 *******************************************************************/
#include "DSP280x_Device.h"     // DSP280x Headerfile Include File
#include "CanSciDeal.h"

/*******************************************************************************
	Function name:	ucAscToHex
	Parameters :	ascl,asch--ASCII code
	returns :		HEX code
	Description:	two ASCII code combine to according one HEX code 
	subfunction	:	null
*******************************************************************************/
UINT8 ucAscToHex(UINT8 ascl, UINT8	asch)
{
	if ( asch > 0x3a )
	{
		asch = asch - 0x37;			// 41h(A)---10
	}
	else
	{
		asch = asch	- 0x30;
	}
	asch = asch <<4	;
	
	if ( ascl > 0x3a )
	{
		ascl = ascl - 0x37;			// 41h(A)---10
	}
	else
	{
		ascl = ascl - 0x30;
	}
	asch = asch | ascl;
	
	return asch;
}
/*******************************************************************************
	Function name:	uiHexToAsc
	Parameters :	hex--HEX code 
	returns :		ASCII code
	Description:	one HEX code transfer two ASCII code saved using INT type
	subfunction	 :	null
*******************************************************************************/
Uint16 uiHexToAsc(UINT8 hex )
{
    ubitint	ascreg ;

	ascreg.bitdata.highchar = hex & 0x0f ;
	if ( ascreg.bitdata.highchar >= 10 )
	{
		ascreg.bitdata.highchar = ascreg.bitdata.highchar + 0x37 ;	// 37h
	}
	else 
	{
		ascreg.bitdata.highchar = ascreg.bitdata.highchar + 0x30 ;	// 30h
	}
		
	ascreg.bitdata.lowchar = hex >> 4	;
	if ( ascreg.bitdata.lowchar >= 10 )
	{
		ascreg.bitdata.lowchar = ascreg.bitdata.lowchar + 0x37 ;	// 37h
	}
	else
	{
		ascreg.bitdata.lowchar = ascreg.bitdata.lowchar + 0x30 ;	// 30h
	}
	return ascreg.id;
}

interrupt void vSciDataT( void )
{
    if (SciaRegs.SCIFFTX.bit.TXFFINT)
    {
        if ( (g_u16SciRxPoint >= (translate_length + 1)) || (translate_length > 1000) )
        {
            g_u16SciRxPoint = 0;
            scia_enableRX_disableTX();
        }
        else
        {
            SciaRegs.SCITXBUF = g_u16DataBuffer0[g_u16SciRxPoint++];
        }
        g_u8uLight_status = g_u8uLight_status | 0x08;
        g_u16light2_time = 0;
    }
    SciaRegs.SCIFFTX.bit.TXFFINTCLR = 1;           // Clear Interrupt flag
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;       // Issue PIE ack
}

/********************************************************************
函数名称 :  vSciDataR
入口参数 :  无
出口参数 :  无
函数功能 :  SCIA接收到数据产生中断，在中断里对相关数据进行处理
子函数   : mSciErrChk()、InitSciPointer()、ucAscToHex()、ucAscToHex
*********************************************************************/
interrupt void vSciDataR( void )
{
    UINT8 t_u8SciData,t_u8SciAddtemp;

    Watch_Dog_Kick();

    t_u8SciData = SciaRegs.SCIRXBUF.all & 0x00FF;
    if( g_u16SciRxPoint > 1024 )
    {
        g_u16SciRxPoint = 0;
        g_u8synchronization = 0;
        g_u8boardaddress = 0;
        // return;
    }

    g_u16DataBuffer0[ g_u16SciRxPoint ++] = t_u8SciData;
    //确保帧起始码为0x7E，并且帧中间不会出现0x7E
    if ( ( g_u16SciRxPoint == 0x01 ) || ( t_u8SciData == SCI_FIRST_CHAR ) )
    {
        if ( g_u16SciRxPoint != 0x01 )
        {
            g_u16SciRxPoint = 1;
        }
        if ( t_u8SciData != SCI_FIRST_CHAR )
        {
            g_u16SciRxPoint = 0;
        }
    }
    /* 确保数据包2，3字节为有效地址。*/
    else if ( g_u16SciRxPoint == 0x03 )
    {
        t_u8SciAddtemp = ucAscToHex(g_u16DataBuffer0[1],g_u16DataBuffer0[2]);
        if(t_u8SciAddtemp <= 110)
        {
            if(g_u8MdlAddresss!= t_u8SciAddtemp)
            {
                g_u8MdlAddresss= t_u8SciAddtemp;

                if(g_u16MdlType == HVDC)
                {
                    hvdc_can_init();
                }
                else if(g_u16MdlType == Converter)//zlm 20120406 add Converter
                {
                   Converter_can_init();
                }
                else if(g_u16MdlType == IS_RECT) //mhp 20130329 add IS rectifiier
                {
                   ISrect_can_init();
                }
                else if(g_u16MdlType == MPPT)    //mhp 20130925 add MPPT rectifiier
                {
                   MPPT_can_init();
                }
                else if(g_u16MdlType == MPPT_HV) //mhp 20131216 add MPPT_HV rectifiier
                {
                   MPPT_can_init();
                }
                else
                {
                   InitECan();
                }
            }
            g_u8synchronization = 1;
            g_u8boardaddress = 0;
        }
        else if(t_u8SciAddtemp == 0xff)
        {
            if(g_u8MdlAddresss!= t_u8SciAddtemp)
            {
                g_u8MdlAddresss= t_u8SciAddtemp;

                if(g_u16MdlType == HVDC)
                {
                    hvdc_can_init();
                }
                else if(g_u16MdlType == Converter)//zlm 20120406 add Converter
                {
                   Converter_can_init();
                }
                else if(g_u16MdlType == IS_RECT) //mhp 20130329 add IS rectifiier
                {
                   ISrect_can_init();
                }
                else if(g_u16MdlType == MPPT)    //mhp 20130925 add MPPT rectifiier
                {
                   MPPT_can_init();
                }
                else if(g_u16MdlType == MPPT_HV) //mhp 20131216 add MPPT_HV rectifiier
                {
                   MPPT_can_init();
                }
                else
                {
                   InitECan();
                }
            }
            g_u8synchronization = 1;
            g_u8boardaddress = 1;
        }
        else
        {
            g_u16SciRxPoint = 0;
            g_u8synchronization = 0;
            g_u8boardaddress = 0;
        }
    }
    else if ( (g_u8synchronization == 1) && (t_u8SciData == 0x0d) )  /*确保数据包收到包尾*/
    {
        g_u8receive_ok = 1;

        g_u8uLight_status = g_u8uLight_status | 0x04;
        g_u16light1_time = 0;
    }
    else
    {;}
    SciaRegs.SCIFFRX.bit.RXFFOVRCLR = 1;                                // Clear Overflow flag
    SciaRegs.SCIFFRX.bit.RXFFINTCLR = 1;                                // Clear Interrupt flag
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;

    //如果检测到SCI通讯错误，重新初始化SCI
   /* if( mSciErrChk() )
    {
        InitSci();
        SciaRegs.SCICTL2.all = 0x0002;                                      //使能SCIa接收中断
        SciaRegs.SCIFFRX.all = 0x2061;                                      //0010 0000 0110 0001，有数据就产生中断
        g_u16SciRxPoint = 0;
        g_u16SciRxLast = 0;
    }*/
}
/*******************************************************************************
*Function name:     vSciDataParsing()
*Description:       完成接收的485数据的处理,启动对应的485发送
*******************************************************************************/

void vSciDataParsing(void)
{
    unsigned int i = 0;
    ubitint  combine,crcreg ;

    /*从地址开始,到数据结束CRC校验*/
    //crcreg.id = crc12(g_u16SciRxPoint-6) ;
    /*将ASCII改为HEX */

    if( Flag_TrigFd == 0 )
    {
        for ( i=0; i<g_u16SciRxPoint; i++)
        {
            g_u16DataBuffer0[i>>1] = ucAscToHex(g_u16DataBuffer0[i+1], g_u16DataBuffer0[i+2]);
            i++ ;
        }
        i = 0 ;
        /* CRC 校验 */
        g_u16SciRxPoint = (g_u16SciRxPoint>>1) - 1 ;    /* HEX褪莞鍪?*/
        combine.bitdata.lowchar = g_u16DataBuffer0[g_u16SciRxPoint-2] ;
        combine.bitdata.highchar = g_u16DataBuffer0[g_u16SciRxPoint-1] ;
        //if ( combine.id != crcreg.id )
        //{
        //  i = 1 ;
        //}
    }
    if ( i == 0 )
    {
        /*CID数据组织*/
        cid_main = g_u16DataBuffer0[1];
        switch ( g_u16DataBuffer0[1] )
        {
            case WR_S_LIMIT :
                wr_s_limit();
                break;

            case WR_S_CONTRL :
                wr_s_contrl();
                break;

            case SGET_DOWNLOAD_ANSWER:
                g_u16CanIntTimer = 0;
                sget_download_answer();
                boardaddress = 0;
                break;

            case SGET_DOWNLOAD_ADDR:
                g_u16CanIntTimer = 0;
                sget_download_addr();
                boardaddress = 1;
                break;

            case SGET_DOWNLOAD_DATA :
                g_u16CanIntTimer = 0;
                sget_download_data();
                boardaddress = 1;
                break;

            case SGET_ALL_DATA:
                sget_all_data();
                break;

            case SGET_CAL_DATA:
                sget_calibrate_data();
                break;

            case GET_AVERAGE_CURR:
                sget_average_curr();
                break;

            case SET_AVERCURR0_DATA:
                sget_avercurr0_data();
                break;

            case SET_AVERCURR1_DATA:
                sget_avercurr1_data();
                break;

            case SET_AVERCURR2_DATA:
                sget_avercurr2_data();
                break;

            case SET_AVERCURR3_DATA:
                sget_avercurr3_data();
                break;

            case SET_AVERCURR4_DATA:
                sget_avercurr4_data();
                break;

            case SET_AVERCURR5_DATA:
                sget_avercurr5_data();
                break;

            case SET_MEMORYTEST_DATA:                   //DCDC内存固定时间间隔连续读取
                g_u16CanIntTimer = 0;
                sget_memorytest_data();
                break;

            case SET_MEMORYTEST1_DATA:                  //DCDC内存时间连续读取
                g_u16CanIntTimer = 0;
                sget_memorytest1_data();
                break;

            case SET_MEMORYTEST2_DATA:                  //PFC内存固定时间间隔连续读取
                g_u16CanIntTimer = 0;
                sget_memorytest2_data();
                break;

            case SET_MEMORYTEST3_DATA:                  //PFC内存时间连续读取
                g_u16CanIntTimer = 0;
                sget_memorytest3_data();
                break;

            default:
                g_u16DataBuffer0[1] = NOVCOM ;    /*RTN 无效命令 */
                g_u16DataBuffer0[2] = 0 ;         /*LENGTH=0*/
                g_u16DataBuffer0[3] = 0 ;
                break ;
        }/*switch end*/
    }
    else /* communication error */
    {
        g_u16DataBuffer0[1] = CRCCOM ;    /*RTN 校验错 */
        g_u16DataBuffer0[2] = 0 ;     /*LENGTH=0*/
        g_u16DataBuffer0[3] = 0 ;
    }
    /*开始发送数据*/
    /*广播命令则不改变通信状态，不用地址位*/
    if ( boardaddress == 0 )    /* 不是广播命令  */
    {
        //translate_length = g_u16DataBuffer0[3]*256+g_u16DataBuffer0[2]+4; /* 实际HEX个数:ADDR+CID+LEN+DAT */
        Temptranslate_length = g_u16DataBuffer0[3]*256+g_u16DataBuffer0[2]+4; /* 实际HEX个数:ADDR+CID+LEN+DAT */
        if(Temptranslate_length <= 1024)
        {
            translate_length = Temptranslate_length;

            txd_asc(translate_length);      /* 变换g_u16DataBuffer0为ASC字符 */
            /* fill CRC check code */
            translate_length = translate_length<<1;
            //crcreg.id = crc12(translate_length);
            crcreg.id=0;
            combine.id = uiHexToAsc(crcreg.bitdata.lowchar) ;
            translate_length++ ;
            g_u16DataBuffer0[translate_length] = combine.bitdata.highchar ;
            translate_length++ ;
            g_u16DataBuffer0[translate_length] = combine.bitdata.lowchar ;
            translate_length++ ;
            combine.id = uiHexToAsc(crcreg.bitdata.highchar) ;
            g_u16DataBuffer0[translate_length] = combine.bitdata.highchar ;
            translate_length++ ;
            g_u16DataBuffer0[translate_length] = combine.bitdata.lowchar ;
            translate_length++ ;
            g_u16DataBuffer0[translate_length] = 0x0d;
            translate_length++ ;
            ready_485_transmit = 1;
            g_u16SciRxPoint = 0;

            g_u16DataBuffer0[0] = 0x7e;
            scia_enableTX_disableRX();
            //SciaRegs.SCITXBUF = 0x7e;//start sci transmit
//            for(i = 0; i < translate_length;i++)
//            {
//                while(SciaRegs.SCICTL2.bit.TXRDY == 0){}
//                SciaRegs.SCITXBUF = g_u16DataBuffer0[i];
//            }
        }
    }
    else
    {
        boardaddress = 0;
        g_u16SciRxPoint = 0;
    }
}

/******************************************************************
    函数名称 :  txd_asc
    入口参数 :  len--需变换字节数
    出口参数 :  无
    函数功能 :  将len个数据变换为ASCII码，存储于通信缓冲区
    子函数   :     hex_asc
    修改:     mhp 20040104 g_u16DataBuffer0[2],g_u16DataBuffer0[3]
******************************************************************/
void txd_asc(unsigned int len)/* len g_u16DataBuffer0[]实际HEX个数:ADDR+CID+LEN+DAT */
{
    unsigned int i ;

    ubitint  com_int ;

    g_u16DataBuffer0[0] = address ;
    /*g_u16DataBuffer0[2] = (len-4)<<1;
    g_u16DataBuffer0[3] = 0;  */              /* mhp 20040104 */
    g_u16DataBuffer0[2] = ((len-4)<<1)%256;
    g_u16DataBuffer0[3] = ((len-4)<<1)/256;
    for ( i=len; i>0; i--)  /* i 无符号数 */
    {
        com_int.id = uiHexToAsc(g_u16DataBuffer0[i-1]) ;
        g_u16DataBuffer0[((i-1)<<1)+1] = com_int.bitdata.lowchar ;
        g_u16DataBuffer0[(i-1)<<1] = com_int.bitdata.highchar ;
    }
    for ( i=len<<1; i>0; i--)
    {
        g_u16DataBuffer0[i] = g_u16DataBuffer0[i-1];
    }
}
/******************************************************************
    函数名称 :  sget_all_data
    入口参数 :  ?
    出口问?:   无
    函数功能 :  设置/获取系统参数及数据(H6412U1扩展命令)
    子函数   :     recycle
    修改内容 :
******************************************************************/
void sget_all_data(void)
{
    unsigned char j,k;
    unsigned int uiDataHigh,uiDataLow,uWarmTmp0,uWarmTmp1;;

    if(u16MdlType == Converter)//zlm for converter
    {
        ucMsgType = 0x20;
        j = 0x00;
        k = 1;
        get_can_data(j,k);

        ucMsgType = 0x01;
        j = 0x08; //zlm 20120418读取限流点
        k = 2;
        get_can_data(j,k);

        ucMsgType = 0x01;
        j = 0x01;//读取输出电压
        k = 0;
        get_can_data(j,k);


        ucMsgType = 0x01;
        j = 0x0f;//设置读取pfc电压
        k = 1;
        get_can_data(j,k);

        ucMsgType = 0x01;
        j = 0x10;//读取输入电压
        k = 2;
        get_can_data(j,k);

        ucMsgType = 0x01;
        j = 0x19;//读取环境温度
        k = 1;
        get_can_data(j,k);

    }
    else if(u16MdlType == efuse)
    {
        ucMsgType = 0x10;
        j = 0x00;
        k = 1;
        get_can_data(j,k);

        ucMsgType = 0x20;
        j = 0x00;
        k = 1;
        get_can_data(j,k);

        /*ucMsgType = 0x30;
        j = 0x00;
        k = 1;
        get_can_data(j,k);*/

/*      ucMsgType = 0x01;
        j = 0x20;
        k = 8;
        get_can_data(j,k);
*/

    }
    else
    {
        ucMsgType = 0x10;
        j = 0x00;
        k = 1;
        get_can_data(j,k);

        ucMsgType = 0x20;
        j = 0x00;
        k = 1;
        get_can_data(j,k);

        /*ucMsgType = 0x30;
        j = 0x00;
        k = 1;
        get_can_data(j,k);*/

        ucMsgType = 0x01;
        j = 0x06;//读取电压上限
        k = 6;
        get_can_data(j,k);

        // 20110506 mhp get pfc software version
        // ID lock state for IS rectifier
        ucMsgType = 0x01;
        j = 0x57;//读取电压
        k = 1;
        get_can_data(j,k);


        ucMsgType = 0x01;   // rectifier warn/state
        j = 0x42;//读取告警位/读取当前运行状态/读取校准参数异常
        k = 3;
        get_can_data(j,k);

        ucMsgType = 0x01;   // rectifier input curr\power
        j = 0xD0;//读取输入电流/B/C电流/输入功率/输入电量/输入频率
        k = 7;
        get_can_data(j,k);

        //ucMsgType = 0x01;     // rectifier input power
        //j = 0xD3;
        //k = 1;
        //get_can_data(j,k);

        ucMsgType = 0x01;
        j = 0x10;//PFC温度
        k = 1;
        get_can_data(j,k);

        if((u16MdlType == MPPT) || (u16MdlType == MPPT_HV))
        {

            ucMsgType = 0x01;   //  temperature for MPPT
            j = 0x04;
            k = 1;
            get_can_data(j,k);

            if(u16MdlType == MPPT_HV)
            {
                ucMsgType = 0x01;
                j = 0x0D;
                k = 2;
                get_can_data(j,k);
            }

        }

    }


    g_u16DataBuffer0[1] = SGET_ALL_DATA ;

    if(u16ComType == COM_SINGLE)        // 20140902
    {
        ucMsgType = 0x01;
        j = 0x01;
        k = 7;
        get_can_data(j,k);

        ucMsgType = 0x01;
        j = 0x40;
        k = 1;
        get_can_data(j,k);

        ucMsgType = 0x01;
        j = 0x54;
        k = 1;
        get_can_data(j,k);

        ucMsgType = 0x01;
        j = 0x58;
        k = 1;
        get_can_data(j,k);
    }
    else
    {
        ucMsgType = 0x00;
        j = 0x00;
        k = 1;
        get_can_data(j,k);
    }

    if ((u16MdlType == HVDC) || (u16MdlType == MPPT_HV))
    {
        uiDataHigh=(unsigned int)(dcvolt.fd*50);
    }
    else
    {
        uiDataHigh=(unsigned int)(dcvolt.fd*100);
    }

    g_u16DataBuffer0[1] = SGET_ALL_DATA ;
    //g_u16DataBuffer0[2] = 68;           // add CAN485 s/w version mhp 20130318
    g_u16DataBuffer0[2] = 98;         // add IS/MPPT/RECT info mhp 20130428
    g_u16DataBuffer0[3] = 0;

    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[4] = uiDataHigh;
    g_u16DataBuffer0[5] = uiDataLow;

    uiDataHigh=(unsigned int)(dccurr.fd*100);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[6] = uiDataHigh;
    g_u16DataBuffer0[7] = uiDataLow;

    uiDataHigh=(unsigned int)(dclimit.fd*100);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[8] = uiDataHigh;
    g_u16DataBuffer0[9] = uiDataLow;

    uiDataHigh=(signed int)(temperature.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[10] = uiDataHigh;
    g_u16DataBuffer0[11] = uiDataLow;

    if(u16MdlType == Converter) //v524 add
    {
        uiDataHigh=(unsigned int)(acvolt.fd*100);
    }
    else
    {
        uiDataHigh=(unsigned int)(acvolt.fd*10);
    }
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[12] = uiDataHigh;
    g_u16DataBuffer0[13] = uiDataLow;

    if ((u16MdlType == HVDC) || (u16MdlType == MPPT_HV))
    {
        uiDataHigh=(unsigned int)(dcvolt_up.fd*50);
    }
    else
    {
        uiDataHigh=(unsigned int)(dcvolt_up.fd*100);
    }
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[14] = uiDataHigh;
    g_u16DataBuffer0[15] = uiDataLow;

    uiDataHigh=(unsigned int)(pfcvolt.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[16] = uiDataHigh;
    g_u16DataBuffer0[17] = uiDataLow;

    uiDataHigh=(unsigned int)(temp_up.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[18] = uiDataHigh;
    g_u16DataBuffer0[19] = uiDataLow;

    uiDataHigh=(unsigned int)(dccurr_disp.fd*100);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[20] = uiDataHigh;
    g_u16DataBuffer0[21] = uiDataLow;

    uiDataHigh=(signed int)(temp_disp.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[22] = uiDataHigh;
    g_u16DataBuffer0[23] = uiDataLow;

    uiDataHigh=(unsigned int)(ac1volt.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[24] = uiDataHigh;
    g_u16DataBuffer0[25] = uiDataLow;

    uiDataHigh=(unsigned int)(ac2volt.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[26] = uiDataHigh;
    g_u16DataBuffer0[27] = uiDataLow;

    uiDataHigh=(unsigned int)(tagRunTime.fd);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[28] = uiDataHigh;
    g_u16DataBuffer0[29] = uiDataLow;

    uiDataLow=softwarever&0x00ff;
    uiDataHigh=(softwarever&0xff00)>>8;
    g_u16DataBuffer0[30] = uiDataHigh;
    g_u16DataBuffer0[31] = uiDataLow;

    uiDataLow=hardwarever&0x00ff;
    uiDataHigh=(hardwarever&0xff00)>>8;
    g_u16DataBuffer0[32] = uiDataHigh;
    g_u16DataBuffer0[33] = uiDataLow;

    uiDataHigh=(unsigned int)(pfcvolt1.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[34] = uiDataHigh;
    g_u16DataBuffer0[35] = uiDataLow;
    g_u16DataBuffer0[36] = uNodeIdL&0x00ff;
    g_u16DataBuffer0[37] = (uNodeIdL&0xff00)>>8;
    g_u16DataBuffer0[38] = uNodeIdH&0x00ff;
    g_u16DataBuffer0[39] = (uNodeIdH&0xff00)>>8;
    g_u16DataBuffer0[40] = uNodeidL&0x00ff;
    g_u16DataBuffer0[41] = (uNodeidL&0xff00)>>8;
    g_u16DataBuffer0[42] = uNodeidH&0x00ff;
    g_u16DataBuffer0[43] = (uNodeidH&0xff00)>>8;
    g_u16DataBuffer0[44] = uBarCode1&0x00ff;
    g_u16DataBuffer0[45] = (uBarCode1&0xff00)>>8;
    g_u16DataBuffer0[46] = uBarCode2&0x00ff;
    g_u16DataBuffer0[47] = (uBarCode2&0xff00)>>8;
    g_u16DataBuffer0[48] = uBarCode3&0x00ff;
    g_u16DataBuffer0[49] = (uBarCode3&0xff00)>>8;
    g_u16DataBuffer0[50] = uBarCode4&0x00ff;
    g_u16DataBuffer0[51] = (uBarCode4&0xff00)>>8;
    g_u16DataBuffer0[52] = uBarCode5&0x00ff;
    g_u16DataBuffer0[53] = (uBarCode5&0xff00)>>8;
    g_u16DataBuffer0[54] = uBarCode6&0x00ff;
    g_u16DataBuffer0[55] = (uBarCode6&0xff00)>>8;
    g_u16DataBuffer0[56] = uBarCode7&0x00ff;
    g_u16DataBuffer0[57] = (uBarCode7&0xff00)>>8;
    g_u16DataBuffer0[58] = uBarCode8&0x00ff;
    g_u16DataBuffer0[59] = (uBarCode8&0xff00)>>8;

    uiDataHigh=(signed int)(temp_pfc.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[60] = uiDataHigh;
    g_u16DataBuffer0[61] = uiDataLow;

    uiDataHigh=(unsigned int)(ac3volt.fd*10);
    uiDataLow=uiDataHigh&0x00ff;
    uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[62] = uiDataHigh;
    g_u16DataBuffer0[63] = uiDataLow;

    if(u16MdlType == Converter)//zlm for converter
    {

    /*uWarmTmp0 转换成 GBB */

    /*uWarmTmp0--L.uWarmTmp1--H*/

        uWarmTmp0=  /*alarm0*/(((status1 & 0x0004) << 13)//alarm29
                    /*alarm1*/|((status & 0x0004) << 12) //alarm13
                    /*alarm2*/|((status1 & 0x0001) << 13)//alarm31
                    /*alarm3*/|((status1 & 0x0020) << 7)//alarm26

                    /*alarm4*/|((status1 & 0x0002) << 10)//alarm30
                    /*alarm5*/|((status & 0x0008) << 7) //alarm12
                    /*alarm6*/
                    /*alarm7*///|((status1 & 0x0008) << 7)//alarm28

                    //zlm 20120929
                    /*alarm7*/|((status1 & 0x0008) << 5)//alarm28


                    /*alarm8*/
                    /*alarm9*///|((status1 & 0x0100) >> 2)//alarm23 1:on 0 :off

                    //zlm 20121019 alarm23 change to alarm27
                    /*alarm9*/|((status1 & 0x0010) << 2)//alarm27  1:off 0: on
                    /*alarm10*/|((status & 0x0100) >> 3)//alarm7
                    /*alarm11*/|((status1 & 0x0200) >> 5)//alarm22

                    /*alarm12*/
                    /*alarm13*/|((status & 0x1000) >> 10)//alarm3
                    /*alarm14*/|((status & 0x0020) >> 4));//alarm10
                    /*alarm15*/

        uWarmTmp1=  /*alarm16*/
                    /*alarm17*/(((status & 0x0002) << 13)//alarm14
                    /*alarm18*/
                    /*alarm19*/

                    /*alarm20*/|((status & 0x0040) << 5)//alarm9
                    /*alarm21*/|((status & 0x2000) >> 3)//alarm2
                    /*alarm22*/|((status & 0x0800) >> 2)//alarm4
                    /*alarm23*/

                    /*alarm24*/|((status & 0x0080)));//alarm8
                    /*alarm25*/
                    /*alarm26*/
                    /*alarm27*/

                    /*alarm28*/
                    /*alarm29*/
                    /*alarm30*/
                    /*alarm31*/


        g_u16DataBuffer0[65] = (uWarmTmp0 & 0xff00)>>8;
        g_u16DataBuffer0[66] = uWarmTmp1 & 0x00ff;
        g_u16DataBuffer0[64] = uWarmTmp0 & 0x00ff;
        g_u16DataBuffer0[67] = (uWarmTmp1 & 0xff00)>>8;

    }
    else
    {
        g_u16DataBuffer0[65] = (status & 0xff00) >> 8;
        g_u16DataBuffer0[66] = status1 & 0x00ff;
        g_u16DataBuffer0[64] = status & 0x00ff;
        g_u16DataBuffer0[67] = (status1 & 0xff00) >> 8;
    }

    // 20110506
    uiDataLow = pfcswver & 0x00ff;
    uiDataHigh = (pfcswver & 0xff00)>>8;
    g_u16DataBuffer0[68] = uiDataHigh;
    g_u16DataBuffer0[69] = uiDataLow;

    g_u16DataBuffer0[70] = 0x02;      // high CAN485 s/w verison 525  mhp 20140902
    g_u16DataBuffer0[71] = 0x15;      // low  (2<<8)+ 0x13 = 531

    // add by mhp 20130428 rect info and IS info
    uiDataLow = u16RecRunInfH & 0x00ff;
    uiDataHigh = (u16RecRunInfH & 0xff00) >> 8;
    g_u16DataBuffer0[72] = uiDataHigh;
    g_u16DataBuffer0[73] = uiDataLow;

    uiDataLow = u16RecRunInfL & 0x00ff;
    uiDataHigh = (u16RecRunInfL & 0xff00) >> 8;
    g_u16DataBuffer0[74] = uiDataHigh;
    g_u16DataBuffer0[75] = uiDataLow;

    uiDataLow = 0x00 & 0x00ff;
    uiDataHigh = (u16ISIdUnlock & 0xff00) >> 8;
    g_u16DataBuffer0[76] = uiDataHigh;
    g_u16DataBuffer0[77] = uiDataLow;

    uiDataLow = 255 - (u16ISAddr & 0x00ff);
    uiDataHigh = (u16ISAddr & 0xff00) >> 8;
    g_u16DataBuffer0[78] = uiDataHigh;
    g_u16DataBuffer0[79] = uiDataLow;

    g_u16DataBuffer0[80] = status2 & 0x00ff;
    g_u16DataBuffer0[81] = (status2 & 0xff00) >> 8;
    g_u16DataBuffer0[82] = status3 & 0x00ff;
    g_u16DataBuffer0[83] = (status3 & 0xff00) >> 8;

    uiDataHigh=(unsigned int)(currin.fd * 100);
    uiDataLow=uiDataHigh & 0x00ff;
    uiDataHigh=(uiDataHigh & 0xff00) >> 8;
    g_u16DataBuffer0[84] = uiDataHigh;
    g_u16DataBuffer0[85] = uiDataLow;

    uiDataHigh=(unsigned int)(powerin.fd);
    uiDataLow=uiDataHigh & 0x00ff;
    uiDataHigh=(uiDataHigh & 0xff00) >> 8;
    g_u16DataBuffer0[86] = uiDataHigh;
    g_u16DataBuffer0[87] = uiDataLow;

    uiDataHigh=(unsigned int)(e_power.fd);
    uiDataLow=uiDataHigh & 0x00ff;
    uiDataHigh=(uiDataHigh & 0xff00) >> 8;
    g_u16DataBuffer0[88] = uiDataHigh;
    g_u16DataBuffer0[89] = uiDataLow;

    uiDataHigh=(unsigned int)(frequencyin.fd  * 100);
    uiDataLow=uiDataHigh & 0x00ff;
    uiDataHigh=(uiDataHigh & 0xff00) >> 8;
    g_u16DataBuffer0[90] = uiDataHigh;
    g_u16DataBuffer0[91] = uiDataLow;

    uiDataHigh=(unsigned int)(currinB.fd  * 100);
    uiDataLow=uiDataHigh & 0x00ff;
    uiDataHigh=(uiDataHigh & 0xff00) >> 8;
    g_u16DataBuffer0[92] = uiDataHigh;
    g_u16DataBuffer0[93] = uiDataLow;

    uiDataHigh=(unsigned int)(currinC.fd  * 100);
    uiDataLow=uiDataHigh & 0x00ff;
    uiDataHigh=(uiDataHigh & 0xff00) >> 8;
    g_u16DataBuffer0[94] = uiDataHigh;
    g_u16DataBuffer0[95] = uiDataLow;

    g_u16DataBuffer0[96] = u16EpromErrH & 0x00ff;
    g_u16DataBuffer0[97] = (u16EpromErrH & 0xff00) >> 8;
    g_u16DataBuffer0[98] = u16EpromErrL & 0x00ff;
    g_u16DataBuffer0[99] = (u16EpromErrL & 0xff00) >> 8;

    uiDataHigh=(unsigned int)(efficiency.fd  * 1000);
    uiDataLow=uiDataHigh & 0x00ff;
    uiDataHigh=(uiDataHigh & 0xff00) >> 8;
    g_u16DataBuffer0[100] = uiDataHigh;
    g_u16DataBuffer0[101] = uiDataLow;

}

/******************************************************************
    函数名称 :  sget_avercurr_data
    入口参数 :  无
    出口参数 :  无
    函数功能 :
    子函数   : recycle
    修改内容 :
******************************************************************/
void sget_average_curr(void)
{
    unsigned int uiDataHigh,uiDataLow;

    g_u16DataBuffer0[1] = GET_AVERAGE_CURR;
    g_u16DataBuffer0[2] = 3;
    g_u16DataBuffer0[3] = 0;

    g_u16DataBuffer0[4] = mokuai_main_address;

    uiDataHigh=(unsigned int)(avergae_current*10);
        uiDataLow=uiDataHigh&0x00ff;
        uiDataHigh=(uiDataHigh&0xff00)>>8;
    g_u16DataBuffer0[5] = uiDataHigh;
    g_u16DataBuffer0[6] = uiDataLow;

}

void sget_avercurr0_data(void)
{
    ubitfloat floattemp;
    unsigned int i,j;
    unsigned int uiDataHigh,uiDataLow;

    g_u16DataBuffer0[1] = SET_AVERCURR0_DATA;
    g_u16DataBuffer0[2] = 176;
    g_u16DataBuffer0[3] = 0;
    j=4;
    g_u16DataBuffer0[j++] = mokuai_main_address;
    for ( i=0; i<25; i++ )
    {
        g_u16DataBuffer0[j++] = mokuai_times[i];
        mokuai_times[i] = 0;

        uiDataHigh=(unsigned int)(mokuai_current[i]*10);
        uiDataLow=uiDataHigh&0x00ff;
        uiDataHigh=(uiDataHigh&0xff00)>>8;
        g_u16DataBuffer0[j++] = uiDataHigh;
        g_u16DataBuffer0[j++] = uiDataLow;

        floattemp.fd = mokuai_delta[i];
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.lowchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.lowchar;
    }
}
void sget_avercurr1_data(void)
{
    ubitfloat floattemp;
    unsigned int i,j;
    unsigned int uiDataHigh,uiDataLow;

    g_u16DataBuffer0[1] = SET_AVERCURR1_DATA;
    g_u16DataBuffer0[2] = 176;
    g_u16DataBuffer0[3] = 0;
    j=4;
    g_u16DataBuffer0[j++] = mokuai_main_address;
    for ( i=25; i<50; i++ )
    {
        g_u16DataBuffer0[j++] = mokuai_times[i];
        mokuai_times[i] = 0;

        uiDataHigh=(unsigned int)(mokuai_current[i]*10);
        uiDataLow=uiDataHigh&0x00ff;
        uiDataHigh=(uiDataHigh&0xff00)>>8;
        g_u16DataBuffer0[j++] = uiDataHigh;
        g_u16DataBuffer0[j++] = uiDataLow;

        floattemp.fd = mokuai_delta[i];
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.lowchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.lowchar;
    }
}
void sget_avercurr2_data(void)
{
    ubitfloat floattemp;
    unsigned int i,j;
    unsigned int uiDataHigh,uiDataLow;

    g_u16DataBuffer0[1] = SET_AVERCURR2_DATA;
    g_u16DataBuffer0[2] = 176;
    g_u16DataBuffer0[3] = 0;
    j=4;
    g_u16DataBuffer0[j++] = mokuai_main_address;
    for ( i=50; i<75; i++ )
    {
        g_u16DataBuffer0[j++] = mokuai_times[i];
        mokuai_times[i] = 0;

        uiDataHigh=(unsigned int)(mokuai_current[i]*10);
        uiDataLow=uiDataHigh&0x00ff;
        uiDataHigh=(uiDataHigh&0xff00)>>8;
        g_u16DataBuffer0[j++] = uiDataHigh;
        g_u16DataBuffer0[j++] = uiDataLow;

        floattemp.fd = mokuai_delta[i];
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.lowchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.lowchar;
    }
}
void sget_avercurr3_data(void)
{
    ubitfloat floattemp;
    unsigned int i,j;
    unsigned int uiDataHigh,uiDataLow;

    g_u16DataBuffer0[1] = SET_AVERCURR3_DATA;
    g_u16DataBuffer0[2] = 176;
    g_u16DataBuffer0[3] = 0;
    j=4;
    g_u16DataBuffer0[j++] = mokuai_main_address;
    for ( i=75; i<100; i++ )
    {
        g_u16DataBuffer0[j++] = mokuai_times[i];
        mokuai_times[i] = 0;

        uiDataHigh=(unsigned int)(mokuai_current[i]*10);
        uiDataLow=uiDataHigh&0x00ff;
        uiDataHigh=(uiDataHigh&0xff00)>>8;
        g_u16DataBuffer0[j++] = uiDataHigh;
        g_u16DataBuffer0[j++] = uiDataLow;

        floattemp.fd = mokuai_delta[i];
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.lowchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.lowchar;
    }
}

void sget_avercurr4_data(void)
{
    ubitfloat floattemp;
    unsigned int i,j;
    unsigned int uiDataHigh,uiDataLow;

    g_u16DataBuffer0[1] = SET_AVERCURR4_DATA;
    g_u16DataBuffer0[2] = 127;
    g_u16DataBuffer0[3] = 0;
    j=4;
    g_u16DataBuffer0[j++] = mokuai_main_address;
    for ( i=72; i<90; i++ )
    {
        g_u16DataBuffer0[j++] = mokuai_times[i];
        mokuai_times[i] = 0;

        uiDataHigh=(unsigned int)(mokuai_current[i]*10);
        uiDataLow=uiDataHigh&0x00ff;
        uiDataHigh=(uiDataHigh&0xff00)>>8;
        g_u16DataBuffer0[j++] = uiDataHigh;
        g_u16DataBuffer0[j++] = uiDataLow;

        floattemp.fd = mokuai_delta[i];
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.lowchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.lowchar;
    }
}
void sget_avercurr5_data(void)
{
    ubitfloat floattemp;
    unsigned int i,j;
    unsigned int uiDataHigh,uiDataLow;

    g_u16DataBuffer0[1] = SET_AVERCURR5_DATA;
    g_u16DataBuffer0[2] = 127;
    g_u16DataBuffer0[3] = 0;
    j=4;
    g_u16DataBuffer0[j++] = mokuai_main_address;
    for ( i=90; i<108; i++ )
    {
        g_u16DataBuffer0[j++] = mokuai_times[i];
        mokuai_times[i] = 0;

        uiDataHigh=(unsigned int)(mokuai_current[i]*10);
        uiDataLow=uiDataHigh&0x00ff;
        uiDataHigh=(uiDataHigh&0xff00)>>8;
        g_u16DataBuffer0[j++] = uiDataHigh;
        g_u16DataBuffer0[j++] = uiDataLow;

        floattemp.fd = mokuai_delta[i];
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[0].bitdata.lowchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.highchar;
        g_u16DataBuffer0[j++] = floattemp.intdata[1].bitdata.lowchar;
    }
}
/******************************************************************
    函数名称 :  sget_calibrate_data
    入口参数 :  无
    出口参数 :  无
    函数功能 :  设置/获取系统参数及数据(H6412U1扩展命令)
    子函数    :    recycle
    修改内容 :  mhp 200031226, 还缺设置PFC电压控制校准系数

    zlm 此处Converter 不需要更改
******************************************************************/
void sget_calibrate_data(void)
{
    unsigned char code_type;
    ubitfloat       comflt,comflt1;
    unsigned char j,k;

    ucMsgType = 0x03;
    code_type = g_u16DataBuffer0[4] ;
    g_u16DataBuffer0[1] = SGET_CAL_DATA;
    g_u16DataBuffer0[2] = 8 ;
    g_u16DataBuffer0[3] = 0 ;

    comflt.intdata[0].bitdata.highchar = g_u16DataBuffer0[6] ;
    comflt.intdata[0].bitdata.lowchar = g_u16DataBuffer0[7] ;
    comflt.intdata[1].bitdata.highchar = g_u16DataBuffer0[8] ;
    comflt.intdata[1].bitdata.lowchar = g_u16DataBuffer0[9] ;

    comflt1.intdata[0].bitdata.highchar = g_u16DataBuffer0[10] ;
    comflt1.intdata[0].bitdata.lowchar = g_u16DataBuffer0[11] ;
    comflt1.intdata[1].bitdata.highchar = g_u16DataBuffer0[12] ;
    comflt1.intdata[1].bitdata.lowchar = g_u16DataBuffer0[13] ;

    switch (code_type)
    {
        case 0x17:
            /* read dcdc ram */
            g_u16DataBuffer0[2] = 8 ;

            uDcReadRamAdd.intdata[0].id=(unsigned int)comflt.fd;
            uDcReadRamAdd.intdata[1].id=(unsigned int)comflt1.fd;
            j = 0xe0;
            k = 1;
            get_can_data(j,k);

            g_u16DataBuffer0[4] = 1;
            g_u16DataBuffer0[5] = uDcReadRamCon.intdata[0].bitdata.highchar;
            g_u16DataBuffer0[6] = uDcReadRamCon.intdata[0].bitdata.lowchar;
            g_u16DataBuffer0[7] = uDcReadRamCon.intdata[1].bitdata.highchar;
            g_u16DataBuffer0[8] = uDcReadRamCon.intdata[1].bitdata.lowchar;

            break ;

        case 0x18:
            /* write dcdc ram */
            uDcWriteRamCon.intdata[0].id=(unsigned int)comflt.fd;
            uDcWriteRamCon.intdata[1].id=(unsigned int)comflt1.fd;
            j = 0xe1;
            k = 1;
            get_can_data(j,k);
            break ;

        case 0x19 :
            /* write dcdc eeprom*/
            uDcWriteEemAdd.fd=comflt.fd;
            uDcWriteEemCon.fd=comflt1.fd;
            j = 0xe2;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x1B:
            /* read pfc ram */
            g_u16DataBuffer0[2] = 8 ;

            uAcReadRamAdd.intdata[0].id=(unsigned int)comflt.fd;
            uAcReadRamAdd.intdata[1].id=(unsigned int)comflt1.fd;
            j = 0xe4;
            k = 1;
            get_can_data(j,k);

            g_u16DataBuffer0[4] = 0;
            g_u16DataBuffer0[5] = uAcReadRamCon.intdata[0].bitdata.highchar;
            g_u16DataBuffer0[6] = uAcReadRamCon.intdata[0].bitdata.lowchar;
            g_u16DataBuffer0[7] = uAcReadRamCon.intdata[1].bitdata.highchar;
            g_u16DataBuffer0[8] = uAcReadRamCon.intdata[1].bitdata.lowchar;

            break ;

        case 0x22:         /*ls 110526*/
            /* read dc E2prom */
            g_u16DataBuffer0[2] = 8 ;

            uDcReadEemAdd.intdata[0].id=comflt.fd;
            uDcReadEemAdd.intdata[1].id=comflt1.fd;
            j = 0xea;
            k = 1;
            get_can_data(j,k);

            g_u16DataBuffer0[4] = 2;
            g_u16DataBuffer0[5] = uDcReadEemCon.intdata[0].bitdata.highchar;
            g_u16DataBuffer0[6] = uDcReadEemCon.intdata[0].bitdata.lowchar;
            g_u16DataBuffer0[7] = uDcReadEemCon.intdata[1].bitdata.highchar;
            g_u16DataBuffer0[8] = uDcReadEemCon.intdata[1].bitdata.lowchar;

            break ;

        case 0x23:       /*ls 110526*/
            /* read pfc E2prom */
            g_u16DataBuffer0[2] = 8 ;

            uAcReadEemAdd.intdata[0].id=comflt.fd;
            uAcReadEemAdd.intdata[1].id=comflt1.fd;
            j = 0xeb;
            k = 1;
            get_can_data(j,k);

            g_u16DataBuffer0[4] = 3;
            g_u16DataBuffer0[5] = uAcReadEemCon.intdata[0].bitdata.highchar;
            g_u16DataBuffer0[6] = uAcReadEemCon.intdata[0].bitdata.lowchar;
            g_u16DataBuffer0[7] = uAcReadEemCon.intdata[1].bitdata.highchar;
            g_u16DataBuffer0[8] = uAcReadEemCon.intdata[1].bitdata.lowchar;

            break ;

        case 0x1C:
            /* write pfc ram */
            uAcWriteRamCon.intdata[0].id=(unsigned int)comflt.fd;
            uAcWriteRamCon.intdata[1].id=(unsigned int)comflt1.fd;
            j = 0xe5;
            k = 1;
            get_can_data(j,k);
            break ;

        case 0x1D :
            /* write pfc eeprom*/
            uAcWriteEemAdd.fd=comflt.fd;
            uAcWriteEemCon.fd=comflt1.fd;
            j = 0xe6;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x0a:
            if(u16MdlType == efuse)
            {
                set_efuseVinsamp_a.fd = comflt.fd;
                set_efuseVinsamp_b.fd = comflt.fd;
            }
            else
            {
                /* 电压采样校准 */
                dcvoltsamp_sys_a.fd = comflt.fd;
                dcvoltsamp_sys_b.fd = comflt1.fd;
            }
            j = 0x60;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x0b :
            if(u16MdlType == efuse)
            {
                set_efuseVolt2_a.fd = comflt.fd;
                set_efuseVolt2_b.fd = comflt.fd;
            }
            else
            {
                /* 电压控制校准 */
                dcvoltcontl_sys_a.fd = comflt.fd;
                dcvoltcontl_sys_b.fd = comflt1.fd;
            }
            j = 0x64;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x0c :
            if(u16MdlType == efuse)
            {
                set_efuseVolt1_a.fd = comflt.fd;
                set_efuseVolt1_b.fd = comflt.fd;
            }
            else
            {
                /* 电流采样校准 */
                dccurrsamp_sys_a.fd = comflt.fd;
                dccurrsamp_sys_b.fd = comflt1.fd;
            }
            j = 0x62;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x0d :
            if(u16MdlType == efuse)
            {
                set_efuseCurr1_a.fd = comflt.fd;
                set_efuseCurr1_b.fd = comflt.fd;
            }
            else
            {
                /* 电流控制校准 */
                dccurrcontl_sys_a.fd = comflt.fd;
                dccurrcontl_sys_b.fd = comflt1.fd;
            }
            j = 0x66;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x0e :
            /* pfc电压采样校准 */
            pfcvoltsamp_sys.fd = comflt.fd;
            j = 0x6c;
            k = 1;
            get_can_data(j,k);
            break ;

        case 0x0f :
            if(u16MdlType == efuse)
            {
                set_efuseCurr2_a.fd = comflt.fd;
                set_efuseCurr2_b.fd = comflt.fd;
            }
            else
            {
                /* ac电压采样校准 */
                acvoltsamp_sys_a.fd = comflt.fd;
                acvoltsamp_sys_b.fd = comflt1.fd;
            }
            j = 0x68;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x11:
            /* ac1电压采样校准 */
            ac1voltsamp_sys_a.fd = comflt.fd;
            ac1voltsamp_sys_b.fd = comflt1.fd;
            j = 0x6a;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x09:
            /* ac2电压采样校准 */
            ac2voltsamp_sys_a.fd = comflt.fd;
            ac2voltsamp_sys_b.fd = comflt1.fd;
            j = 0x72;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x10 :
            /* tc电压采样校准 */
            tempsamp_sys.fd = comflt.fd;
            j = 0x6e;
            k = 1;
            get_can_data(j,k);
            break ;

        case 0x12 :
            /* 功率控制校准 */
            dcpowercontl_sys_a.fd = comflt.fd;
            dcpowercontl_sys_b.fd = comflt1.fd;
            j = 0x70;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x13:
            /* 电压采样校准 */
            fancon_sys_a.fd = comflt.fd;
            fancon_sys_b.fd = comflt1.fd;
            j = 0x78;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x1A :
            /*id*/
            uNodeIdH=comflt1.intdata[0].id;
            uNodeIdL=comflt1.intdata[1].id;
            uNodeidH=comflt.intdata[0].id;
            uNodeidL=comflt.intdata[1].id;
            j = 0xa0;
            k = 2;
            get_can_data(j,k);
            break ;
        case 0x1E :
            /*BAR CODE*/
            uBarCode1=comflt.intdata[0].id;
            uBarCode2=comflt.intdata[1].id;
            uBarCode3=comflt1.intdata[0].id;
            uBarCode4=comflt1.intdata[1].id;
            break ;
        case 0x1F :
            /*BAR CODE*/
            uBarCode5=comflt.intdata[0].id;
            uBarCode6=comflt.intdata[1].id;
            uBarCode7=comflt1.intdata[0].id;
            uBarCode8=comflt1.intdata[1].id;
            j = 0xa6;
            k = 4;
            get_can_data(j,k);
            break ;

        case 0x20 :
            /*id*/
            hardwarever=comflt.fd;
            softwarever=comflt1.fd;
            j = 0xa2;
            k = 1;
            get_can_data(j,k);
            break ;
        case 0x21 :
            /*id*/
            downloadadd=comflt.fd;
            downloadcon=comflt1.fd;
            j = 0xFF;
            k = 1;
            get_can_data(j,k);
            break ;

        case 0x24 :
            /* 输入电流采样校准 */
            currinsamp_sys_a.fd = comflt.fd;
            currinsamp_sys_b.fd = comflt1.fd;
            j = 0x76;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x25 :
            /* 输出电流偏置校准 */
            outcuroffset.fd = comflt.fd;
            j = 0x7B;
            k = 1;
            get_can_data(j,k);
            break ;

        case 0x26:         //ls 20150728
            //read E2prom fault record, v525 add
            g_u16DataBuffer0[2] = 8 ;

            uFaultRecEemAdd.intdata[0].id=comflt.fd;
            uFaultRecEemAdd.intdata[1].id=comflt1.fd;
            j = 0xf3;
            k = 1;
            get_can_data(j,k);

            g_u16DataBuffer0[4] = 4;
            g_u16DataBuffer0[5] = uFaultRecEemCon.intdata[0].bitdata.highchar;
            g_u16DataBuffer0[6] = uFaultRecEemCon.intdata[0].bitdata.lowchar;
            g_u16DataBuffer0[7] = uFaultRecEemCon.intdata[1].bitdata.highchar;
            g_u16DataBuffer0[8] = uFaultRecEemCon.intdata[1].bitdata.lowchar;

            break ;

        case 0x27 :
            /* B相输入电流采样校准 */
            currinBsamp_sys_a.fd = comflt.fd;
            currinBsamp_sys_b.fd = comflt1.fd;
            j = 0x80;
            k = 2;
            get_can_data(j,k);
            break ;

        case 0x28 :
            /* C相输入电流采样校准 */
            currinCsamp_sys_a.fd = comflt.fd;
            currinCsamp_sys_b.fd = comflt1.fd;
            j = 0x82;
            k = 2;
            get_can_data(j,k);
            break ;

        default :
            g_u16DataBuffer0[1] = NOVCOM ;
            break ;
    }/* switch end*/
}

/******************************************************************
    函数名称 :  sget_download_answer
    入口参数 :  无
    出口参数 :  无
    函数功能 :  下载时模块的应答
    子函数    :
    修改内容 :  mhp 20110407
******************************************************************/
void sget_download_answer(void)
{
    unsigned int uiDataHigh,uiDataLow;

    Flag_TrigFd = 0;
    g_u16DataBuffer0[1] = SGET_DOWNLOAD_ANSWER;
    g_u16DataBuffer0[2] = 8 ;
    g_u16DataBuffer0[3] = 0 ;


    uiDataHigh = (u16LoadStatus & 0xFF00) >> 8;
    uiDataLow = u16LoadStatus & 0x00FF;
    g_u16DataBuffer0[4] = uiDataHigh;
    g_u16DataBuffer0[5] = uiDataLow;

    uiDataHigh = (u16LoadAddrH & 0xFF00) >> 8;
    uiDataLow = u16LoadAddrH & 0x00FF;
    g_u16DataBuffer0[6] = uiDataHigh;
    g_u16DataBuffer0[7] = uiDataLow;

    uiDataHigh = (u16LoadAddrLL & 0xFF00) >> 8;
    uiDataLow = u16LoadAddrLL & 0x00FF;
    g_u16DataBuffer0[8] = uiDataHigh;
    g_u16DataBuffer0[9] = uiDataLow;

    uiDataHigh = (u16LoadSize & 0xFF00) >> 8;
    uiDataLow = u16LoadSize & 0x00FF;
    g_u16DataBuffer0[10] = uiDataHigh;
    g_u16DataBuffer0[11] = uiDataLow;

}


/******************************************************************
    函数名称 :  sget_download_addr
    入口参数 :  无
    出口参数 :  无
    函数功能 :  下载地址和长度的?
    子函数    :    Box5ID ，Box5AddrSend
    修改内容 :  mhp 20110407
******************************************************************/
void sget_download_addr(void)
{
    unsigned int uiDataHigh,uiDataLow;
    unsigned int uiDataLength;
    struct ECAN_REGS ECanaShadow;

    uiDataLength = ( g_u16DataBuffer0[3] << 8 ) | g_u16DataBuffer0[2];
    uiDataHigh = g_u16DataBuffer0[5] ;
    uiDataLow = g_u16DataBuffer0[6] ;
    u16LoadAddr = (uiDataHigh << 8) | uiDataLow;

    if( ( uiDataLength == 0x12 ) &&
      ( ( ( g_u16DataBuffer0[5] == 0x80 ) && ( g_u16DataBuffer0[6] == 0xF0 ) ) ||
        ( ( g_u16DataBuffer0[5] == 0x81 ) && ( g_u16DataBuffer0[6] == 0xF1 ) ) ) )
    {
        u16CANlen = 8;

        uiDataHigh = g_u16DataBuffer0[7] ;
        uiDataLow = g_u16DataBuffer0[8] ;
        u16LoadAddrL = (uiDataHigh << 8) | uiDataLow;

        uiDataHigh = g_u16DataBuffer0[9] ;
        uiDataLow = g_u16DataBuffer0[10] ;
        u16LoadSize = (uiDataHigh << 8) | uiDataLow;

        uiDataHigh = g_u16DataBuffer0[11] ;
        uiDataLow = g_u16DataBuffer0[12] ;
        u16LoadAddrH = (uiDataHigh << 8) | uiDataLow;

        //ECanaRegs.CANME.all = ECanaRegs.CANME.all & 0xFFFFFFDF;
        EALLOW;
        ECanaShadow.CANME.all = ECanaRegs.CANME.all;
        ECanaShadow.CANME.bit.ME5 = 0;
        ECanaRegs.CANME.all = ECanaShadow.CANME.all;
        EDIS;
        asm (" NOP ");

        ECanaMboxes.MBOX5.MSGID.bit.IDE = 1;
        ECanaMboxes.MBOX5.MSGID.bit.AME = 1;
        ECanaMboxes.MBOX5.MSGID.bit.AAM = 0;
        if( g_u16DataBuffer0[5] == 0x80 )
        {
            ECanaMboxes.MBOX5.MSGID.bit.PROTNO = 0x1CA;
        }
        else
        {
            ECanaMboxes.MBOX5.MSGID.bit.PROTNO = 0x1CD;
        }
        ECanaMboxes.MBOX5.MSGID.bit.PTP = 1;
        ECanaMboxes.MBOX5.MSGID.bit.DSTADDRH = 0x7;       // first, only addr
        ECanaMboxes.MBOX5.MSGID.bit.DSTADDRL = 0x0;       //
        ECanaMboxes.MBOX5.MSGID.bit.SRCADDR = 0x0;
        ECanaMboxes.MBOX5.MSGID.bit.CNT = 0;
        ECanaMboxes.MBOX5.MSGID.bit.RES1 = 1;
        ECanaMboxes.MBOX5.MSGID.bit.RES2 = 1;

        ECanaMboxes.MBOX5.MSGCTRL.all = 0x00000008;

        //ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x00000020;
        EALLOW;
        ECanaShadow.CANME.all = ECanaRegs.CANME.all;
        ECanaShadow.CANME.bit.ME5 = 1;
        ECanaRegs.CANME.all = ECanaShadow.CANME.all;
        EDIS;

        //Box5AddrSend();
        DownloadAddrSend(u16CANlen);
    }
    else if( ( uiDataLength  == 0x0A ) &&
             ( g_u16DataBuffer0[5] >= 0x80 ) &&
             ( g_u16DataBuffer0[5] <= 0x9F ) )
    {
        u16CANlen = 4;

        uiDataHigh = g_u16DataBuffer0[7] ;
        uiDataLow = g_u16DataBuffer0[8] ;

        u16LoadSize = (uiDataHigh << 8) | uiDataLow;

        //Box5ID(0,u16CANlen);
        //Box5AddrSend();
        sEncodeMailBox5ID(0, u16CANlen);
        DownloadAddrSend(u16CANlen);
    }
    else if( ( uiDataLength  == 0x0E ) &&
             ( g_u16DataBuffer0[5] == 0x00 ) )
    {
        u16CANlen = 6;

        uiDataHigh = g_u16DataBuffer0[7] ;
        uiDataLow = g_u16DataBuffer0[8] ;

        u16LoadAddrL = (uiDataHigh << 8) | uiDataLow;

        uiDataHigh = g_u16DataBuffer0[9] ;
        uiDataLow = g_u16DataBuffer0[10] ;

        u16LoadSize = (uiDataHigh << 8) | uiDataLow;

        //Box5ID(0,u16CANlen);
        //Box5AddrSend();
        sEncodeMailBox5ID(0, u16CANlen);
        DownloadAddrSend(u16CANlen);
    }
    else if( uiDataLength  == 0x06 )
    {
        u16CANlen = 2;

        //Box5ID(0,u16CANlen);
        //Box5AddrSend();
        sEncodeMailBox5ID(0, u16CANlen);
        DownloadAddrSend(u16CANlen);
    }
    g_u16DataBuffer0[1] = SGET_DOWNLOAD_ADDR;
    g_u16DataBuffer0[2] = 8;
    g_u16DataBuffer0[3] = 0;
}

/******************************************************************
    函数名称 :  sget_download_data
    入诓问?:   无
    出口参数 :  无
    函数功能 :  下厥莸姆⑺?
    子函数    :     Box5ID,Box5DataSend
    修改内容 :  mhp 20110402,
******************************************************************/
void sget_download_data(void)
{
    unsigned char code_type;
    unsigned char i,m;

    code_type = g_u16DataBuffer0[4] ;

    g_u16DataBuffer0[1] = SGET_DOWNLOAD_DATA;
    g_u16DataBuffer0[2] = 4 ;
    g_u16DataBuffer0[3] = 0 ;

    i = 6;

    if(u16LoadSize % 8 == 0)
    {
        for ( m=0; m < (u16LoadSize >> 3); m++ )
        {
            fLoadDataA.intdata[0].bitdata.lowchar = g_u16DataBuffer0[i] ;
            fLoadDataA.intdata[0].bitdata.highchar = g_u16DataBuffer0[i + 1] ;
            fLoadDataA.intdata[1].bitdata.lowchar = g_u16DataBuffer0[i + 2] ;
            fLoadDataA.intdata[1].bitdata.highchar = g_u16DataBuffer0[i + 3] ;

            fLoadDataB.intdata[0].bitdata.lowchar = g_u16DataBuffer0[i + 4] ;
            fLoadDataB.intdata[0].bitdata.highchar = g_u16DataBuffer0[i + 5 ];
            fLoadDataB.intdata[1].bitdata.lowchar = g_u16DataBuffer0[i + 6] ;
            fLoadDataB.intdata[1].bitdata.highchar = g_u16DataBuffer0[i + 7] ;

            i = i + 8;

            if(m == (u16LoadSize >> 3))
            {
                if (u16LoadSize % 8)
                {
                    u16CANlen = u16LoadSize % 8;
                }
                else
                {
                    u16CANlen = 8;
                }
            }
            else
            {
                u16CANlen = 8;
            }

            //Box5ID(m,u16CANlen);
            //Box5DataSend();
            sEncodeMailBox5ID(m, u16CANlen);
            sEncodeCanTxU16Data(fLoadDataA.intdata[1].id, fLoadDataA.intdata[0].id,
                                fLoadDataB.intdata[1].id, fLoadDataB.intdata[0].id);
            sCanHdSend5(&g_stCanTxData);
            DELAY_US(1500);
        }
    }
    else
    {
        for ( m=0; m <= (u16LoadSize>>3); m++ )
        {
            fLoadDataA.intdata[0].bitdata.lowchar = g_u16DataBuffer0[i] ;
            fLoadDataA.intdata[0].bitdata.highchar = g_u16DataBuffer0[i + 1] ;
            fLoadDataA.intdata[1].bitdata.lowchar = g_u16DataBuffer0[i + 2] ;
            fLoadDataA.intdata[1].bitdata.highchar = g_u16DataBuffer0[i + 3] ;

            fLoadDataB.intdata[0].bitdata.lowchar = g_u16DataBuffer0[i + 4] ;
            fLoadDataB.intdata[0].bitdata.highchar = g_u16DataBuffer0[i + 5 ];
            fLoadDataB.intdata[1].bitdata.lowchar = g_u16DataBuffer0[i + 6] ;
            fLoadDataB.intdata[1].bitdata.highchar = g_u16DataBuffer0[i + 7] ;

            i= i + 8;

            if(m == ((u16LoadSize >> 3) ))
            {
                if (u16LoadSize % 8)
                {
                    u16CANlen = u16LoadSize % 8;
                }
                else
                {
                    u16CANlen = 8;
                }
            }
            else
            {
                u16CANlen = 8;
            }

            //Box5ID(m,u16CANlen);
            //Box5DataSend();
            sEncodeMailBox5ID(m, u16CANlen);
            sEncodeCanTxU16Data(fLoadDataA.intdata[1].id, fLoadDataA.intdata[0].id,
                                fLoadDataB.intdata[1].id, fLoadDataB.intdata[0].id);
            sCanHdSend5(&g_stCanTxData);
            DELAY_US(1500);
        }
    }
}

/******************************************************************
    函数名称 :  sget_memorytest_data
    入口参数 :  无
    出口参数 :  无
    函数功能 :
    子函数   : recycle
    修改内容 :
******************************************************************/
void sget_memorytest_data(void)
{
    unsigned char j,k;

    ucMsgType = 0x03;

    g_u16DataBuffer0[1] = SET_MEMORYTEST_DATA;
    g_u16DataBuffer0[2] = 36 ;
    g_u16DataBuffer0[3] = 0 ;

    j = 0xe0;
    k = 1;
    get_can_data(j,k);
    g_u16DataBuffer0[4] = uDcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[5] = uDcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[6] = uDcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[7] = uDcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[8] = uDcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[9] = uDcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[10] = uDcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[11] = uDcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[12] = uDcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[13] = uDcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[14] = uDcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[15] = uDcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[16] = uDcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[17] = uDcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[18] = uDcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[19] = uDcReadRamCon.intdata[1].bitdata.lowchar;
}

void sget_memorytest1_data(void)
{
    unsigned char j,k;

    ucMsgType = 0x03;

    g_u16DataBuffer0[1] = SET_MEMORYTEST1_DATA;
    g_u16DataBuffer0[2] = 35 ;
    g_u16DataBuffer0[3] = 0 ;

    j = 0xe8;
    k = 1;
    get_can_data(j,k);
    g_u16DataBuffer0[4] = uDcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[5] = uDcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[6] = uDcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[7] = uDcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[8] = uDcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[9] = uDcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[10] = uDcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[11] = uDcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[12] = uDcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[13] = uDcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[14] = uDcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[15] = uDcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[16] = uDcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[17] = uDcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[18] = uDcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[19] = uDcReadRamCon.intdata[1].bitdata.lowchar;
}
void sget_memorytest2_data(void)
{
    unsigned char j,k;

    ucMsgType = 0x03;

    g_u16DataBuffer0[1] = SET_MEMORYTEST2_DATA;
    g_u16DataBuffer0[2] = 34 ;
    g_u16DataBuffer0[3] = 0 ;

    j = 0xe4;
    k = 1;
    get_can_data(j,k);
    g_u16DataBuffer0[4] = uAcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[5] = uAcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[6] = uAcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[7] = uAcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[8] = uAcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[9] = uAcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[10] = uAcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[11] = uAcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[12] = uAcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[13] = uAcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[14] = uAcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[15] = uAcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[16] = uAcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[17] = uAcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[18] = uAcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[19] = uAcReadRamCon.intdata[1].bitdata.lowchar;
}
void sget_memorytest3_data(void)
{
    unsigned char j,k;

    ucMsgType = 0x03;

    g_u16DataBuffer0[1] = SET_MEMORYTEST3_DATA;
    g_u16DataBuffer0[2] = 33 ;
    g_u16DataBuffer0[3] = 0 ;

    j = 0xe9;
    k = 1;
    get_can_data(j,k);
    g_u16DataBuffer0[4] = uAcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[5] = uAcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[6] = uAcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[7] = uAcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[8] = uAcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[9] = uAcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[10] = uAcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[11] = uAcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[12] = uAcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[13] = uAcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[14] = uAcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[15] = uAcReadRamCon.intdata[1].bitdata.lowchar;
    get_can_data(j,k);
    g_u16DataBuffer0[16] = uAcReadRamCon.intdata[0].bitdata.highchar;
    g_u16DataBuffer0[17] = uAcReadRamCon.intdata[0].bitdata.lowchar;
    g_u16DataBuffer0[18] = uAcReadRamCon.intdata[1].bitdata.highchar;
    g_u16DataBuffer0[19] = uAcReadRamCon.intdata[1].bitdata.lowchar;
}
/******************************************************************/
/*-------通信指令处理部分-------*/
/******************************************************************
    函数名称 :  wr_s_contrl
    入口参数 :  无
    出口参数 :  无
    函数功能 :  处理对模块的控制命令
    子函数   : 无
    修改:     mhp 20031224  (case No. ???)
                多－－设置均流数据上送
                少－－过压保护重启时间，带载软启动时间，
                      复位，模块是否WALk IN，风扇是否全速
******************************************************************/
void wr_s_contrl(void)
{
    unsigned char code_type,j,k;
    ubitfloat      comflt ;

    ucMsgType = 0x03;
    code_type = g_u16DataBuffer0[4] ;
    //g_u16DataBuffer0[1] = WR_S_CONTRL ;
    //g_u16DataBuffer0[2] = 0 ;
    //g_u16DataBuffer0[3] = 0 ;

    if((code_type != 0x17) && (code_type != 0x18)) //ls 2013.4.3, for download problem
    {
        g_u16DataBuffer0[1] = WR_S_CONTRL ;
        g_u16DataBuffer0[2] = 0 ;
        g_u16DataBuffer0[3] = 0 ;
    }

    comflt.intdata[0].bitdata.highchar = g_u16DataBuffer0[6] ;
    comflt.intdata[0].bitdata.lowchar = g_u16DataBuffer0[7] ;
    comflt.intdata[1].bitdata.highchar = g_u16DataBuffer0[8] ;
    comflt.intdata[1].bitdata.lowchar = g_u16DataBuffer0[9] ;

    if(u16MdlType == Converter) //zlm add Converter protocol
    {
        switch (code_type)
        {
            case 0x01 : /* 调节限流点  */
                setlimit.fd = comflt.fd;
                j = 0x08; //0x22;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x02 : /* 调节限功率点  */
                setpower.fd = comflt.fd;
                j = 0x17; //0x20;
                k = 1;
                get_can_data(j,k);
                break ;

            //case 0x03 :   /* 设置ac 缺相动作*/
            /*  acfault_send=comflt.intdata[0].bitdata.highchar;
                j = 0x3d;
                k = 1;
                get_can_data(j,k);
                break ;*/

            case 0x04 : /* 设置开/关机 */
                openstatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x80; //0x30;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x05 : /* 调节模块输出电压微调量 */
                volt_adj_delta.fd = comflt.fd;
                j = 0x12;//0x26;
                k = 1;
                get_can_data(j,k);
                break ;

            //case 0x06 :   /* 设置过压脱离 */
            /*  ovrelaystatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x35;
                k = 1;
                get_can_data(j,k);
                break ;*/

            case 0x07 : /* 调节模块输出电压 */
                setvolt.fd = comflt.fd;
                j = 0x07; //0x21;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x08 : /* 设置inbus 电压 */
                setpfcvolt.fd = comflt.fd;
                j = 0x0F;//0x27;
                k = 1;
                get_can_data(j,k);
                break ;

            /*
            下述为单条设置命令，Converter监控的操作是通过00综合命令按位操作进行的
            下述更改命令号码，（GBB的通讯协议号从0x0030开始-0x003F，现更改成0x0080开始，其余定义一致），
            单条设置命令0x0080，0x0081，0x0082，0x0083，0x0084在00命令中按位设置控制，
            0x0086~0x008F 单条设置，建议所有需要增加
            */

            case 0x09 : /* 设置识别闪烁*/
                twinklestatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x84;//0x34;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0a : /* 设置WALK-IN */
                walkinstatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x82;//0x32;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0b : /* 设置风扇调速*/
                fullspeedstatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x83;//0x33;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0c : /* 设置模块复位*/
                resetstatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x81;//0x31;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0d : /* 设置模块是否处于校准状态*/
                permitadjest=comflt.intdata[0].bitdata.highchar;
                j = 0x87;//0x37;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0e : /* 设置模块风扇版本*/
                bigfan=comflt.intdata[0].bitdata.highchar;
                j = 0x88;//0x38;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0f : /* 设置模块输出过压保护版本*/
                overvoltprot=comflt.intdata[0].bitdata.highchar;
                j = 0x89;//0x39;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x10 : /* 设置模块是否允许输入过压保护*/
                uPermitOverAc=comflt.intdata[0].bitdata.highchar;
                j = 0x8A;//0x3A;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x11 : /* 设置模块是否允许输入过压保护*/
                uPermitOverDc=comflt.intdata[0].bitdata.highchar;
                j = 0x8E;//0x3E;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x12 : // 设置是否进行内存测试更改为选择模块
                regstatus_send=comflt.intdata[0].bitdata.highchar;
                //20110505 mhp change to choose HVDC
                //j = 0x3B;
                //k = 1;
                //get_can_data(j,k);
                if(regstatus_send == HVDC)
                {
                    //hvdc_id_init();
                    u16MdlType = HVDC;
                }
                else if(regstatus_send == Converter) //zlm
                {
                    //Converter_id_init();
                    u16MdlType = Converter;
                }
                else if(regstatus_send == IS_RECT) //mhp 20130329
                {
                    //ISrect_id_init();
                    u16MdlType = IS_RECT;
                }
                else if(regstatus_send == MPPT) //mhp 20130925
                {
                    //MPPT_id_init();
                    u16MdlType = MPPT;
                }
                else if(regstatus_send == MPPT_HV) //mhp 20131216
                {
                    //MPPT_id_init();
                    u16MdlType = MPPT_HV;
                }
                else if(regstatus_send == efuse) //mhp 20131216
                {
                    //efuse_id_init();
                    u16MdlType = efuse;
                }
                else
                {
                    //rect_id_init();
                    u16MdlType = RECT;
                }
                break ;

            case 0x13 : /* 设置模块是否进行下载*/
                download_permit=comflt.intdata[0].bitdata.highchar;
                j = 0xFE;
                k = 1;
                get_can_data(j,k);
                break ;

            // 0x14,0x15 leave for 200A calibration

            case 0x16 : /* 设置模块是否进行ESTOP 测试*/
                estop_permit=comflt.intdata[0].bitdata.highchar;      // mhp 20081028
                j = 0x16;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x17 : /* 设置模块进行normal download*/
                download_permit = comflt.intdata[0].bitdata.highchar;     // mhp 20110402
                //ls 2013.4.3, for download problem
                /*
                j = 0xFE;
                k = 1;
                get_can_data(j,k);
                */
                //CAN_MBX4B = (ucMsgType<<8) | 0x00f0;
                //CAN_MBX4A = 0xFE;
                //CAN_MBX4C = 0;
                //CAN_MBX4D = download_permit;
                //CAN_TCR = 0x0040;  //transmit request set (TRS4);

                g_u16MsgType = ucMsgType;
                g_u16ErrType = 0x00f0;
                g_u16ValueType = 0;
                g_ubitfCanData.intdata[1].id = 0;
                g_ubitfCanData.intdata[0].id = download_permit;//和模块对应不上，模块接收是【1】，需要核对
                sEncodeCanTxID1(NACMP,PTP_MODE,CSU_ADDR,DATA_END);
                sEncodeCanTxData(g_u16MsgType,g_u16ErrType,g_u16ValueType,g_ubitfCanData);
                sCanHdSend4(&g_stCanTxData);
                break ;

            case 0x18 : /*设置模块进行force download*/
                //ECanaRegs.CANME.all  = ECanaRegs.CANME.all  & 0xFFFFFFF7;
                //zbw 20111114
                EALLOW;
                ECanaShadow.CANME.all = ECanaRegs.CANME.all;
                ECanaShadow.CANME.bit.ME3 = 0;
                ECanaRegs.CANME.all = ECanaShadow.CANME.all;
                EDIS;
                asm ( " NOP " );
                if( comflt.intdata[0].bitdata.highchar == 1 )
                {
                    EALLOW;
                    ECanaMboxes.MBOX3.MSGID.bit.IDE = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AME = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AAM = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.PROTNO = 0x1CA;
                    ECanaMboxes.MBOX3.MSGID.bit.PTP = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRH = address/32;            // first, only addr 0
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRL = address%32;
                    ECanaMboxes.MBOX3.MSGID.bit.SRCADDR = 0x0E0;      // CAN box addr:0xE0
                    ECanaMboxes.MBOX3.MSGID.bit.CNT = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.RES1 = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.RES2 = 1;             // 1CA80703
                    EDIS;
                }
                else if( comflt.intdata[0].bitdata.highchar == 2 )
                {
                    EALLOW;
                    ECanaMboxes.MBOX3.MSGID.bit.IDE = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AME = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AAM = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.PROTNO = 0x1CD;
                    ECanaMboxes.MBOX3.MSGID.bit.PTP = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRH = address/32;            // first, only addr 0
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRL = address%32;
                    ECanaMboxes.MBOX3.MSGID.bit.SRCADDR = 0x0E0;      // CAN box addr:0xE0
                    ECanaMboxes.MBOX3.MSGID.bit.CNT = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.RES1 = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.RES2 = 1;             // 1CA80703
                    EDIS;
                }
                EALLOW;
                ECanaMboxes.MBOX3.MSGCTRL.all = 0;
                //ECanaRegs.CANME.all  = ECanaRegs.CANME.all  | 0x00000008;
                ECanaShadow.CANME.all = ECanaRegs.CANME.all;
                ECanaShadow.CANME.bit.ME3 = 1;
                ECanaRegs.CANME.all = ECanaShadow.CANME.all;
                /*set transmit request to start transmit process*/
                ECanaShadow.CANTRS.all = ECanaRegs.CANTRS.all;
                ECanaShadow.CANTRS.bit.TRS3 = 1;
                ECanaRegs.CANTRS.all = ECanaShadow.CANTRS.all;
                EDIS;

                DELAY_US(2000);
                break;

            // 0x19,0x1a,0x1b,0x1c,0x1d leave for 200A calibration and read/write RAM

            /*case 0x1e :   // 设置模块AC限功率模式 默认0:模式1；1：模式2  //20111123
                AcLimPowModeFlag = comflt.intdata[0].bitdata.highchar;
                j = 0x2f;
                k = 1;
                get_can_data(j,k);
                break ;*/

            case 0x1f : /* 设置模块均流模式 默认0:按百分比；1：按实际电流 */   //20111125
                ShareModeFlag = comflt.intdata[0].bitdata.highchar;
                //j = 0x2f;
                //k = 1;
                //get_can_data(j,k);
                if(ShareModeFlag)
                {
                    u16ShareMode = LOAD_PCT;
                }
                else
                {
                    u16ShareMode = REAL_CURR;
                }
                break ;

            case 0x28 : // 设置通讯方式为综合命令还是单条命令
                regstatus_send=comflt.intdata[0].bitdata.highchar;
                if(regstatus_send == 0)
                {
                    u16ComType = COM_SINGLE;
                }
                else
                {
                    u16ComType = COM_0X00;
                }
                break;  //ls 20150928

            default :
                g_u16DataBuffer0[1] = NOVCOM ;
                break ;
        }
    }
    else if(u16MdlType == efuse)//REC 、HVDC
    {
        switch (code_type)
        {
                case 0x0d : /* 设置模块是否处于校准状态*/
                permitadjest=comflt.intdata[0].bitdata.highchar;
                j = 0x31;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x12 : // 设置是否进行内存测试更改为选择模块
                regstatus_send=comflt.intdata[0].bitdata.highchar;
                //20110505 mhp change to choose HVDC
                //j = 0x3B;
                //k = 1;
                //get_can_data(j,k);
                if(regstatus_send == HVDC)
                {
                   // hvdc_id_init();
                    u16MdlType = HVDC;
                }
                else if(regstatus_send == Converter) //zlm
                {
                    //Converter_id_init();
                    u16MdlType = Converter;
                }
                else if(regstatus_send == IS_RECT) //mhp 20130329
                {
                    //ISrect_id_init();
                    u16MdlType = IS_RECT;
                }
                else if(regstatus_send == MPPT) //mhp 20130925
                {
                    //MPPT_id_init();
                    u16MdlType = MPPT;
                }
                else if(regstatus_send == MPPT_HV) //mhp 20131216
                {
                    //MPPT_id_init();
                    u16MdlType = MPPT_HV;
                }
                else if(regstatus_send == EPA) //mhp 20131216
                {
                    //EPA_id_init();
                    u16MdlType = EPA;
                }
                else if(regstatus_send == efuse) //mhp 20131216
                {
                    //efuse_id_init();
                    u16MdlType = efuse;
                }
                else
                {
                    //rect_id_init();
                    u16MdlType = RECT;
                }
                break ;

            case 0x13 : /* 设置模块是否进行下载*/
                download_permit=comflt.intdata[0].bitdata.highchar;
                j = 0xFE;
                k = 1;
                get_can_data(j,k);
                break ;

            // 0x14,0x15 leave for 200A calibration

            case 0x16 : /* 设置模块是否进行ESTOP 测试*/
                estop_permit=comflt.intdata[0].bitdata.highchar;      // mhp 20081028
                j = 0x32;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x17 : /* 设置模块进行normal download*/
                download_permit = comflt.intdata[0].bitdata.highchar;     // mhp 20110402
                //ls 2013.4.3, for download problem
                /*
                j = 0xFE;
                k = 1;
                get_can_data(j,k);
                */
                //CAN_MBX4B = (ucMsgType<<8) | 0x00f0;
                //CAN_MBX4A = 0xFE;
                //CAN_MBX4C = 0;
                //CAN_MBX4D = download_permit;
                //CAN_TCR = 0x0040;  //transmit request set (TRS4);

                g_u16MsgType = ucMsgType;
                g_u16ErrType = 0x00f0;
                g_u16ValueType = 0;
                g_ubitfCanData.intdata[1].id = 0;
                g_ubitfCanData.intdata[0].id = download_permit;//和模块对应不上，模块接收是【1】，需要核对
                sEncodeCanTxID1(EMP,PTP_MODE,CSU_ADDR,DATA_END);
                sEncodeCanTxData(g_u16MsgType,g_u16ErrType,g_u16ValueType,g_ubitfCanData);
                sCanHdSend4(&g_stCanTxData);

                break ;

            case 0x18 : /*设置模块进行force download*/
                //ECanaRegs.CANME.all  = ECanaRegs.CANME.all  & 0xFFFFFFF7;
                EALLOW;
                ECanaShadow.CANME.all = ECanaRegs.CANME.all;
                ECanaShadow.CANME.bit.ME3 = 0;
                ECanaRegs.CANME.all = ECanaShadow.CANME.all;
                EDIS;
                asm ( " NOP " );
                if( comflt.intdata[0].bitdata.highchar == 1 )
                {
                    EALLOW;
                    ECanaMboxes.MBOX3.MSGID.bit.IDE = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AME = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AAM = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.PROTNO = 0x1CA;
                    ECanaMboxes.MBOX3.MSGID.bit.PTP = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRH = address/32;            // first, only addr 0
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRL = address%32;
                    ECanaMboxes.MBOX3.MSGID.bit.SRCADDR = 0x0E0;      // CAN box addr:0xE0
                    ECanaMboxes.MBOX3.MSGID.bit.CNT = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.RES1 = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.RES2 = 1;             // 1CA80703
                    EDIS;
                }
                else if( comflt.intdata[0].bitdata.highchar == 2 )
                {
                    EALLOW;
                    ECanaMboxes.MBOX3.MSGID.bit.IDE = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AME = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AAM = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.PROTNO = 0x1CD;
                    ECanaMboxes.MBOX3.MSGID.bit.PTP = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRH = address/32;            // first, only addr 0
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRL = address%32;
                    ECanaMboxes.MBOX3.MSGID.bit.SRCADDR = 0x0E0;      // CAN box addr:0xE0
                    ECanaMboxes.MBOX3.MSGID.bit.CNT = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.RES1 = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.RES2 = 1;             // 1CA80703
                    EDIS;
                }
                EALLOW;
                ECanaMboxes.MBOX3.MSGCTRL.all = 0;
                //ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x00000008;
                ECanaShadow.CANME.all = ECanaRegs.CANME.all;
                ECanaShadow.CANME.bit.ME3 = 1;
                ECanaRegs.CANME.all = ECanaShadow.CANME.all;
                /*set transmit request to start transmit process*/
                ECanaShadow.CANTRS.all = ECanaRegs.CANTRS.all;
                ECanaShadow.CANTRS.bit.TRS3 = 1;
                ECanaRegs.CANTRS.all = ECanaShadow.CANTRS.all;
                EDIS;
                DELAY_US(2000);
                break;

            case 0x21 :     // 模块写ID解锁，预留，不清楚用处
                if (comflt.intdata[0].bitdata.highchar)
                {
                    u16EnWrIDLo = 0x55AA;
                    u16EnWrIDHi = 0x55AA;
                }
                else
                {
                    u16EnWrIDLo = 0x5500;
                    u16EnWrIDHi = 0x5500;
                }
                j = 0xA3;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x22 :     // 设置模块CPU复位
                uSetCpuRst = comflt.intdata[0].bitdata.highchar;
                j = 0xFC;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x24 : /* 清校准默认值*/
                u16ClrEpromFlag = comflt.intdata[0].bitdata.highchar;
                j = 0xEC;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x28 : // 设置通讯方式为综合命令还是单条命令
                regstatus_send=comflt.intdata[0].bitdata.highchar;
                if(regstatus_send == 0)
                {
                    u16ComType = COM_SINGLE;
                }
                else
                {
                    u16ComType = COM_0X00;
                }
                break;  //ls 20150928

            default :
                g_u16DataBuffer0[1] = NOVCOM ;
                break ;


        }
    }
    else //REC 、HVDC
    {
        switch (code_type)
        {
            case 0x01 : /* 调节限流点  */
                setlimit.fd = comflt.fd;
                j = 0x22;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x02 : /* 调节限功率点  */
                setpower.fd = comflt.fd;
                j = 0x20;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x03 : /* 设置ac 缺相动作*/
                acfault_send=comflt.intdata[0].bitdata.highchar;
                j = 0x3d;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x04 : /* 设置开/关机 */
                openstatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x30;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x05 : /* 调节模块输出电压微调量 */
                volt_adj_delta.fd = comflt.fd;
                j = 0x26;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x06 : /* 设置过压脱离 */
                ovrelaystatus_send=comflt.intdata[0].bitdata.highchar;
                //j = 0x35;
                if((u16MdlType == MPPT) || (u16MdlType == MPPT_HV))
                {
                    j = 0x9F;
                }
                else
                {
                    j = 0x35;
                }

                k = 1;
                get_can_data(j,k);
                break ;

            case 0x07 : /* 调节模块输出电压 */
                setvolt.fd = comflt.fd;
                j = 0x21;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x08 : /* 设置pfc电压 */
                setpfcvolt.fd = comflt.fd;
                j = 0x27;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x09 : /* 设置识别闪烁*/
                twinklestatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x34;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0a : /* 设置WALK-IN */
                walkinstatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x32;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0b : /* 设置风扇调速*/
                fullspeedstatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x33;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0c : /* 设置模块复位*/
                resetstatus_send=comflt.intdata[0].bitdata.highchar;
                j = 0x31;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0d : /* 设置模块是否处于校准状态*/
                permitadjest=comflt.intdata[0].bitdata.highchar;
                j = 0x37;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0e : /* 设置模块风扇版本*/
                bigfan=comflt.intdata[0].bitdata.highchar;
                j = 0x38;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0f : /* 设置模块输出过压保护版本*/
                overvoltprot=comflt.intdata[0].bitdata.highchar;
                j = 0x39;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x10 : /* 设置模块是否允许输入过压保护*/
                uPermitOverAc=comflt.intdata[0].bitdata.highchar;
                j = 0x3A;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x11 : /* 设置模块是否允许输入过压保护*/
                uPermitOverDc=comflt.intdata[0].bitdata.highchar;
                j = 0x3E;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x12 : // 设置是否进行内存测试更改为选择模块
                regstatus_send=comflt.intdata[0].bitdata.highchar;
                //20110505 mhp change to choose HVDC
                //j = 0x3B;
                //k = 1;
                //get_can_data(j,k);
                if(regstatus_send == HVDC)
                {
                    //hvdc_id_init();
                    u16MdlType = HVDC;
                }
                else if(regstatus_send == Converter) //zlm
                {
                    //Converter_id_init();
                    u16MdlType = Converter;
                }
                else if(regstatus_send == IS_RECT) //mhp 20130329
                {
                    //ISrect_id_init();
                    u16MdlType = IS_RECT;
                }
                else if(regstatus_send == MPPT) //mhp 20130925
                {
                    //MPPT_id_init();
                    u16MdlType = MPPT;
                }
                else if(regstatus_send == MPPT_HV) //mhp 20131216
                {
                    //MPPT_id_init();
                    u16MdlType = MPPT_HV;
                }
                else if(regstatus_send == EPA) //mhp 20131216
                {
                    //EPA_id_init();
                    u16MdlType = EPA;
                }
                else if(regstatus_send == efuse) //mhp 20131216
                {
                    //efuse_id_init();
                    u16MdlType = efuse;
                }
                else
                {
                    //rect_id_init();
                    u16MdlType = RECT;
                }
                break ;

            case 0x13 : /* 设置模块是否进行下载*/
                download_permit=comflt.intdata[0].bitdata.highchar;
                j = 0xFE;
                k = 1;
                get_can_data(j,k);
                break ;

            // 0x14,0x15 leave for 200A calibration

            case 0x16 : /* 设置模块是否进行ESTOP 测试*/
                estop_permit=comflt.intdata[0].bitdata.highchar;      // mhp 20081028
                j = 0x16;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x17 : /* 设置模块进行normal download*/
                download_permit = comflt.intdata[0].bitdata.highchar;     // mhp 20110402

                //CAN_MBX4B = (ucMsgType<<8) | 0x00f0;
                //CAN_MBX4A = 0xFE;
                //CAN_MBX4C = 0;
                //CAN_MBX4D = download_permit;
                //CAN_TCR = 0x0040;  //transmit request set (TRS4);

                g_u16MsgType = ucMsgType;
                g_u16ErrType = 0x00f0;
                g_u16ValueType = 0xFE;
                g_ubitfCanData.intdata[0].id = 0;
                g_ubitfCanData.intdata[1].id = download_permit;//和模块对应不上，模块接收是【1】，需要核对
                //sEncodeCanTxID1(EMP,PTP_MODE,CSU_ADDR,DATA_END);
                sEncodeCanTxData(g_u16MsgType,g_u16ErrType,g_u16ValueType,g_ubitfCanData);
                sCanHdSend4(&g_stCanTxData);

                break ;

            case 0x18 : /*设置模块进行force download*/
                //ECanaRegs.CANME.all  = ECanaRegs.CANME.all  & 0xFFFFFFF7;
                EALLOW;
                ECanaShadow.CANME.all = ECanaRegs.CANME.all;
                ECanaShadow.CANME.bit.ME3 = 0;
                ECanaRegs.CANME.all = ECanaShadow.CANME.all;
                EDIS;
                asm ( " NOP " );
                if( comflt.intdata[0].bitdata.highchar == 1 )
                {
                    EALLOW;
                    ECanaMboxes.MBOX3.MSGID.bit.IDE = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AME = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AAM = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.PROTNO = 0x1CA;
                    ECanaMboxes.MBOX3.MSGID.bit.PTP = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRH = address/32;            // first, only addr 0
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRL = address%32;
                    ECanaMboxes.MBOX3.MSGID.bit.SRCADDR = 0x0E0;      // CAN box addr:0xE0
                    ECanaMboxes.MBOX3.MSGID.bit.CNT = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.RES1 = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.RES2 = 1;             // 1CA80703
                    EDIS;
                }
                else if( comflt.intdata[0].bitdata.highchar == 2 )
                {
                    EALLOW;
                    ECanaMboxes.MBOX3.MSGID.bit.IDE = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AME = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.AAM = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.PROTNO = 0x1CD;
                    ECanaMboxes.MBOX3.MSGID.bit.PTP = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRH = address/32;            // first, only addr 0
                    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRL = address%32;
                    ECanaMboxes.MBOX3.MSGID.bit.SRCADDR = 0x0E0;      // CAN box addr:0xE0
                    ECanaMboxes.MBOX3.MSGID.bit.CNT = 0;
                    ECanaMboxes.MBOX3.MSGID.bit.RES1 = 1;
                    ECanaMboxes.MBOX3.MSGID.bit.RES2 = 1;             // 1CA80703
                    EDIS;
                }
                EALLOW;
                ECanaMboxes.MBOX3.MSGCTRL.all = 0;
                //ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x00000008;
                 ECanaShadow.CANME.all = ECanaRegs.CANME.all;
                 ECanaShadow.CANME.bit.ME3 = 1;
                 ECanaRegs.CANME.all = ECanaShadow.CANME.all;
                /*set transmit request to start transmit process*/
                ECanaShadow.CANTRS.all = ECanaRegs.CANTRS.all;
                ECanaShadow.CANTRS.bit.TRS3 = 1;
                ECanaRegs.CANTRS.all = ECanaShadow.CANTRS.all;
                EDIS;
                DELAY_US(2000);
                break;

            // 0x19,0x1a,0x1b,0x1c,0x1d leave for 200A calibration and read/write RAM

            case 0x1e : /* 设置模块AC限功率模式 默认0:模式1；1：模式2*/   //20111123
                AcLimPowModeFlag = comflt.intdata[0].bitdata.highchar;
                DCinMarnFlag = 0;
                j = 0x2f;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x1f : /* 设置模块均流模式 默认0:按百分比；1：按实际电流 */   //20111125
                ShareModeFlag = comflt.intdata[0].bitdata.highchar;
                //j = 0x2f;
                //k = 1;
                //get_can_data(j,k);
                if(ShareModeFlag)
                {
                    u16ShareMode = LOAD_PCT;
                }
                else
                {
                    u16ShareMode = REAL_CURR;
                }
                break ;

            // 0x20 for self test

            // 20130329 mhp add for IS rect
            case 0x21 :     // 模块写ID解锁
                if (comflt.intdata[0].bitdata.highchar)
                {
                    u16EnWrIDLo = 0x55AA;
                    u16EnWrIDHi = 0x55AA;
                }
                else
                {
                    u16EnWrIDLo = 0x5500;
                    u16EnWrIDHi = 0x5500;
                }
                j = 0xA3;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x22 :     // 设置模块CPU复位
                uSetCpuRst = comflt.intdata[0].bitdata.highchar;
                j = 0xFC;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x23 : /* 设置模块DC输入时是否告警 默认0:告警；1：不告警，正常工作*/
                DCinMarnFlag = comflt.intdata[0].bitdata.highchar;
                AcLimPowModeFlag = 0;
                j = 0x2f;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x24 : /* 清校准默认值*/
                u16ClrEpromFlag = comflt.intdata[0].bitdata.highchar;
                j = 0xEC;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x25 : /* 控制面板灯*/
                u16LEDFlag = comflt.intdata[0].bitdata.highchar;
                j = 0x9E;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x28 : // 设置通讯方式为综合命令还是单条命令
                regstatus_send=comflt.intdata[0].bitdata.highchar;
                if(regstatus_send == 0)
                {
                    u16ComType = COM_SINGLE;
                }
                else
                {
                    u16ComType = COM_0X00;
                }
                break;  //ls 20150928

            case 0x29 : /* 设置模块锁死 */
                modelstatus_lock=comflt.intdata[0].bitdata.highchar;
                j = 0x0b7;
                k = 1;
                get_can_data(j,k);
                break ;


            default :
                g_u16DataBuffer0[1] = NOVCOM ;
                break ;


        }

    }


    /* switch end */
}
/******************************************************************
    函数名称 :  wr_s_limit
    入口参数 :  无
    出口参数 :  无
    函数功能 :  写模块上、下限设置
    子函数   : 无
    修改:     mhp 20031226 电压上下限和过温点
                zlm 20120406 增加Converter 模块下发协议（与GBB模块区分）

******************************************************************/
void wr_s_limit(void)
{
    unsigned char code_type;
    ubitfloat       comflt;

    unsigned char j,k;

    ucMsgType = 0x03;
    code_type = g_u16DataBuffer0[4] ;
    g_u16DataBuffer0[1] = WR_S_LIMIT;
    g_u16DataBuffer0[2] = 0 ;
    g_u16DataBuffer0[3] = 0 ;

    comflt.intdata[0].bitdata.highchar = g_u16DataBuffer0[6] ;
    comflt.intdata[0].bitdata.lowchar = g_u16DataBuffer0[7] ;
    comflt.intdata[1].bitdata.highchar = g_u16DataBuffer0[8] ;
    comflt.intdata[1].bitdata.lowchar = g_u16DataBuffer0[9] ;

    if(u16MdlType == Converter) //zlm add Converter protocol
    {

        switch (code_type)
        {
            case 0x01:
                /* 输出控制电压上限 */
                dcvolt_up.fd = comflt.fd;       /* dcvolt_up <->acvolt */
                j = 0x0C;//0x23; //09,不写入EEPROM，0C写入EEPROM中
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x02 :
                /* 输出控制电压默认值*/
                dcvolt_float.fd = comflt.fd;
                j = 0x0A;//0x24;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x03 :
                /*模块过温点*/
                temp_up.fd=comflt.fd;
                j = 0x11;//0x25;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x04 :
                /*二次过压时间*/
                tagReontime.fd=comflt.fd;
                j = 0x13;//0x28;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x05 :
                /*WALK-IN TIME*/
                tagSstartTime.fd=comflt.fd;
                j = 0x14;//0x29;
                k = 1;
                get_can_data(j,k);
                break ;

            //case 0x06 :
                /*顺序起机TIME*/
                /*tagOpenTime.fd=comflt.fd;
                j = 0x2a;
                k = 1;
                get_can_data(j,k);
                break ;*/

            case 0x07 :
                /*模块运行TIME*/
                tagRunTimeSet.fd=comflt.fd;
                j = 0xa4;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x08 :
                /*模块curr*/
                dclimit_float.fd=comflt.fd;
                j = 0x0b;//0x19;
                k = 1;
                get_can_data(j,k);
                break ;
            case 0x09 :
                /*模块power*/
                dcpower_float.fd=comflt.fd;
                j = 0x15;//0x18;
                k = 1;
                get_can_data(j,k);
                break ;

            //case 0x0a :
                /*模块BAUD */
                /*dcbaud_float.fd=comflt.fd;
                j = 0x2b;
                k = 1;
                get_can_data(j,k);
                break ;*/

            case 0x0b :
                /*模块AC INPUT CURR SET */
                accurr_set.fd=comflt.fd;
                j = 0x18;//0x1a;
                k = 1;
                get_can_data(j,k);
                break ;


            default :
                g_u16DataBuffer0[1] = NOVCOM ;
                break ;
        }/* switch end*/

    }
    if(u16MdlType == efuse) //zlm add Converter protocol
    {
        switch (code_type)
        {
                case 0x01 : /* 设通道1电压上限  */
                set_efuseVoltUp1.fd = comflt.fd;
                j = 0x20;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x02 : /* 设通道2电压上限  */
                set_efuseVoltUp2.fd = comflt.fd;
                j = 0x21;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x03 : /* 设通道1电压下限*/
                set_efuseVoltDown1.fd = comflt.intdata[0].bitdata.highchar;
                j = 022;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x04 : /* 设置开关机 */
                openstatus_send = comflt.intdata[0].bitdata.highchar;
                j = 0x30;
                k = 1;
                get_can_data(j,k);

            case 0x05 : /* 设通道2电压下限 */
                set_efuseVoltDown2.fd = comflt.intdata[0].bitdata.highchar;
                j = 0x23;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x06 : /* 设通道1电流上限 */
                set_efuseCurrUp1.fd = comflt.fd;
                j = 0x24;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x07 : /* 设通道2电流上限 */
                set_efuseCurrUp2.fd = comflt.fd;
                j = 0x25;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x08 : /* 设置通道1过温点 */
                set_efuseOtp1.fd = comflt.fd;
                j = 0x26;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x09 : /* 设置通道2过温点 */
                set_efuseOtp2.fd = comflt.fd;
                j = 0x27;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0a :
                /*模块运行TIME*/
                tagRunTimeSet.fd=comflt.fd;
                j = 0xa4;
                k = 1;
                get_can_data(j,k);
                break ;

            default :
                g_u16DataBuffer0[1] = NOVCOM ;
                break ;
        }
    }
    else // REC 、HVDC
    {
        switch (code_type)
        {
            case 0x01:
                /* 输出控缪股舷?*/
                dcvolt_up.fd = comflt.fd;       /* dcvolt_up <->acvolt */
                j = 0x23;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x02 :
                /* 输出控制电压默认值*/
                dcvolt_float.fd = comflt.fd;
                j = 0x24;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x03 :
                /*模块过温点*/
                temp_up.fd=comflt.fd;
                j = 0x25;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x04 :
                /*二次过压时间*/
                tagReontime.fd=comflt.fd;
                j = 0x28;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x05 :
                /*WALK-IN TIME*/
                tagSstartTime.fd=comflt.fd;
                j = 0x29;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x06 :
                /*顺序起机TIME*/
                tagOpenTime.fd=comflt.fd;
                j = 0x2a;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x07 :
                /*模块运行TIME*/
                tagRunTimeSet.fd=comflt.fd;
                j = 0xa4;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x08 :
                /*模块curr*/
                dclimit_float.fd=comflt.fd;
                j = 0x19;
                k = 1;
                get_can_data(j,k);
                break ;
            case 0x09 :
                /*模块power*/
                dcpower_float.fd=comflt.fd;
                j = 0x18;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0a :
                /*模块BAUD */
                dcbaud_float.fd=comflt.fd;
                j = 0x2b;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0b :
                /*模块AC INPUT CURR SET */
                accurr_set.fd=comflt.fd;
                j = 0x1a;
                k = 1;
                get_can_data(j,k);
                break ;

            // add by mhp for IS, 20130428
            case 0x0C :     // 设模块电压下限
                dcvoltdn.fd = comflt.fd;
                j = 0x2D;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0D :     // 设模块手动电压
                dcvoltset.fd = comflt.fd;
                j = 0x2C;
                k = 1;
                get_can_data(j,k);
                break ;

            case 0x0E :     // 设模风扇占空比
                fandutyset.fd = comflt.fd;
                j = 0x2D;
                k = 1;
                get_can_data(j,k);
                break ;

            default :
                g_u16DataBuffer0[1] = NOVCOM ;
                break ;
        }/* switch end*/
    }

}

