/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : DSP280x_ECan.c
 *  PURPOSE  : DSP280x Enhanced CAN Initialization & Support Functions	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A00           zoulx             Created.   Pre-research 
 *============================================================================*/
//############################################################################
// Running on TMS320LF280xA   DCDC part   h741au1(1.00)               
// External clock is 20MHz, PLL * 10/2 , CPU-Clock 100 MHz	      
// Date: from May 23, 2005 to Oct 30, 2006  , (C) www & mhp & mj & lyg
// Version:1.00     Change Date: May 24, 2005 , 
//############################################################################

#include "DSP280x_Device.h"     // DSP28 Headerfile Include File

/*******************************************************************************
*Function name:	InitECan()
*Description:   This function initializes the eCAN module  to a known state                            
*******************************************************************************/
void InitECan(void)
{
	InitECanaGpio();
	//InitECana();
 	InitECana();  //ls 20111128
	//InitECanaID();
	InitECanaID(); //ls 20111128
}

/*******************************************************************************
*Function name:	InitECana()
*Parameters:  ECanaShadow:Create a shadow register structure for the CAN control
*             registers. This is needed, since, only 32-bit access is allowed to
*             these registers. 16-bit access to these registers could potentially
*             corrupt the register contents. This is especially true while writing
*             to a bit (or group of bits) among bits 16 - 31.
*Description:   This function initializes the eCAN module                            
*******************************************************************************/
void InitECana(void)	
{
	struct ECAN_REGS ECanaShadow;
	
	// EALLOW enables access to protected bits
	EALLOW;		

	//disable all mailbox
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.all = 0x00000000;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
	//ECanaRegs.CANME.all = 0x00000000;
	
	// Configure eCAN RX and TX pins for eCAN transmissions using eCAN register  
	ECanaShadow.CANTIOC.all = ECanaRegs.CANTIOC.all;
	ECanaShadow.CANTIOC.bit.TXFUNC = 1;
	ECanaRegs.CANTIOC.all = ECanaShadow.CANTIOC.all;

	ECanaShadow.CANRIOC.all = ECanaRegs.CANRIOC.all;
	ECanaShadow.CANRIOC.bit.RXFUNC = 1;
	ECanaRegs.CANRIOC.all = ECanaShadow.CANRIOC.all;

	// Configure eCAN for HECC mode - (reqd to access mailboxes 16 thru 31) 
	// HECC mode also enables time-stamping feature
	ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
	ECanaShadow.CANMC.bit.SUSP = 1;	//free mode
	ECanaShadow.CANMC.bit.SCB = 1;  //Select eCAN mode.
	ECanaShadow.CANMC.bit.ABO = 1;	//Auto bus on
	ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;
	    
	// TAn, RMPn, GIFn bits are all zero upon reset and are cleared again
	//	as a matter of precaution.
	ECanaRegs.CANTA.all	= 0xFFFFFFFF;	// Clear all TAn bits       
	ECanaRegs.CANRMP.all = 0xFFFFFFFF;	// Clear all RMPn bits       
	ECanaRegs.CANGIF0.all = 0xFFFFFFFF;	// Clear all interrupt flag bits  
	ECanaRegs.CANGIF1.all = 0xFFFFFFFF;
	
    ECanaRegs.CANMIL.all = 0x0;//1:interrupt generated on lin1,0:interrupt generated on lin0
    ECanaRegs.CANMIM.all = 0x03f;//1:enable,0:disable
    ECanaRegs.CANGIM.bit.I0EN = 1;

	// Configure bit timing parameters for eCANA
	ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
	ECanaShadow.CANMC.bit.CCR = 1 ;            // Set CCR = 1
	ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

	ECanaShadow.CANES.all = ECanaRegs.CANES.all;

	while(ECanaShadow.CANES.bit.CCE != 1 ) 	   // Wait for CCE bit to be set..
	{ECanaShadow.CANES.all = ECanaRegs.CANES.all;}   
    
	ECanaShadow.CANBTC.all = 0;

	ECanaShadow.CANBTC.bit.BRPREG = 14;		// TQ=(BRPREG+1)/SYSCLK/2=533ns, NTQ = 8us/533ns = 15
	ECanaShadow.CANBTC.bit.TSEG2REG = 5;	//2
	ECanaShadow.CANBTC.bit.TSEG1REG =8;	//10

 	ECanaShadow.CANBTC.bit.SAM = 0;			// 0: once sample 
 	ECanaShadow.CANBTC.bit.SJWREG = 3;		// 0: synchronization jump width is 1
 	ECanaRegs.CANBTC.all = ECanaShadow.CANBTC.all;
    
	ECanaShadow.CANMC.all = ECanaRegs.CANMC.all;
	ECanaShadow.CANMC.bit.CCR = 0 ;         // Set CCR = 0
	ECanaRegs.CANMC.all = ECanaShadow.CANMC.all;

	ECanaShadow.CANES.all = ECanaRegs.CANES.all;
    
	while(ECanaShadow.CANES.bit.CCE != 0 ) 	// Wait for CCE bit to be  cleared..
	{ECanaShadow.CANES.all = ECanaRegs.CANES.all;}   

	EDIS;
}	

//ls 20140812 CAN pro rewrite 0x62 by mistake, regulate CANID initiate sequence
void InitECanaID(void)
{
    struct ECAN_REGS ECanaShadow;

    EALLOW;
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.all = 0x00000000;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
    //ECanaRegs.CANME.all = 0x00000000;
    EDIS;

	ECanaMboxes.MBOX0.MSGID.bit.IDE = 1;
	ECanaMboxes.MBOX0.MSGID.bit.AME = 1;
	ECanaMboxes.MBOX0.MSGID.bit.AAM = 0;
	ECanaMboxes.MBOX0.MSGID.bit.PROTNO = 0x070;
	ECanaMboxes.MBOX0.MSGID.bit.PTP = 0;
	ECanaMboxes.MBOX0.MSGID.bit.DSTADDRH = 0x07;
	ECanaMboxes.MBOX0.MSGID.bit.DSTADDRL = 0x1f;
	ECanaMboxes.MBOX0.MSGID.bit.SRCADDR = 0x00;
    ECanaMboxes.MBOX0.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX0.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX0.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX1.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX1.MSGID.bit.PROTNO = 0x060;
    ECanaMboxes.MBOX1.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRH = 0;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRL = 0x01;
    ECanaMboxes.MBOX1.MSGID.bit.SRCADDR = 0x31;
    ECanaMboxes.MBOX1.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX1.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX1.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX2.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX2.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX2.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX2.MSGID.bit.PROTNO = 0x1CA;
    ECanaMboxes.MBOX2.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX2.MSGID.bit.DSTADDRH = 0x07;
    ECanaMboxes.MBOX2.MSGID.bit.DSTADDRL = 0x0;
    ECanaMboxes.MBOX2.MSGID.bit.SRCADDR = 0x00;
    ECanaMboxes.MBOX2.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX2.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX2.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX3.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX3.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX3.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX3.MSGID.bit.PROTNO = 0x1CA;
    ECanaMboxes.MBOX3.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRH = g_u8MdlAddresss/32;
    ECanaMboxes.MBOX3.MSGID.bit.DSTADDRL = g_u8MdlAddresss%32;
    ECanaMboxes.MBOX3.MSGID.bit.SRCADDR = 0x0E0;
    ECanaMboxes.MBOX3.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX3.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX3.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX4.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX4.MSGID.bit.PROTNO = 0x060;
    ECanaMboxes.MBOX4.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRH = g_u8MdlAddresss/32;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRL = g_u8MdlAddresss%32;
    ECanaMboxes.MBOX4.MSGID.bit.SRCADDR = 0x0F0;
    ECanaMboxes.MBOX4.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX4.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX4.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX5.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX5.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX5.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX5.MSGID.bit.PROTNO = 0x1D0;
    ECanaMboxes.MBOX5.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX5.MSGID.bit.DSTADDRH = g_u8MdlAddresss/32;
    ECanaMboxes.MBOX5.MSGID.bit.DSTADDRL = g_u8MdlAddresss%32;
    ECanaMboxes.MBOX5.MSGID.bit.SRCADDR = 0x0E0;
    ECanaMboxes.MBOX5.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX5.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX5.MSGID.bit.RES2 = 1;

	ECanaMboxes.MBOX0.MSGCTRL.all = 0x00000008;//data length = 8
    ECanaMboxes.MBOX1.MSGCTRL.all = 0x00000008;
    ECanaMboxes.MBOX2.MSGCTRL.all = 0x00000008;
    ECanaMboxes.MBOX3.MSGCTRL.all = 0x00000008;
	ECanaMboxes.MBOX4.MSGCTRL.all = 0x00000008;
	ECanaMboxes.MBOX5.MSGCTRL.all = 0x00000008;

	ECanaLAMRegs.LAM0.all = 0x800FFFFC;
    ECanaLAMRegs.LAM1.all = 0x800FFFFC;
    ECanaLAMRegs.LAM2.all = 0x80FFFFFC;
    //ECanaLAMRegs.LAM3.all = 0x800FFFFC;
	//ECanaLAMRegs.LAM4.all = 0x800FFFFC;
	//ECanaLAMRegs.LAM5.all = 0x800FFFFC;

	ECanaRegs.CANMD.all = 0x00000007;//mail0~2 R  others T, temp

    EALLOW;
    ECanaShadow.CANME.all = ECanaRegs.CANME.all;
    ECanaShadow.CANME.all = 0x0000003F;
    ECanaRegs.CANME.all = ECanaShadow.CANME.all;
    //ECanaRegs.CANME.all = 0x0000003F;// Required before writing the MSGIDs
    EDIS;


}
/*******************************************************************************
*Function name:	InitECanaGpio()
*Description:   This function initializes GPIO pins to function as eCAN pins                            
*******************************************************************************/
// Each GPIO pin can be configured as a GPIO pin or up to 3 different
// peripheral functional pins. By default all pins come up as GPIO
// inputs after reset.  
// 
// Caution: 
// Only one GPIO pin should be enabled for CANTXA/B operation.
// Only one GPIO pin shoudl be enabled for CANRXA/B operation. 
// Comment out other unwanted lines.
void InitECanaGpio(void)
{
   EALLOW;

	// Enable internal pull-up for the selected CAN pins 
	// Pull-ups can be enabled or disabled by the user. 
	// This will enable the pullups for the specified pins.
	// Comment out other unwanted lines.
	GpioCtrlRegs.GPAPUD.bit.GPIO30 = 0;	    // Enable pull-up for GPIO30 (CANRXA)
	GpioCtrlRegs.GPAPUD.bit.GPIO31 = 0;	    // Enable pull-up for GPIO31 (CANTXA)

	// Set qualification for selected CAN pins to asynch only
	// Inputs are synchronized to SYSCLKOUT by default.  
	// This will select asynch (no qualification) for the selected pins.
    GpioCtrlRegs.GPAQSEL2.bit.GPIO30 = 3;   // Asynch qual for GPIO30 (CANRXA)   

	// Configure eCAN-A pins using GPIO regs
	// This specifies which of the possible GPIO pins will be eCAN functional pins.
	GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 1;	// Configure GPIO30 for CANTXA operation
	GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 1;	// Configure GPIO31 for CANRXA operation

    EDIS;
}

/******************************************************************************/
/* Bit configuration parameters for 100 MHz SYSCLKOUT*/ 
/******************************************************************************/
/*
The table below shows how BRP field must be changed to achieve different bit
rates with a BT of 10, for a 80% SP:
--------------------------------------------------------------------------------
BT = 10, TSEG1 = 6, TSEG2 = 1, Sampling Point = 80% 
--------------------------------------------------------------------------------
1   Mbps : BRP+1 = 10 	: CAN clock = 10 MHz
500 kbps : BRP+1 = 20 	: CAN clock = 5 MHz 
250 kbps : BRP+1 = 40 	: CAN clock = 2.5 MHz 
125 kbps : BRP+1 = 80 	: CAN clock = 1.25 MHz 
100 kbps : BRP+1 = 100 	: CAN clock = 1 MHz
50  kbps : BRP+1 = 200 	: CAN clock = 0.5 MHz

The table below shows how to achieve different sampling points with a BT of 25:
--------------------------------------------------------------------------------
Achieving desired SP by changing TSEG1 & TSEG2 with BT = 25  
--------------------------------------------------------------------------------

TSEG1 = 18, TSEG2 = 4, SP = 80% 
TSEG1 = 17, TSEG2 = 5, SP = 76% 
TSEG1 = 16, TSEG2 = 6, SP = 72% 
TSEG1 = 15, TSEG2 = 7, SP = 68% 
TSEG1 = 14, TSEG2 = 8, SP = 64% 

The table below shows how BRP field must be changed to achieve different bit
rates with a BT of 25, for the sampling points shown above: 

1   Mbps : BRP+1 = 4 
500 kbps : BRP+1 = 8 
250 kbps : BRP+1 = 16 
125 kbps : BRP+1 = 32 
100 kbps : BRP+1 = 40
50  kbps : BRP+1 = 80

*/
void hvdc_can_init(void)
{
    ECanaRegs.CANME.all = ECanaRegs.CANME.all & 0xFFFFFFEC;
    asm (" NOP ");

    ECanaMboxes.MBOX0.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX0.MSGID.bit.PROTNO = 0x075;
    ECanaMboxes.MBOX0.MSGID.bit.PTP = 0;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRH = 0x07;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRL = 0x1f;
    ECanaMboxes.MBOX0.MSGID.bit.SRCADDR = 0x00;
    ECanaMboxes.MBOX0.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX0.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX0.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX1.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX1.MSGID.bit.PROTNO = 0x069;
    ECanaMboxes.MBOX1.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRH = 0;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRL = 0x01;
    ECanaMboxes.MBOX1.MSGID.bit.SRCADDR = 0x31;
    ECanaMboxes.MBOX1.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX1.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX1.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX4.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX4.MSGID.bit.PROTNO = 0x069;
    ECanaMboxes.MBOX4.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRH = g_u8MdlAddresss/32;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRL = g_u8MdlAddresss%32;
    ECanaMboxes.MBOX4.MSGID.bit.SRCADDR = 0x0F0;
    ECanaMboxes.MBOX4.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX4.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX4.MSGID.bit.RES2 = 1;

    //ECanaRegs.CANMD.all = 0x00000007;//mail0~2 R  others T, temp

    ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x00000013;       // Required before writing the MSGIDs

}
void Converter_can_init(void)
{
    ECanaRegs.CANME.all = ECanaRegs.CANME.all & 0xFFFFFFEC;
    asm (" NOP ");

    ECanaMboxes.MBOX0.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX0.MSGID.bit.PROTNO = 0x071;
    ECanaMboxes.MBOX0.MSGID.bit.PTP = 0;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRH = 0x07;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRL = 0x1f;
    ECanaMboxes.MBOX0.MSGID.bit.SRCADDR = 0x00;
    ECanaMboxes.MBOX0.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX0.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX0.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX1.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX1.MSGID.bit.PROTNO = 0x064;
    ECanaMboxes.MBOX1.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRH = 0;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRL = 0x01;
    ECanaMboxes.MBOX1.MSGID.bit.SRCADDR = 0x31;
    ECanaMboxes.MBOX1.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX1.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX1.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX4.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX4.MSGID.bit.PROTNO = 0x064;
    ECanaMboxes.MBOX4.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRH = g_u8MdlAddresss/32;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRL = g_u8MdlAddresss%32;
    ECanaMboxes.MBOX4.MSGID.bit.SRCADDR = 0x0F0;
    ECanaMboxes.MBOX4.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX4.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX4.MSGID.bit.RES2 = 1;

    //ECanaRegs.CANMD.all = 0x00000007;//mail0~2 R  others T, temp

    ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x00000013;       // Required before writing the MSGIDs

}

void ISrect_can_init(void)
{
    ECanaRegs.CANME.all = ECanaRegs.CANME.all & 0xFFFFFFEC;
    asm (" NOP ");

    ECanaMboxes.MBOX0.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX0.MSGID.bit.PROTNO = 0x081;
    ECanaMboxes.MBOX0.MSGID.bit.PTP = 0;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRH = 0x07;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRL = 0x1f;
    ECanaMboxes.MBOX0.MSGID.bit.SRCADDR = 0x00;
    ECanaMboxes.MBOX0.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX0.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX0.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX1.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX1.MSGID.bit.PROTNO = 0x080;
    ECanaMboxes.MBOX1.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRH = 0;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRL = 0x01;
    ECanaMboxes.MBOX1.MSGID.bit.SRCADDR = 0x31;
    ECanaMboxes.MBOX1.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX1.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX1.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX4.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX4.MSGID.bit.PROTNO = 0x080;
    ECanaMboxes.MBOX4.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRH = g_u8MdlAddresss/32;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRL = g_u8MdlAddresss%32;
    ECanaMboxes.MBOX4.MSGID.bit.SRCADDR = 0x0F0;
    ECanaMboxes.MBOX4.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX4.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX4.MSGID.bit.RES2 = 1;

    //ECanaRegs.CANMD.all = 0x00000007;//mail0~2 R  others T, temp

    ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x00000013;       // Required before writing the MSGIDs

}

// add for MPPT module 0x67 and 0x72
void MPPT_can_init(void)
{
    ECanaRegs.CANME.all = ECanaRegs.CANME.all & 0xFFFFFFEC;
    asm (" NOP ");

    ECanaMboxes.MBOX0.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX0.MSGID.bit.PROTNO = 0x072;
    ECanaMboxes.MBOX0.MSGID.bit.PTP = 0;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRH = 0x07;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRL = 0x1f;
    ECanaMboxes.MBOX0.MSGID.bit.SRCADDR = 0x00;
    ECanaMboxes.MBOX0.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX0.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX0.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX1.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX1.MSGID.bit.PROTNO = 0x067;
    ECanaMboxes.MBOX1.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRH = 0;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRL = 0x01;
    ECanaMboxes.MBOX1.MSGID.bit.SRCADDR = 0x31;
    ECanaMboxes.MBOX1.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX1.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX1.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX4.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX4.MSGID.bit.PROTNO = 0x067;
    ECanaMboxes.MBOX4.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRH = g_u8MdlAddresss/32;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRL = g_u8MdlAddresss%32;
    ECanaMboxes.MBOX4.MSGID.bit.SRCADDR = 0x0F0;
    ECanaMboxes.MBOX4.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX4.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX4.MSGID.bit.RES2 = 1;

    //ECanaRegs.CANMD.all = 0x00000007;//mail0~2 R  others T, temp

    ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x00000013;       // Required before writing the MSGIDs

}
void EPA_can_init(void)
{
    ECanaRegs.CANME.all = ECanaRegs.CANME.all & 0xFFFFFFEC;
    asm (" NOP ");

    ECanaMboxes.MBOX0.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX0.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX0.MSGID.bit.PROTNO = 0x07C;
    ECanaMboxes.MBOX0.MSGID.bit.PTP = 0;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRH = 0x07;
    ECanaMboxes.MBOX0.MSGID.bit.DSTADDRL = 0x1f;
    ECanaMboxes.MBOX0.MSGID.bit.SRCADDR = 0x00;
    ECanaMboxes.MBOX0.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX0.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX0.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX1.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX1.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX1.MSGID.bit.PROTNO = 0x06C;
    ECanaMboxes.MBOX1.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRH = 0;
    ECanaMboxes.MBOX1.MSGID.bit.DSTADDRL = 0x01;
    ECanaMboxes.MBOX1.MSGID.bit.SRCADDR = 0x31;
    ECanaMboxes.MBOX1.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX1.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX1.MSGID.bit.RES2 = 1;

    ECanaMboxes.MBOX4.MSGID.bit.IDE = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AME = 1;
    ECanaMboxes.MBOX4.MSGID.bit.AAM = 0;
    ECanaMboxes.MBOX4.MSGID.bit.PROTNO = 0x06C;
    ECanaMboxes.MBOX4.MSGID.bit.PTP = 1;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRH = g_u8MdlAddresss/32;
    ECanaMboxes.MBOX4.MSGID.bit.DSTADDRL = g_u8MdlAddresss%32;
    ECanaMboxes.MBOX4.MSGID.bit.SRCADDR = 0x0F0;
    ECanaMboxes.MBOX4.MSGID.bit.CNT = 0;
    ECanaMboxes.MBOX4.MSGID.bit.RES1 = 1;
    ECanaMboxes.MBOX4.MSGID.bit.RES2 = 1;

    ECanaRegs.CANME.all = ECanaRegs.CANME.all | 0x00000013;       // Required before writing the MSGIDs

}
void efuse_id_init(void)
{
;
}
//===========================================================================
// End of file.
//===========================================================================

