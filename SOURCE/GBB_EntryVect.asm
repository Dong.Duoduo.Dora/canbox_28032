;	The second level boot loader and application depend on these
;	constants being exactly where they are.  Their addresses are
;  hard coded in the linkder command files because the DSECT
;	liker directive does not prevent overlaying code on the
;  specified section but instead puts code wherever it wants.

;	THE NUMBER AND LOCATION OF THESE CONSTANTS CANNOT BE CHANGED
;  WITHOUT ALSO CHANGING THE SECOND LEVEL BOOT AND APPLICATION!


	.global _Application_Valid


	
	.global _Boot_Version;_Hardware_Version
	
	.global _SendBlockRequest;,	_SendBlockRequestPointer	
	.global _GetHeader;,	_GetHeaderPointer 			
	.global _GetDataBlock;,	_GetDataBlockPointer 		
	.global _StartTimer;,	_StartTimerPointer 			
	.global _TimeIsUp;,	_TimeIsUpPointer 
	.global _ProcessInquiry
	.global	_B1RstEntryPointer	,	_B1_Rst_Entry
	.sect	".bootvec"
_Boot_Version   .int   0x0100         ; 0x3F7000
		.int   0x0100                 ; 0x3F7001


_B1RstEntryPointer			LB		    _B1_Rst_Entry ;0x3F7008


;********************************************************************
	
	.ref _c_int00
	.global  _BOOT_MAIN
	.global  _uiFromApplication,_g_u16MdlAddr
__stack:	.usect	".stack",0
	
	.sect	"FlashBoot"
_B1_Rst_Entry:
	;MOV     @SP,#0x0400;
	MOV 	AL,#__stack
	MOV     @SP,AL
	SPM     0
	SETC    OBJMODE
	CLRC    AMODE
	.c28_amode
	SETC    M0M1MAP
	CLRC    PAGE0    ;28x mode
	MOVW    DP,#0x0000
	CLRC    OVM
	ASP
	
	SETC	  INTM
	LCR 	_c_int00


_Software_Version	.int  0x0100
		 			.int   0x0100
_Baseline_Version	.int  205

;***********************************************************************
	.global _CRC16_CheckWord
	.sect	"appcrc"			
_CRC16_CheckWord:
	.int	0xCEA6
;***********************************************************************
																				
	.end
;===========================================================================
; No more.
;===========================================================================
