;//###########################################################################
;//
;// FILE:	DSP280x_CSMPasswords.asm
;//
;// TITLE:	DSP280x Code Security Module Passwords.
;// 
;// DESCRIPTION:
;//
;//         This file is used to specify password values to
;//         program into the CSM password locations in Flash
;//         at 0x3F7FF8 - 0x3F7FFF.
;//
;//         In addition, the reserved locations 0x3F7F80 - 0X3f7ff5 are 
;//         all programmed to 0x0000
;//
;//###########################################################################
;// Running on TMS320LF280xA   PFC part�   h741Au1(1.00)               
;// External clock is 20MHz, PLL * 10/2 , CPU-Clock 100 MHz	      
;// Date: from May 23, 2005 to Oct 30, 2006  , (C) www & mhp & lsy & lyg
;// Version:1.00     Change Date: May 24, 2005 , 		
;//###########################################################################

; The "csmpasswords" section contains the actual CSM passwords that will be
; linked and programmed into to the CSM password locations (PWL) in flash.  
; These passwords must be known in order to unlock the CSM module. 
; All 0xFFFF's (erased) is the default value for the password locations (PWL).

; It is recommended that all passwords be left as 0xFFFF during code
; development.  Passwords of 0xFFFF do not activate code security and dummy 
; reads of the CSM PWL registers is all that is required to unlock the CSM.  
; When code development is complete, modify the passwords to activate the
; code security module.

          .global _PRG_key0
          .global _PRG_key1
          .global _PRG_key2
          .global _PRG_key3
          .global _PRG_key4
          .global _PRG_key5
          .global _PRG_key6
          .global _PRG_key7
      
      
      .sect "csmpasswds" ;ZLX/20080307

_PRG_key0	.int 0xFFFF  ; PSWD bits 15-0
_PRG_key1	.int 0xFFFF  ; PSWD bits 31-16
_PRG_key2	.int 0xFFFF  ; PSWD bits 47-32
_PRG_key3	.int 0xFFFF ; PSWD bits 63-48
_PRG_key4	.int 0xFFFF  ; PSWD bits 79-64
_PRG_key5	.int 0xFFFF  ; PSWD bits 95-80
_PRG_key6	.int 0xFFFF  ; PSWD bits 111-96
_PRG_key7	.int 0xFFFF  ; PSWD bits 127-112

;_PRG_key0	.int 0x4132  ; PSWD bits 15-0
;_PRG_key1	.int 0x0231  ; PSWD bits 31-16
;_PRG_key2	.int 0x4156  ; PSWD bits 47-32
;_PRG_key3	.int 0x5120  ; PSWD bits 63-48
;_PRG_key4	.int 0xF890  ; PSWD bits 79-64
;_PRG_key5	.int 0x7536  ; PSWD bits 95-80
;_PRG_key6	.int 0xF128  ; PSWD bits 111-96
;_PRG_key7	.int 0xF426  ; PSWD bits 127-112
	
;----------------------------------------------------------------------

; For code security operation, all addresses between 0x3F7F80 and
; 0X3f7ff5 cannot be used as program code or data.  These locations
; must be programmed to 0x0000 when the code security password locations
; (PWL) are programmed.  If security is not a concern, then these addresses
; can be used for code or data.  

; The section "csm_rsvd" can be used to program these locations to 0x0000.

        .sect "csm_rsvd"
        .loop (3F7FF5h - 3F7F80h + 1)
              .int 0x0000
        .endloop

;//===========================================================================
;// End of file.
;//===========================================================================

      
