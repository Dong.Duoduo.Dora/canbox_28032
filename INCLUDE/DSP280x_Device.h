// TI File $Revision: /main/2 $
// Checkin $Date: January 25, 2006   14:03:29 $
//###########################################################################
//
// FILE:   DSP280x_Device.h
//
// TITLE:  DSP280x Device Definitions.
//
//###########################################################################
// $TI Release: DSP280x V1.30 $
// $Release Date: February 10, 2006 $
//###########################################################################

#ifndef DSP280x_DEVICE_H
#define DSP280x_DEVICE_H

#ifdef __cplusplus
extern "C" {
#endif

#define   TARGET   1
//---------------------------------------------------------------------------
// Common CPU Definitions:

extern cregister volatile unsigned int IFR;
extern cregister volatile unsigned int IER;

#define  EINT   asm(" clrc INTM")
#define  DINT   asm(" setc INTM")
#define  ERTM   asm(" clrc DBGM")
#define  DRTM   asm(" setc DBGM")
#define  EALLOW asm(" EALLOW")
#define  EDIS   asm(" EDIS")
#define  ESTOP0 asm(" ESTOP0")

#define M_INT1  0x0001
#define M_INT2  0x0002
#define M_INT3  0x0004
#define M_INT4  0x0008
#define M_INT5  0x0010
#define M_INT6  0x0020
#define M_INT7  0x0040
#define M_INT8  0x0080
#define M_INT9  0x0100
#define M_INT10 0x0200
#define M_INT11 0x0400
#define M_INT12 0x0800
#define M_INT13 0x1000
#define M_INT14 0x2000
#define M_DLOG  0x4000
#define M_RTOS  0x8000

#define true    1
#define false   0

//---------------------------------------------------------------------------
// For Portability, User Is Recommended To Use Following Data Type Size
// Definitions For 16-bit and 32-Bit Signed/Unsigned Integers:
//

#ifndef DSP28_DATA_TYPES
#define DSP28_DATA_TYPES
typedef int             int16;
typedef long            int32;
typedef unsigned int    Uint16;
typedef unsigned long   Uint32;
typedef float           float32;
typedef long double     float64;

typedef unsigned char   UINT8;
typedef int             INT16;
typedef long            INT32;
typedef unsigned int    UINT16;
typedef unsigned long   UINT32;
typedef float           FLOAT32;
typedef long double     FLOAT64;
#endif

#ifndef GLOBAL_Q
#define GLOBAL_Q 10 /* Q1 to Q29 */
#endif

struct MDLCTRLEXTEND_BITS{		//bits description
	Uint16	ACLIMMODE:1;	//0 AC lim mode0			1:mode1		0:mode0
	Uint16	DCINPUTALARM:1;	//1 DC input alarm			1:no alarm	0:alarm
	Uint16	rsd:14;			//15~2
};
typedef union{
	Uint16						all;
	struct 	MDLCTRLEXTEND_BITS bit;
}ubitintaextend;

struct MDLCTRL_BITS {		// bits  description
    Uint16  WALKIN:1;		// 0  set Walkin 			1:enable	0:disable
    Uint16  FANFULL:1;		// 1  set fan full speed	1:full		0:adjust
    Uint16  ALLOWACOV:1;	// 2  ACOV allow start		1:enable	0:disable
	Uint16  ONCELOCK:1;		// 3  DCOV times for lock	1:once		0:twice
	Uint16  PARAFLAG:1;		// 4  Rectifier single or more 1: more  0:single
	Uint16  EStopFlag:1;	// 5  Emergent stop Rect    1:off       0:on
	Uint16  NoDeratingFlag:1; // 6  Derating or not       1:not       0:derating
    Uint16  OFFCTRL:1;		// 7  set rectifier on/off  1:off  		0:on
    Uint16  IDENTIFY:1;		// 8  set rect identify 	1:identify	0:no identify
    Uint16  ACLIMIT:1;		// 9  AC power limit		1:limit		0:normal
    Uint16  THERMLIMIT:1;	// 10  Therm power limit		1:limit	0:normal
    Uint16  SHORTRELAY:1;	// 11  
	Uint16  PFCCOMPENSATE:1;//12   CURR dispaly compensate by pfc volt 0:no
    Uint16	HWONCELOCK:1;	//13
    Uint16  FUSEB:1;		// 14 fuse broken	1:broken	0:normal
	Uint16  PARAFLAGMASTER:1; //15
};

typedef	union {
   Uint16                all;
   struct MDLCTRL_BITS   bit;
}ubitinta;



struct MDLSTAT_BITS {         	// bits  description
    Uint16  OFFSTAT:1;			// 0	on/off state		1:off	0:on
    Uint16  DCOV:1;				// 1	dc over voltage		1:OV	0:normal
    Uint16  AMBIOT:1;			// 2	ambi over temp		1:OT	0:normal
    Uint16  RECTOT:1;			// 3	rect over temp		1:OT	0:normal
    Uint16  FANFAIL:1;			// 4	fan fail			1:fail	0:normal
    Uint16  ACUV:1;				// 5	AC under voltage	1:UV	0:normal
    Uint16  ACOV:1;				// 6	AC over voltage		1:OV	0:normal
    Uint16  PFCUV:1;			// 7	PFC under voltage	1:UV	0:normal
    Uint16  PFCOV:1;			// 8	PFC over voltage	1:OV	0:normal
    Uint16  CALIBRFAIL:1;		// 9	calibrat data fail	1:fail	0:normal
    Uint16  NOCONTROLLER:1;		// 10	can't com with controller	1:fail	0:normal
    Uint16  IUNBALMAJOR:1;		// 11	load seriously unbalance	1:fail	0:normal
    Uint16  IUNBALMINOR:1;		// 12	load slightly unbalance		1:fail	0:normal
    Uint16  SHORT:1;			// 13	output short        1:fail	0:normal
    Uint16  IDERR:1;			// 14	ID error            1:fail	0:normal
    Uint16  HVSDLOCK:1;			// 15	hvsd lock			1:lock	0:normal
};

typedef	union {
	Uint16               all;
	struct MDLSTAT_BITS   bit;
}ubitintb;

struct MDLSTATEXTEND_BITS {         // bits  description
   		Uint16  PFCOT:1;			// 0	PFC over temp		1:OT	0:normal
    	Uint16  ACCOMHALT:1;		// 1	 AC>80V,COM fail    1:fail	0:normal 
		Uint16  HWHVSD:1;			// 2	HWHVSD flag			1:fail	0:normal 
		Uint16  DCFAULT:1;			// 3 DC Fault Flag  		1: DC Fault 0: Normal
    	Uint16  DCFUSEBROKEN:1;		// 4 DC Fust broken Flag 	1: broken 0: Normal
       	Uint16  PFCFAIL:1;			// 5	PFC Fail			1:Fail 	0:normal
    	Uint16  DIAGNOSISTEST:1;   //6//begin test SCU set volt or not:set:1// ELSE :0
    	Uint16  DIAGNOSISBEGIN:1;  //7//SCU set volt start self diagnosis��1;ELSE:0
    	Uint16  DCINPUTALARM:1;		// 8 DC input alarm  		1: DC input 0: Normal
		Uint16  DCINPUTSTATUS:1;		// 9 DC input status  		1: DC input 0: Normal
    	Uint16  ACDYN:1;			// 10 
    	Uint16  DSPOT:1;			// 11
		Uint16  DCOCP:1;			// 12
    	Uint16  rsd:3;				// 15~13	
};

typedef	union {
	Uint16               all;
	struct MDLSTATEXTEND_BITS   bit;
}ubitintbextend;

struct RUNFLAG_BITS {         // bits  description
    Uint16  CAL:1;				// 0	circuit calculation		1:set	0:clear
    Uint16  WARN:1;				// 1	warn and control		1:set	0:clear
    Uint16  LOADSHARE:1;		// 2	load sharing flag		1:set	0:clear
    Uint16  SHARESYN:1;			// 3	load share syn send		1:set	0:clear
    Uint16  SHAREDATA:1;		// 4	load share data send	1:set	0:clearl
    Uint16  ADDRCAL:1;			// 5	address calculation		1:set	0:clear
    Uint16  ADC:1;				// 6	ADC filter calculation	1:set	0:clear
    Uint16  LIGHT:1;			// 7	light blink flag		1:set	0:clear
    Uint16  PFCUVCHECKTIMER:1;	// 8	PFC UV Check flag		1:set	0:clear
    Uint16  DCFAULTCHECKTIMER:1;// 9	DC FAULT Check flag		1:set	0:clear
    Uint16  FUSEFAULBACK:1;     //10
	Uint16  QUICKLIGHT:1;		//11

    Uint16  RECORDTIME:1;		// 12 	record time every 1s 			1:set	0:clear
    Uint16  TIMEEEPROM:1;		// 13 	record time to eeprom flag every 30min	1:set	0:clear

    Uint16  rsd:2;				// 15:14	
};

typedef	union {
   Uint16               all;
   struct RUNFLAG_BITS   bit;
}ubitintc;

struct EPROM_BITS {         // bits  description
    Uint16  RUNTIME:1;		// 0	module run time
    Uint16  MDLCTRL:1;		// 1	module control flag
    Uint16  INITCAN:1;		// 2	initial can flag
    Uint16  VOLTUP:1;		// 3	module max dc voltage
    Uint16  VDCDFT:1;		// 4	module default dc voltage
    Uint16  TEMPUP:1;		// 5	dc max temp flag 
    Uint16  REONTIME:1;		// 6	module reon time
    Uint16  WALKTIME:1;		// 7	module walk-in time
    Uint16  ORDERTIME:1;	// 8	module order on time 
    Uint16  POWERDFT:1;		// 9	module default power
    Uint16  CURRRDFT:1;		// 10	module default current
    Uint16  ACCURRRDFT:1;	// 11	module default AC current
	Uint16  MDLCTRLEXTEND:1;	// 12	module AC limit power mode
	Uint16	EEPROMROINTER:1;	//13  eeprom pointer
	Uint16  WATTACCUMULATE:1;   // 14	module input power accumulate
    Uint16  WATTACCUMULATETIME:1;// 15	module input power accumulate time
};

typedef	union {
   Uint16               all;
   struct EPROM_BITS   bit;
}ubitintd;

struct EPROMEXTEND_BITS {         // bits  description
    Uint16  RECORDTIMESELF:1;	// 0	module run time by oneself 
	Uint16	rsd:15;			    // 15:1
};


typedef	union {
   Uint16               all;
   struct EPROMEXTEND_BITS   bit;
}ubitintdextend;




//20111130
struct SELFDXVALUE_BITS {		// bits  description
	Uint16  ADDRESS:8;		// address
	Uint16  ATTR:8;			// attribute
};

typedef	union {
   Uint16               all;
   struct SELFDXVALUE_BITS   bit;
}ubitintg;

struct SELFDXCTRL_BITS {	// bits  description
	Uint16  INGADDR:7;		// address(which is diagnosing) 0~100
	Uint16  MASTERMDL:1;	// 1: master module, 0: not
	Uint16  START:1;		// 1: start self diagnosis, 0: disable
	Uint16  TXEN:1;			// 1: send can data enable, 0: disable
	Uint16  NEXTDELAY:3;	// time for master mdl not receive data
	Uint16  STATE:3;		// self diagnosis state
};

typedef	union {
   Uint16               all;
   struct SELFDXCTRL_BITS   bit;
}ubitinth;

struct SELFDXTIMER_BITS {		// bits  description
	Uint16  SLAVETIMER:6;	// slave mdl timer
	Uint16  CHKDELAY:10;	// delay time for judge whether is fuse broken or not
};

typedef	union {
   Uint16               all;
   struct SELFDXTIMER_BITS   bit;
}ubitinti;

struct intData
{
	int iLD;
	int iHD;
};
typedef struct intData intStructData;

typedef	union 
{
	int32 lData;
	intStructData iData;
}longunion;


typedef	union 
{
	unsigned int id ;
	struct   packed_data
	{
		 unsigned highchar : 8;
		 unsigned lowchar  : 8;
	}bitdata;                                                                                                    
}ubitint ;
/*
typedef union
{
    unsigned int id1 ;
    struct   packed_data1
    {
         unsigned highchar1 : 8;
         unsigned lowchar1  : 8;
    }bitdata1;
}ubitint1 ;
*/
typedef	union 
{
	float fd ;
	ubitint intdata[2];   
	long	lData;	//changed by lvyg
	intStructData iData; //changed by lvyg
}ubitfloat ;

typedef union
{
    unsigned int msgid[2];
    struct
    {
        unsigned RES2       :1;     /* reserved */
        unsigned RES1       :1;     /* reserved */
        unsigned CNT        :1;     /* 1:continuous frame */
        unsigned SrcAddr    :8;     /* source address */
        unsigned DstAddrL   :5;     /* destination/group address low bits*/
        unsigned DstAddrH   :3;     /* destination/group address high bits*/
        unsigned PTP        :1;     /* 0:broadcast, 1:point to point */
        unsigned ProtNo     :9;     /* protocol number */
        unsigned AAM        :1;     /* auto answer bit */
        unsigned AME        :1;     /* acceptance mask enable */
        unsigned IDE        :1;     /* identifier extension */
    }msgbit;
}frameid;

typedef union
{
	unsigned int msgaddid[3];
	struct
	{
		unsigned ADDIDL	:16;		/* reserved */
		unsigned ADDIDH	:16;		/* reserved */
		unsigned ADDADD	:8;		/* com attribute */
		unsigned ADDOK	:8;		/* 1:continuous frame */
	}msgaddbit;
}frameaddid;

//20160902 wzc 
typedef struct
{
	Uint16 		uiAddress;
	Uint16		uiAlarm;
	ubitfloat  	ubitfData;//32 bits
}EepromData;




/*----------for boot---------------*/

typedef struct 
{
	Uint16	*uiLoadAddress;
	Uint16	uiLength;
} APPLICATIONHEADER; 

//----------------------------------------------------------------------
#define	TIME_200mS					10000  //20us time base  ls 20111128
#define	TIME_1000mS					50000
#define SIZE_OF_BOOTLOADERHEADER		4
#define FIRSTTRIES				255	// We try 255 times on the first header
#define	TRIES						5		// We try five times on subsequent data
#define	BLOCKACKNOWLEDGE			0  	// Block Acknowledge
#define	MISSINGDATAPACKET			1  	// Missing Data Packet in Block
#define	LASTBLOCKACKNOWLEDGE		2  	// Last Block Acknowledge
#define	INVALIDAPPLICATION		3  	//	Invalid Application Image
#define	DEVICEMISPROGRAMMED		4  	//	Device Misprogrammed

#define	FLASHROWSIZE				128	// 128 words = 256 bytes, the maximum size
#define	RAMBLOCKSIZE				128


#define DSP28_DIVSEL                  2	
#define MSG_SIZE_8		0x08		/* 	the address arbitration message has 6 bytes of data */ 

//lzy
// The following pointer to a function call calibrates the ADC and internal oscillators
#define Device_cal (void   (*)(void))0x3D7C80
extern volatile ubitfloat   ulENP_Lo_SN;
extern volatile ubitfloat   ulENP_Hi_SN;
extern volatile Uint16	u16WaitLoopCount;

extern Uint16 BootRamfuncsLoadStart;
extern Uint16 BootRamfuncsLoadEnd;
extern Uint16 BootRamfuncsRunStart;

extern Uint16 IsrRamfuncsLoadStart;
extern Uint16 IsrRamfuncsLoadEnd;
extern Uint16 IsrRamfuncsRunStart;

extern volatile Uint16 uiFromApplication;
extern volatile	Uint16 g_u16MdlAddr; 
/*-----------------------------------------------------------------*/

// shift from DSP280x_Examples.h
/*-----------------------------------------------------------------------------
      Specify the clock rate of the CPU (SYSCLKOUT) in nS.

      Take into account the input clock frequency and the PLL multiplier
      selected in step 1.
 
      Use one of the values provided, or define your own.
      The trailing L is required tells the compiler to treat 
      the number as a 64-bit value.  

      Only one statement should be uncommented.

      Example:  CLKIN is a 20MHz crystal. 
 
                In step 1 the user specified PLLCR = 0xA for a 
                100Mhz CPU clock (SYSCLKOUT = 100MHz).  

                In this case, the CPU_RATE will be 10.000L
                Uncomment the line:  #define CPU_RATE   10.000L
-----------------------------------------------------------------------------*/
//#define CPU_RATE   16.670L   // for a 100MHz CPU clock speed (SYSCLKOUT)

//----------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Include Example Header Files:
//

#include "DSP280x_GlobalPrototypes.h"         // Prototypes for global functions within the .c files.
#include "DSP280x_ePwm_defines.h"             // Macros used for PWM examples. 
#include "DSP280x_I2C_defines.h"              // Macros used for I2C examples.

// Include files not used with DSP/BIOS
#ifndef DSP28_BIOS
#include "DSP280x_DefaultISR.h"
#endif


// DO NOT MODIFY THIS LINE.  
#define DELAY_US(A)  DSP28x_usDelay(((((long double) A * 1000.0L) / (long double)CPU_RATE) - 9.0L) / 5.0L)


extern void Load_Code_And_Execute(void);

extern void InitPeripherals(void);

extern void Watch_Dog_Kick(void);
extern void InitRAM(void);

extern void CRC16_CheckWord(void);
extern void Boot_Version(void);
extern void Software_Version(void);
extern void Hardware_Version(void);
extern void Baseline_Version(void);
extern void InitSci(void);
extern void scia_enableTX_disableRX(void);
extern void scia_enableRX_disableTX(void);
extern Uint16 uiHexToAsc(UINT8 hex);//lzy 20120312
extern UINT8 ucAscToHex(UINT8 ascl, UINT8 asch);
extern interrupt void vCanIsr(void);
extern interrupt void cpu_timer0_isr(void);
extern unsigned char get_can_data(unsigned char start_cid,unsigned char len);
void com_can(UINT16 value_type);
//---------------------------------------------------------------------------
// Include All Peripheral Header Files:
//

#include "DSP280x_Adc.h"                // ADC Registers
#include "DSP280x_DevEmu.h"             // Device Emulation Registers
#include "DSP280x_CpuTimers.h"          // 32-bit CPU Timers
#include "DSP280x_ECan.h"               // Enhanced eCAN Registers
#include "DSP280x_ECap.h"               // Enhanced Capture
#include "DSP280x_EPwm.h"               // Enhanced PWM 
#include "DSP280x_EQep.h"               // Enhanced QEP
#include "DSP280x_Gpio.h"               // General Purpose I/O Registers
#include "DSP280x_I2c.h"                // I2C Registers
#include "DSP280x_PieCtrl.h"            // PIE Control Registers
#include "DSP280x_PieVect.h"            // PIE Vector Table
#include "DSP280x_Spi.h"                // SPI Registers
#include "DSP280x_Sci.h"                // SCI Registers
#include "DSP280x_SysCtrl.h"            // System Control/Power Modes
#include "DSP280x_XIntrupt.h"           // External Interrupts
#include "GBB_constant.h"
#include "SFO_V6.h"                     //lzy  for 2803x SFO library headerfile
#include "IQmathLib.h"
#include "H7413DU111_main.h"
#include "H7413DU111_macro.h"
#include "H7413DU111_constant.h"
#include "CanDriver.h"
#include "DSP2803x_Comp.h"

#include "Flash2803x_API.h"


#ifdef __cplusplus
}
#endif /* extern "C" */

#endif  // end of DSP280x_DEVICE_H definition


//===========================================================================
// End of file.
//===========================================================================
