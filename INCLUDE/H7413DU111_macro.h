 /*=============================================================================*
 *         Copyright(c) 2007-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : GBB Rectifiers
 *
 *  FILENAME : GBB_macro.h
 *  PURPOSE  : Define the io assignment the macro from the hardware of dsp
 *				     
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *  2008-6-10         OOA            Agnes           Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    
 *============================================================================*/


//Slope of temperature sensor (deg. C / ADC code, fixed pt Q15 format)
#define getTempSlope() (*(int (*)(void))0x3D7E82)()
//ADC code corresponding to temperature sensor output at 0-degreesC
#define getTempOffset() (*(int (*)(void))0x3D7E85)()

#define mRedLedOn()		  (GpioDataRegs.GPASET.bit.GPIO28 = 1)
#define mRedLedOff()	  (GpioDataRegs.GPACLEAR.bit.GPIO28 = 1)
#define mRedLedTwink()	  (GpioDataRegs.GPATOGGLE.bit.GPIO28 = 1)


#define mGreenLedOn()	  (GpioDataRegs.GPASET.bit.GPIO18 = 1)
#define mGreenLedOff()	  (GpioDataRegs.GPACLEAR.bit.GPIO18 = 1)
#define mGreenLedTwink()  (GpioDataRegs.GPATOGGLE.bit.GPIO18 = 1)


#define mYellowLedOn()	  (GpioDataRegs.GPASET.bit.GPIO29 = 1)
#define mYellowLedOff()	  (GpioDataRegs.GPACLEAR.bit.GPIO29 = 1)
#define mYellowLedTwink() (GpioDataRegs.GPATOGGLE.bit.GPIO29 = 1)


#define mGreenLed1On()     {GpioDataRegs.GPASET.bit.GPIO3 = 1; GpioDataRegs.GPACLEAR.bit.GPIO4 = 1;}
#define mGreenLed1Off()    {GpioDataRegs.GPACLEAR.bit.GPIO3 = 1; GpioDataRegs.GPACLEAR.bit.GPIO4 = 1;}
#define mGreenLed1Twink()  {GpioDataRegs.GPATOGGLE.bit.GPIO3 = 1; GpioDataRegs.GPACLEAR.bit.GPIO4 = 1;}

#define mRedLed1On()       {GpioDataRegs.GPACLEAR.bit.GPIO3 = 0; GpioDataRegs.GPASET.bit.GPIO4 = 1;}
#define mRedLed1Off()      {GpioDataRegs.GPACLEAR.bit.GPIO3 = 0; GpioDataRegs.GPACLEAR.bit.GPIO4 = 1;}
#define mRedLed1Twink()    {GpioDataRegs.GPACLEAR.bit.GPIO3 = 0; GpioDataRegs.GPATOGGLE.bit.GPIO4 = 1;}

#define mYellowLed1On()    {GpioDataRegs.GPASET.bit.GPIO3 = 1; GpioDataRegs.GPASET.bit.GPIO4 = 1;}
#define mYellowLed1Off()   {GpioDataRegs.GPACLEAR.bit.GPIO3 = 1; GpioDataRegs.GPACLEAR.bit.GPIO4 = 1;}
#define mYellowLed1Twink() {GpioDataRegs.GPATOGGLE.bit.GPIO3 = 1; GpioDataRegs.GPATOGGLE.bit.GPIO4 = 1;}

#define mGreenLed2On()     {GpioDataRegs.GPASET.bit.GPIO1 = 1; GpioDataRegs.GPACLEAR.bit.GPIO2 = 1;}
#define mGreenLed2Off()    {GpioDataRegs.GPACLEAR.bit.GPIO1 = 1; GpioDataRegs.GPACLEAR.bit.GPIO2 = 1;}
#define mGreenLed2Twink()  {GpioDataRegs.GPATOGGLE.bit.GPIO1 = 1; GpioDataRegs.GPACLEAR.bit.GPIO2 = 1;}

#define mRedLed2On()       {GpioDataRegs.GPACLEAR.bit.GPIO1 = 0; GpioDataRegs.GPASET.bit.GPIO2 = 1;}
#define mRedLed2Off()      {GpioDataRegs.GPACLEAR.bit.GPIO1 = 0; GpioDataRegs.GPACLEAR.bit.GPIO2 = 1;}
#define mRedLed2Twink()    {GpioDataRegs.GPACLEAR.bit.GPIO1 = 0; GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;}

#define mYellowLed2On()    {GpioDataRegs.GPASET.bit.GPIO1 = 1; GpioDataRegs.GPASET.bit.GPIO2 = 1;}
#define mYellowLed2Off()   {GpioDataRegs.GPACLEAR.bit.GPIO1 = 1; GpioDataRegs.GPACLEAR.bit.GPIO2 = 1;}
#define mYellowLed2Twink() {GpioDataRegs.GPATOGGLE.bit.GPIO1 = 1; GpioDataRegs.GPATOGGLE.bit.GPIO2 = 1;}

#define mGreenLed3On()     {GpioDataRegs.GPASET.bit.GPIO0 = 1; }
#define mGreenLed3Off()    {GpioDataRegs.GPACLEAR.bit.GPIO0 = 1;}


#define	mGet5msTiming()		CpuTimer0Regs.TCR.bit.TIF
#define	mClr5msTimFlag()	CpuTimer0Regs.TCR.bit.TIF = 1;
