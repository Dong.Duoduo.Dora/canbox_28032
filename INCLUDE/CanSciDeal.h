/*******************************************************************
 * 					Emerson Network Power Co., Ltd.
 * 
 *	FILENAME: H1P18U211_PFCSciDownload.c
 *
 *	DESCRIPTION: For the 2nd DSP bootdownload in two DSPs system	
 *
 *	AUTHOR:	pevin
 *
 *	HISTORY: V1.0.0(2011.10.31)
 *	
 *******************************************************************/
#include "DSP280x_Device.h"     // DSP280x Headerfile Include File

/*******************************************************************************
*Variables definition:these variables only can be used in this file 														  
*******************************************************************************/
#define	SCIDATASIZE			300
#define BOOTSTEP_RAM		0						//下载阶段0：下载RAM空间数据	
#define BOOTSTEP_RAM2FLASH	1						//下载阶段1：RAM下载到FLASH下载过渡阶段
#define BOOTSTEP_FLASH		2						//下载阶段2：下载FLASH空间数据
#define ERROR_RAMHEADREAD	1						//RAM数据头读取过程中出错
#define ERROR_FLASHHEADREAD 2						//FLASH数据头读取过程中出错
#define ERROR_DATABLOCKREAD	3						//数据块读取过程中出错
#define ERROR_DCDCDOWN		0x0E					//DCDC侧下载过程中出现错误
#define FLAG_RAMDOWNOK      1						//PFC侧RAM空间数据下载已处理完毕
#define FLAG_FLASHDOWNOK    2						//PFC侧FLASH空间数据下载已处理完毕
#define FLAG_TRIGDOWN	    3						//触发PFC进入下载状态
#define PROTNOSCI_BL		21						//SCI BootLoader下载数据传送协议
#define PROTNOSCI_BLOI		22						//SCI BootLoader其它信息传送协议
#pragma DATA_SECTION(g_u16DataBuffer0,"RamData")
UINT16  g_u16DataBuffer0[1024];						//专门用来存SCI接收到的数据
#pragma DATA_SECTION(g_u16DataBuffer1,"RamData")
UINT16  g_u16DataBuffer1[SCIDATASIZE];				//定义数据存储空间1
#pragma DATA_SECTION(g_u16DataBuffer2,"RamData")
UINT16  g_u16DataBuffer2[SCIDATASIZE];				//定义数据存储空间2
#pragma DATA_SECTION(g_u16PacketAddr1,"RamData")
UINT16  g_u16PacketAddr1;							//接收数据块数据数组中存放地址1
#pragma DATA_SECTION(g_u16PacketAddr2,"RamData")
UINT16  g_u16PacketAddr2;							//接收数据块数据数组中存放地址2
#pragma DATA_SECTION(g_u16DataLength,"RamData")
UINT16  g_u16DataLength;
#pragma DATA_SECTION(g_u16SciRxLast,"RamData")
UINT16  g_u16SciRxLast;
#pragma DATA_SECTION(g_u16SciRxPoint,"RamData")
UINT16  g_u16SciRxPoint;							// SCI 接收数据指针
#pragma DATA_SECTION(g_u16SciTxPoint,"RamData")
UINT16  g_u16SciTxPoint;							// SCI 发送数据指针
#pragma DATA_SECTION(g_u16SciTxLength,"RamData")
UINT16  g_u16SciTxLength;							// SCI 收到数据长度
#pragma DATA_SECTION(g_u16SciDataSend,"RamData")
UINT16  g_u16SciDataSend; 							// SCI 发送数据标志位
struct  BOOTLOADER_BITS {							// bits  description
	Uint16  SciR_Ok:1;								// 0  SCI未接收完数据				1  SCI已接收完数据
	Uint16  CanR_DBlk_Start:1;						// 0  数据块数据未准备好			1  数据块数据已准备好
    Uint16  CanR_DBlk_More:1;						// 0  无需向g_u16DataBuffer2存数据	1  需要要g_u16DataBuffer 2存数据
	Uint16  DataBlk_Ok:1;							// 0  已读取完一个数据块			1  未开始读数据块
	Uint16  GetHeader:1;							// 0  当前读取数据块数据			1  当前读取包头数据
    Uint16  DownLoadStep:2;							// 0  BOOTSTEP_RAM					1  BOOTSTEP_RAM2FLASH 2  BOOTSTEP_FLASH 				
    Uint16  Flash1stData:1;							// 0  未开始FLASH下载				1  开始下载FLASH空间第一串数据流		
    Uint16  CanDBlk_Err:1;							// 0  数据块下载过程中未出现错误	1  数据下载过程中出现错误
    Uint16  rsvd6:1;		
    Uint16  rsvd5:1;
    Uint16  rsvd4:1;		
    Uint16  rsvd3:1;		
    Uint16  rsvd2:1;		
    Uint16  rsvd1:1;
	Uint16  rsvd0:1;		
};
typedef	union {
    Uint16             		  all;
    struct  BOOTLOADER_BITS   bit;
}bitdataBL;
#pragma DATA_SECTION(Flag_BLStatus,"RamData")
bitdataBL Flag_BLStatus;							//一些标志位定义

/*******************************************************************************
*Functions definition 														  
*******************************************************************************/

void vSaveCanData( UINT16 *t_u16DataBuffer, UINT16 *t_u16DataAddr );
void vCanDataT( void );
Uint16 uiHexToAsc( UINT8 hex );
void Load_Code_And_Execute2( void );
interrupt void vCanDataR( void );
interrupt void vSciDataR( void );
interrupt void vSciDataT( void );
Uint16 SciWaitForceDownload( void );
void vSciOtherHandle( Uint16 t_u16DataType, Uint16 t_u16MainData, Uint16 t_u16OtherInfo );
Uint16 RamHeaderHandle( struct ECAN_MBOXES ECanaMboxes );
Uint16 FlashHeaderHandle( struct ECAN_MBOXES ECanaMboxes );
Uint16 DataBlkHandle( struct ECAN_MBOXES ECanaMboxes );
UINT8 ucAscToHex(UINT8 ascl, UINT8 asch);
Uint16 uiHexToAsc( UINT8 hex );
unsigned char g_u8MdlAddresss;
unsigned char g_u8MdlAddresss_Count;    /* 本模块地址    */
unsigned int  g_u16MdlType;
unsigned char g_u8boardaddress;       /* 485广播命令标志       */
unsigned char g_u8synchronization;     /* 485通信接收同步信号    */
unsigned char g_u8receive_ok;        /* 485通信口是否接收到数据 */
unsigned char g_u8uLight_status;
unsigned int  g_u16light1_time;
unsigned int  g_u16light2_time;
unsigned int  g_u16light3_time;
unsigned int  g_u16light4_time;

unsigned char Flag_TrigFd;
unsigned char boardaddress;       /* 485广播命令标志       */
unsigned char ready_485_transmit; /*485收发转换T2定时器延时 */
unsigned int Temptranslate_length;    /* 485发送字节数长度      */
unsigned int translate_length;    /* 485发送字节数长度      */
unsigned int  cid_main,cid;
unsigned int  g_u16CanIntTimer;
unsigned char address,address_count;    /* 本模块地址    */

unsigned int u16MdlType;
unsigned char ucMsgType;
unsigned int u16ComType;

/**************************************************************/
ubitfloat     dcvolt;       /* 模块电压 */
ubitfloat     dccurr;       /* 模块电流 */
ubitfloat     dccurr_disp;
ubitfloat     dclimit;          /* 模块实际限流点 */
ubitfloat     temperature;    /* 模块温度      */
ubitfloat     setvolt;      /* 监控单元设置的模块电压输出调节 */
ubitfloat     setlimit;     /* 监控单元设置的模块限流输出调节 */
ubitfloat     setpower;     /* 监控单元设置的模块限流输出调节 */

ubitfloat     avecurr;      /* 模块平均电流  */
ubitfloat     dcvolt_up;    /* 监控单元设置的模块电压上限  */
ubitfloat     dcvolt_float;
ubitfloat     dclimit_float;
ubitfloat     dcpower_float;
ubitfloat     accurr_set;
ubitfloat     temp_up;
ubitfloat     temp_pfc;

ubitfloat     volt_adj_delta;
ubitfloat     temp_disp;
ubitfloat     pfcvolt;
ubitfloat     pfcvolt1;
ubitfloat     acvolt;
ubitfloat     ac1volt;
ubitfloat     ac2volt;
ubitfloat     ac3volt;

ubitfloat     setpfcvolt;
ubitfloat     tagReontime;  /* add by mhp, 2031223 */
ubitfloat     tagRunTime;   /* add by mhp, 2031223 */
ubitfloat     tagRunTimeSet;

ubitfloat     tagMaintainTimes; /* add by mhp, 2031223 */
ubitfloat     tagSstartTime;
ubitfloat     tagOpenTime;
ubitfloat     dcbaud_float;
/**************************************************************/
ubitfloat     dcvoltsamp_sys_a;
ubitfloat     dcvoltsamp_sys_b;
ubitfloat     dccurrsamp_sys_a;
ubitfloat     dccurrsamp_sys_b;
ubitfloat     dcvoltcontl_sys_a;
ubitfloat     dcvoltcontl_sys_b;
ubitfloat     dccurrcontl_sys_a;
ubitfloat     dccurrcontl_sys_b;
ubitfloat     dcpowercontl_sys_a;
ubitfloat     dcpowercontl_sys_b;
ubitfloat     currinsamp_sys_a;
ubitfloat     currinsamp_sys_b;
ubitfloat     currinBsamp_sys_a;
ubitfloat     currinBsamp_sys_b;
ubitfloat     currinCsamp_sys_a;
ubitfloat     currinCsamp_sys_b;
ubitfloat     outcuroffset;
//----------------------------
ubitfloat     pfcvoltsamp_sys;
ubitfloat     pfcvoltcon_sys;   /* add by mhp, 2031223 */
ubitfloat     tempsamp_sys;

ubitfloat     acvoltsamp_sys_a;
ubitfloat     acvoltsamp_sys_b;
ubitfloat     ac1voltsamp_sys_a;
ubitfloat     ac1voltsamp_sys_b;
ubitfloat     ac2voltsamp_sys_a;
ubitfloat     ac2voltsamp_sys_b;
ubitfloat     fancon_sys_a;
ubitfloat     fancon_sys_b;

ubitfloat     uDcReadRamAdd;
ubitfloat     uDcReadRamCon;
ubitfloat     uDcWriteRamCon;
ubitfloat     uDcWriteEemAdd;
ubitfloat     uDcWriteEemCon;

ubitfloat     uAcReadRamAdd;
ubitfloat     uAcReadRamCon;
ubitfloat     uAcWriteRamCon;
ubitfloat     uAcWriteEemAdd;
ubitfloat     uAcWriteEemCon;

ubitfloat     uDcReadEemAdd;
ubitfloat     uDcReadEemCon;

ubitfloat     uAcReadEemAdd;
ubitfloat     uAcReadEemCon;

ubitfloat     uFaultRecEemAdd;
ubitfloat     uFaultRecEemCon;

//add for IS rectifier
ubitfloat   dcvolthandget;
ubitfloat   dcvoltdnget;
ubitfloat   dcvoltset;
ubitfloat   dcvoltdn;

ubitfloat   fandutyset;
ubitfloat   currin;
ubitfloat   currinB;
ubitfloat   currinC;
ubitfloat   powerin;

ubitfloat   frequencyin;
ubitfloat   e_power;
ubitfloat   efficiency;

unsigned int    u16ISAddr;
unsigned int    u16WrIdLmtH;
unsigned int    u16WrIdLmtL;
unsigned int    u16EnWrIDLo;
unsigned int    u16EnWrIDHi;
unsigned int    u16ISIdUnlock;

// add end

////////add for efuse start////////
ubitfloat   efuseVolt1;
ubitfloat   efuseVolt2;

ubitfloat   efuseCurr1;
ubitfloat   efuseCurr2;

ubitfloat   efusePower1;
ubitfloat   efusePower2;

ubitfloat   efuseKWH1;
ubitfloat   efuseKWH2;

ubitfloat   efusePortTemp;
ubitfloat   efuseVin;

ubitfloat   efuseVoltUp1;
ubitfloat   efuseVoltUp2;

ubitfloat   efuseVoltDown1;
ubitfloat   efuseVoltDown2;

ubitfloat   efuseCurrUp1;
ubitfloat   efuseCurrUp2;

ubitfloat   efuseOtp1;
ubitfloat   efuseOtp2;

ubitfloat   set_efuseVoltUp1;
ubitfloat   set_efuseVoltUp2;

ubitfloat   set_efuseVoltDown1;
ubitfloat   set_efuseVoltDown2;

ubitfloat   set_efuseCurrUp1;
ubitfloat   set_efuseCurrUp2;

ubitfloat   set_efuseOtp1;
ubitfloat   set_efuseOtp2;

//
ubitfloat   set_efuseVinsamp_a;
ubitfloat   set_efuseVinsamp_b;

ubitfloat   set_efuseVolt2_a;
ubitfloat   set_efuseVolt2_b;

ubitfloat   set_efuseVolt1_a;
ubitfloat   set_efuseVolt1_b;

ubitfloat   set_efuseCurr1_a;
ubitfloat   set_efuseCurr1_b;

ubitfloat   set_efuseCurr2_a;
ubitfloat   set_efuseCurr2_b;



ubitfloat fLoadDataA;
ubitfloat fLoadDataB;


unsigned int u16LoadSize;
unsigned int u16LoadAddrLL;
//unsigned int u16LoadReProto;
unsigned int u16LoadStatus;
unsigned int u16LoadAddrH;
unsigned int u16LoadAddrL;
unsigned int u16LoadAddr;
unsigned int u16CANlen;
unsigned int u16MdlType;
unsigned int u16ShareMode;//ls 20111125 change for Load PCT
unsigned int u16ComType;
////////add for efuse end///////////



// add for rectifier info
unsigned int    u16RecRunInfH;
unsigned int    u16RecRunInfL;

unsigned int    u16EpromErrH;
unsigned int    u16EpromErrL;
// add end

unsigned int   uSetCpuRst;
unsigned int   u16CanIntTimer;

unsigned int   hardwarever;
unsigned int   softwarever;
unsigned int   pfcswver;
unsigned int   pfcswbom;
unsigned int   downloadadd;
unsigned int   downloadcon;

ubitfloat fNodeId;
unsigned int uNodeIdH;
unsigned int uNodeIdL;
unsigned int uNodeidH;
unsigned int uNodeidL;
unsigned int uVersionNoHw;
unsigned int uVersionNoSw;
unsigned int uBarCode1;
unsigned int uBarCode2;
unsigned int uBarCode3;
unsigned int uBarCode4;
unsigned int uBarCode5;
unsigned int uBarCode6;
unsigned int uBarCode7;
unsigned int uBarCode8;

unsigned int   status;
unsigned int   status1;
unsigned int   status2;
unsigned int   status3;

unsigned char mokuai_main_address;
unsigned char mokuai_status[110];     /* 模块开关机状态        */
float         mokuai_current[110];   /* 所有模块的电流值       */
float         avergae_current;
float         mokuai_delta[110];     /* 所有模块均流调节PWM变化值 */
unsigned char mokuai_times[110];


unsigned char AcLimPowModeFlag;
unsigned char DCinMarnFlag;
unsigned char ShareModeFlag;//ls 20111125 change for share mode
unsigned char  estop_permit;
unsigned char  download_permit;
unsigned char acfault_send;
unsigned char openstatus_send;      /* 模块开关机判断 */
unsigned char  bigfan;
unsigned char fullspeedstatus;      /* 风扇全速  mhp 20031223 */
unsigned char fullspeedstatus_send;     /* 风扇全速  mhp 20031223 */
unsigned char twinklestatus_send;       /* 绿灯闪烁 mhp 20031223 */
unsigned char modelstatus_lock;
unsigned char  overvoltprot;
unsigned char ovrelaystatus;        /* 过压脱离继电器 mhp 20031223 */
unsigned char ovrelaystatus_send;       /* 过压脱离继电器 mhp 20031223 */
unsigned char permitadjest;
unsigned char regstatus_send;
unsigned int u16ClrEpromFlag;
unsigned int u16LEDFlag;
unsigned char  uPermitOverAc;
unsigned char walkinstatus_send;        /* walk-in  mhp 20031223 */
unsigned char resetstatus_send;         /* 模块复位判断 mhp 20031223 */
unsigned char uPermitOverDc;


struct ECAN_REGS ECanaShadow;



void           wr_s_limit(void);
void           wr_s_contrl(void);
void           sget_all_data(void);
void           sget_all_data_extend(void);
void           sget_calibrate_data(void);
void           sget_download_addr(void);
void           sget_download_data(void);
void           sget_download_answer(void);

void           sget_avercurr0_data(void);
void           sget_avercurr1_data(void);
void           sget_avercurr2_data(void);
void           sget_avercurr3_data(void);
void           sget_avercurr4_data(void);
void           sget_avercurr5_data(void);
void           sget_average_curr(void);

void           sget_memorytest_data(void);
void           sget_memorytest1_data(void);
void           sget_memorytest2_data(void);
void           sget_memorytest3_data(void);














