											 /*=============================================================================*
 *         Copyright(c) 2007-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : GBB Rectifiers
 *
 *  FILENAME : H7413DU111_constant.h
 *  PURPOSE  : Define the constant
 *				     
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *  2008-6-10         OOA            Agnes           Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    
 *============================================================================*/

/*============================================================================*
 * constant definition
 *============================================================================*/

/*------------��������-------------------------*/
//------------------------------------------------------------------------------
//define constant for SCI com
//------------------------------------------------------------------------------
#define SCI_FIRST_CHAR      0x7E        // begin byte for sci send
#define SCI_LAST_CHAR       0x0D        // end byte for sci send
#define SCI_RX_MAX_LENGTH   0x1E        // sci receive max length


//----------------------------------------------------------------------
//define constant for module RAM address CLEAR                                         
//-----------------------------------------------------------------------

#define BOOT_ISR_RAM_START		((UINT16)0x600)
#define BOOT_ISR_RAM_END		((UINT16)0x7E0)

#define MAIN_ISR_RAM_START		((UINT16)0x600)
#define MAIN_ISR_RAM_END		((UINT16)0x800)

#define BOOT_RAM_START		((UINT16)0x8000)
#define BOOT_RAM_END		((UINT16)0xA000)  //ls change RAM 20120319

#define MAIN_RAM_START		((UINT16)0x8940)
#define MAIN_RAM_END		((UINT16)0x9170)  //wzc change RAM 20160909

#ifndef NULL
#define NULL 	0
#endif 
