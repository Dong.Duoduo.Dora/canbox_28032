/*=============================================================================*
 *         Copyright(c) 2004-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_CanDriver.h
 *  PURPOSE  : rectifier communicate with controller and other rectifiers	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2008-11-25      A000           zoulx             Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    detail information refer to gbb_main.h      
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                               		     DESCRIPTION 
 *============================================================================*/

/*******************************************************************************
*define constants for tx and rx can data														  
*******************************************************************************/
#define CAN_RXBUF_SIZE   	10
#define CAN_TXBUF_SIZE    	15
#define	CAN_RXBUF_EMPTY	  	0
#define	CAN_RXBUF_RDY	  	1

/*******************************************************************************
*define constant in CAN frame
*******************************************************************************/
//for PROTNO
#define   RMP            	0x060
#define   RRP            	0x070

#define   NACMP               0x064
#define   NACCP               0x071

#define   SCMP               0x066
#define   SCCP               0x074

#define   NACMP               0x064
#define   NACCP               0x071

#define   EMP         0X6B

//for PTP
#define   BROAD_CAST      	0x00
#define   PTP_MODE        	0x01

//for CNT
#define   DATA_END       	 0x00
#define   DATA_CNT        	0x01

//for ErrType
#define   NORMAL         	0xf0
#define   ADDR_FAIL      	0xf1
#define   COMM_FAIL      	0xf2
#define   DATA_CHK_FAIL  	0xf3
#define   ADDR_IDENTIFY  	0xf4

//for module address
#define   BROAD_CAST_ADDR  	0xff 
#define   CSU_ADDR          0xf0 

//for message type
#define   REPLY_BY_BYTE   	0x41
#define   REPLY_BY_BIT     	0x42
/*******************************************************************************
*declare variables  for CAN frame
*******************************************************************************/
extern UINT16 g_u16PROTNO;                
extern UINT16 g_u16PTP;      
extern UINT16 g_u16DSTADDR;  
extern UINT16 g_u16SRCADDR;  
extern UINT16 g_u16CNT;      
extern UINT16 g_u16ErrFlag;  
extern UINT16 g_u16ErrType;  
extern UINT16 g_u16MsgType;  
extern UINT16 g_u16ValueType;
extern ubitfloat g_ubitfCanData;
extern ubitfloat g_iq10CanData;
extern ubitfloat g_iq12CanData;

extern CANFRAME  g_stCanTxData;
extern CANFRAME  g_stCanRxData;

/*******************************************************************************
*declare application for app
*******************************************************************************/
extern void sCanBufferInitial(void);
extern void sCanRxISR0(void);
extern void sCanTxISR5(void);
extern void sCanResetTx4(void);
extern UINT16 sCanRead(CANFRAME *pdata);
extern void sCanWrite5(CANFRAME *pdata);
extern void sCanHdSend4(CANFRAME *pdata);
extern void sCanHdSend5(CANFRAME *pdata);
extern void sDecodeCanRxFrame(CANFRAME stRxFrameTemp);
extern void sEncodeCanTxID(UINT16 u16ProtNoTemp,UINT16 u16PTPTemp,
                           UINT16 u16DstAddrTemp,UINT16 u16CntTemp);
extern void sEncodeCanTxID1(UINT16 u16ProtNoTemp,UINT16 u16PTPTemp,
                           UINT16 u16DstAddrTemp,UINT16 u16CntTemp);
extern void sEncodeCanTxData(UINT16 u16MsgTypeTemp,UINT16 u16ErrTypeTemp, 
                             UINT16 u16ValueTypeTemp,ubitfloat ubitfDataTemp);
extern void sEncodeCanTxIQ10Data(UINT16 u16MsgTypeTemp,UINT16 u16ErrTypeTemp, 
                                UINT16 u16ValueTypeTemp,ubitfloat ubitfDataTemp);
extern void sEncodeCanTxU16Data(UINT16 CanData0, UINT16 CanData1,
                                UINT16 CanData2, UINT16 CanData3);
extern void sEncodeMailBox5ID(unsigned int num,unsigned int len);

