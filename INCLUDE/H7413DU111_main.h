//########################################################
//
// FILE:   HC415U111_main.h
//
// TITLE:   HA415U111_main variables definitions.
//
//########################################################
// Running on TMS320LF280xA   DCDC part   HA415U111                
// External clock is 20MHz, PLL * 6/2 , CPU-Clock 60 MHz	      
// Date: from March 16, 2007 to Nov 30, 2007  , (C) czk
// Version:1.00     Change Date: April 4, 2007 , (C) czk					  	
//########################################################

#ifndef MAIN_H
#define MAIN_H

#ifdef __cplusplus
extern "C" {
#endif
/*******************************************************************************
*variables and functions for main and time treament 														  
*******************************************************************************/

enum CAN_VALUE_TYPE
{
    GET_VOLT = 0x01,
    GET_REAL_CURR,
};

extern ubitintc	g_u16RunFlag;		//run flag of all the functions

//the functions of interrupt
extern interrupt void vSciDataR( void );
extern interrupt void vSciDataT( void );
void vCanProcess(void);
extern unsigned char get_can_data(unsigned char start_cid,unsigned char len);
void DownloadAddrSend(UINT16 data_len);
void txd_asc(unsigned int len);

extern void error(void);  
extern void	vDataInit(void);
  	

// CAN communication treatment 
extern void vSciDataParsing(void);

//Rrp protocol treatment
extern void	vRrpProtocol(void);	

void           can_init(void);
void           hvdc_can_init(void);
void           Converter_can_init(void);
void           ISrect_can_init(void);
void           MPPT_can_init(void);

extern int16 PRG_key0;        //   CSM Key values
extern int16 PRG_key1;
extern int16 PRG_key2;
extern int16 PRG_key3;
extern int16 PRG_key4;
extern int16 PRG_key5;
extern int16 PRG_key6;
extern int16 PRG_key7; 

extern unsigned char Flag_TrigFd;
extern  ubitfloat g_fRdTemp;
extern unsigned char g_u8MdlAddresss;
extern unsigned char g_u8MdlAddresss_Count;    /* 本模块地址    */
extern unsigned int  g_u16MdlType;
extern unsigned char ucMsgType;
extern unsigned char g_u8boardaddress;       /* 485广播命令标志       */
extern unsigned char g_u8synchronization;     /* 485通信接收同步信号    */
extern unsigned char g_u8receive_ok;        /* 485通信口是否接收到数据 */
extern unsigned char g_u8uLight_status;
extern unsigned int  g_u16light1_time;
extern unsigned int  g_u16light2_time;
extern unsigned int  g_u16light3_time;
extern unsigned int  g_u16light4_time;
extern unsigned char can_overtime_ok;
extern unsigned int  g_u16CanIntTimer;

extern unsigned char AcLimPowModeFlag;
extern unsigned char DCinMarnFlag;
extern unsigned char ShareModeFlag;//ls 20111125 change for share mode
extern unsigned char  estop_permit;
extern unsigned char  download_permit;
extern unsigned char acfault_send;
extern unsigned char openstatus_send;      /* 模块开关机判断 */
extern unsigned char  bigfan;
extern unsigned char fullspeedstatus;      /* 风扇全速  mhp 20031223 */
extern unsigned char fullspeedstatus_send;     /* 风扇全速  mhp 20031223 */
extern unsigned char twinklestatus_send;       /* 绿灯闪烁 mhp 20031223 */
extern unsigned char modelstatus_lock;
extern unsigned char  overvoltprot;
extern unsigned char ovrelaystatus;        /* 过压脱离继电器 mhp 20031223 */
extern unsigned char ovrelaystatus_send;       /* 过压脱离继电器 mhp 20031223 */
extern unsigned char permitadjest;
extern unsigned char regstatus_send;
extern unsigned int u16ClrEpromFlag;
extern unsigned int u16LEDFlag;
extern unsigned char  uPermitOverAc;
extern unsigned char walkinstatus_send;        /* walk-in  mhp 20031223 */
extern unsigned char resetstatus_send;         /* 模块复位判断 mhp 20031223 */
extern unsigned char uPermitOverDc;

extern ubitfloat     volt_adj_delta;
extern ubitfloat     tagRunTimeSet;

extern ubitfloat     dcvolt;       /* 模块电压 */
extern ubitfloat     dccurr;       /* 模块电流 */
extern ubitfloat     dccurr_disp;
extern ubitfloat     dclimit;          /* 模块实际限流点 */
extern ubitfloat     temperature;    /* 模块温度      */
extern ubitfloat     setvolt;      /* 监控单元设置的模块电压输出调节 */
extern ubitfloat     setlimit;     /* 监控单元设置的模块限流输出调节 */
extern ubitfloat     setpower;     /* 监控单元设置的模块限流输出调节 */

extern ubitfloat     avecurr;      /* 模块平均电流  */
extern ubitfloat     dcvolt_up;    /* 监控单元设置的模块电压上限  */
extern ubitfloat     dcvolt_float;
extern ubitfloat     dclimit_float;
extern ubitfloat     dcpower_float;
extern ubitfloat     accurr_set;
extern ubitfloat     temp_up;
extern ubitfloat     temp_pfc;

extern ubitfloat     volt_adj_delta;
extern ubitfloat     temp_disp;
extern ubitfloat     pfcvolt;
extern ubitfloat     pfcvolt1;
extern ubitfloat     acvolt;
extern ubitfloat     ac1volt;
extern ubitfloat     ac2volt;
extern ubitfloat     ac3volt;

extern ubitfloat     setpfcvolt;
extern ubitfloat     tagReontime;  /* add by mhp, 2031223 */
extern ubitfloat     tagRunTime;   /* add by mhp, 2031223 */
extern ubitfloat     tagRunTimeSet;

extern ubitfloat     tagMaintainTimes; /* add by mhp, 2031223 */
extern ubitfloat     tagSstartTime;
extern ubitfloat     tagOpenTime;
extern ubitfloat     dcbaud_float;

extern ubitfloat     dcvoltsamp_sys_a;
extern ubitfloat     dcvoltsamp_sys_b;
extern ubitfloat     dccurrsamp_sys_a;
extern ubitfloat     dccurrsamp_sys_b;
extern ubitfloat     dcvoltcontl_sys_a;
extern ubitfloat     dcvoltcontl_sys_b;
extern ubitfloat     dccurrcontl_sys_a;
extern ubitfloat     dccurrcontl_sys_b;
extern ubitfloat     dcpowercontl_sys_a;
extern ubitfloat     dcpowercontl_sys_b;
extern ubitfloat     currinsamp_sys_a;
extern ubitfloat     currinsamp_sys_b;
extern ubitfloat     currinBsamp_sys_a;
extern ubitfloat     currinBsamp_sys_b;
extern ubitfloat     currinCsamp_sys_a;
extern ubitfloat     currinCsamp_sys_b;
extern ubitfloat     outcuroffset;

//add for IS rectifier
extern ubitfloat   dcvolthandget;
extern ubitfloat   dcvoltdnget;
extern ubitfloat   dcvoltset;
extern ubitfloat   dcvoltdn;

extern ubitfloat   fandutyset;
extern ubitfloat   currin;
extern ubitfloat   currinB;
extern ubitfloat   currinC;
extern ubitfloat   powerin;

extern ubitfloat     acvoltsamp_sys_a;
extern ubitfloat     acvoltsamp_sys_b;
extern ubitfloat     ac1voltsamp_sys_a;
extern ubitfloat     ac1voltsamp_sys_b;
extern ubitfloat     ac2voltsamp_sys_a;
extern ubitfloat     ac2voltsamp_sys_b;
extern ubitfloat     fancon_sys_a;
extern ubitfloat     fancon_sys_b;

extern ubitfloat     pfcvoltsamp_sys;
extern ubitfloat     pfcvoltcon_sys;   /* add by mhp, 2031223 */
extern ubitfloat     tempsamp_sys;

extern ubitfloat     uDcReadRamAdd;
extern ubitfloat     uDcReadRamCon;
extern ubitfloat     uDcWriteRamCon;
extern ubitfloat     uDcWriteEemAdd;
extern ubitfloat     uDcWriteEemCon;

extern ubitfloat     uAcReadRamAdd;
extern ubitfloat     uAcReadRamCon;
extern ubitfloat     uAcWriteRamCon;
extern ubitfloat     uAcWriteEemAdd;
extern ubitfloat     uAcWriteEemCon;

extern ubitfloat     uDcReadEemAdd;
extern ubitfloat     uDcReadEemCon;

extern ubitfloat     uAcReadEemAdd;
extern ubitfloat     uAcReadEemCon;

extern ubitfloat     uFaultRecEemAdd;
extern ubitfloat     uFaultRecEemCon;

extern unsigned int   uSetCpuRst;

extern unsigned int    u16ISAddr;
extern unsigned int    u16WrIdLmtH;
extern unsigned int    u16WrIdLmtL;
extern unsigned int    u16EnWrIDLo;
extern unsigned int    u16EnWrIDHi;
extern unsigned int    u16ISIdUnlock;

extern unsigned int   hardwarever;
extern unsigned int   softwarever;
extern unsigned int   pfcswver;
extern unsigned int   pfcswbom;
extern unsigned int   downloadadd;
extern unsigned int   downloadcon;

extern ubitfloat fNodeId;
extern unsigned int uNodeIdH;
extern unsigned int uNodeIdL;
extern unsigned int uNodeidH;
extern unsigned int uNodeidL;
extern unsigned int uVersionNoHw;
extern unsigned int uVersionNoSw;
extern unsigned int uBarCode1;
extern unsigned int uBarCode2;
extern unsigned int uBarCode3;
extern unsigned int uBarCode4;
extern unsigned int uBarCode5;
extern unsigned int uBarCode6;
extern unsigned int uBarCode7;
extern unsigned int uBarCode8;

extern unsigned char mokuai_main_address;
extern unsigned char mokuai_status[110];     /* 模块开关机状态        */
extern float         mokuai_current[110];   /* 所有模块的电流值       */
extern float         avergae_current;
extern float         mokuai_delta[110];     /* 所有模块均流调节PWM变化值 */
extern unsigned char mokuai_times[110];

extern unsigned int u16LoadSize;
extern unsigned int u16LoadAddrLL;
//unsigned int u16LoadReProto;
extern unsigned int u16LoadStatus;
extern unsigned int u16LoadAddrH;
extern unsigned int u16LoadAddrL;
extern unsigned int u16LoadAddr;
extern unsigned int u16CANlen;
extern unsigned int u16MdlType;
extern unsigned int u16ShareMode;//ls 20111125 change for Load PCT
extern unsigned int u16ComType;

extern unsigned char address,address_count;    /* 本模块地址    */

extern unsigned int   status;
extern unsigned int   status1;
extern unsigned int   status2;
extern unsigned int   status3;

// add for rectifier info
extern unsigned int    u16RecRunInfH;
extern unsigned int    u16RecRunInfL;

extern unsigned int    u16EpromErrH;
extern unsigned int    u16EpromErrL;

extern ubitfloat   frequencyin;
extern ubitfloat   e_power;
extern ubitfloat   efficiency;

extern UINT16  g_u16DataBuffer0[1024];

//----------------sci--------------------
extern  UINT16  g_u16SciRxPoint;


#ifdef __cplusplus
}
#endif 


#endif 


