											 /*=============================================================================*
 *         Copyright(c) 2007-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : GBB Rectifiers
 *
 *  FILENAME : GBB_constant.h
 *  PURPOSE  : Define the constant
 *				     
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *  2009-12-18         OOA                      Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    
 *============================================================================*/

/*============================================================================*
 * constant definition  
 *============================================================================*/
 //--------------------------------------------------------------------
//define constant status falg  for communication    csm 091217                                    
//--------------------------------------------------------------------

#define SCIARX_INT_ENABLE   1

#define CRCSPE           0x180d     /* CRC ������*/

#define RECOK		0xf0		/* communication normal */ 		
#define CRCCOM		0xf1 		/* CRC check fail */
#define XORCOM		0xf1 		/* XOR check fail */
#define NOVCOM		0xf2		/* invalid command  */
#define NULLCOM		0xf3		/* other fault  */

#define WR_S_LIMIT             0x51
#define WR_S_CONTRL          0x53

#define SET_AVERCURR0_DATA  0x37
#define SET_AVERCURR1_DATA  0x38
#define SET_AVERCURR2_DATA  0x39
#define SET_AVERCURR3_DATA  0x32
#define SET_AVERCURR4_DATA  0x31
#define SET_AVERCURR5_DATA  0x30
#define GET_AVERAGE_CURR  0x29

#define SET_MEMORYTEST_DATA 0x36
#define SET_MEMORYTEST1_DATA 0x35
#define SET_MEMORYTEST2_DATA 0x34
#define SET_MEMORYTEST3_DATA 0x33

#define SGET_ALL_DATA  0x40
#define SGET_CAL_DATA  0x46
#define SGET_DOWNLOAD_DATA  0x47
#define SGET_DOWNLOAD_ADDR  0x48
#define SGET_DOWNLOAD_ANSWER  0x49

#define ADJ_STAT        0x5aa5
#define OPENMODE       0x5a
#define CLOSEMODE      0xa5
/****************************************************************************/
/****** SETUP for the CAN Master Control  - Register ( CAN_MCR)****************/
#define CAN_SUSP    0           /* 13 : Soft Mode : Stop CAN when Trans complete */
#define CAN_CCR     1           /* 12 : write access to CAN-Configuration registers */
#define CAN_PDR     0           /* 11 : no power down mode requested  */
#define CAN_DBO     1           /* 10 : Data-Byte-Order : 0,1,2,3,4,5,6,7  */
#define CAN_WUBA    0           /* 9  : Wake from power down only after write 0 to PDR */
#define CAN_CDR     0           /* 8  : no "change data field request to any mailbox */
#define CAN_ABO     1           /* 7  : after "bus off" only wit reset return to CAN */
#define CAN_STM     0           /* 6  : no self test mode */
#define CAN_MBNR    0           /* 1-0: Mailbox Nr  */
/****************************************************************************/
#define CAN_BRP     19          //19(125k) change for 250K/* Baud rate prescaler    */
#define CAN_SBG     0           /* synchronise CAN with both edges */
#define CAN_SJW     2           /* Syncronisation jump width */
#define CAN_SAM     0           /* take only one sample per bit */
#define CAN_TSEG1   10          /* Time Segment 1  */
#define CAN_TSEG2   3           /* Time Segment 2  */

#define RECT        0
#define HVDC        1
#define Converter   2
#define IS_RECT     3
#define MPPT        4
#define MPPT_HV     5
#define EPA     6
#define efuse 7

#define LOAD_PCT    1
#define REAL_CURR   0

#define COM_0X00    1
#define COM_SINGLE  0
//
#define WR_S_LIMIT             0x51
#define WR_S_CONTRL          0x53

#define SET_AVERCURR0_DATA  0x37
#define SET_AVERCURR1_DATA  0x38
#define SET_AVERCURR2_DATA  0x39
#define SET_AVERCURR3_DATA  0x32
#define SET_AVERCURR4_DATA  0x31
#define SET_AVERCURR5_DATA  0x30
#define GET_AVERAGE_CURR  0x29

#define SET_MEMORYTEST_DATA 0x36
#define SET_MEMORYTEST1_DATA 0x35
#define SET_MEMORYTEST2_DATA 0x34
#define SET_MEMORYTEST3_DATA 0x33

#define SGET_ALL_DATA  0x40
#define SGET_CAL_DATA  0x46
#define SGET_DOWNLOAD_DATA  0x47
#define SGET_DOWNLOAD_ADDR  0x48
#define SGET_DOWNLOAD_ANSWER  0x49

#define ADJ_STAT        0x5aa5
#define OPENMODE       0x5a
#define CLOSEMODE      0xa5
